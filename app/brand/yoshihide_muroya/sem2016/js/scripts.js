(function() {
	$(document).ready(function() {
		$('#check').click(function() {
			if ($(this).prop('checked')) {
				$(this).parent().addClass('checked');
			} else {
				$(this).parent().removeClass('checked');
			}
		});
		$('.btn-submit a').click(function(e) {
			if ($('#check').prop('checked')) {
				return true;
			} else {
				e.preventDefault();
				alert('応募規約を読み、同意にチェックを入れてください。 ');
			}
		});
		var getDevice = (function(){
    	var ua = navigator.userAgent;
    	if(ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0){
			$(".btn-submit > a").attr("href","https://sp.lexus.jp/cmpn/gp/le/dc/sem2016");
			$(".btn-submit > a").on('click', function() {
				if ($('#check').prop('checked')) {
					//SP ClickCount
					sc('lexus.smp:brand:amazing_in_motion_yoshihide_muroya:submit-sem2016');
				}
			});
    	}
		else{
			$(".btn-submit > a").attr("href","https://lexus.jp/cmpn/gp/le/dc/sem2016");
			$(".btn-submit > a").on('click', function() {
				if ($('#check').prop('checked')) {
					//PC ClickCount
					sc('lexus:brand:amazing_in_motion_yoshihide_muroya:submit-sem2016');
				}
			});
    	}
		})();
	});
}());
