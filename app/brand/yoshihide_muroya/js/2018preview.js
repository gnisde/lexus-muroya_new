$(function () {

  $(window).on('scroll', function () {
    var wt = $(window).scrollTop();

    $('.preview').each(function () {
      var tp = $(this).offset().top;
      if(tp < wt + $(window).height() * 0.7) {
        if(!$(this).hasClass('show')) {
          $(this).addClass('show');
        }
        /*
        var d = ((wt + $(window).height() * 0.7) - tp) * 0.4 - 100;
        if(d < -50) d = -50;
        $(this).find('.date').css({
          'transform' : 'translate3d(0,'+(d)+'px,0)'
        })*/
      }
    })
    
  }).trigger('scroll')

  
  //
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

})




function onYouTubeIframeAPIReady() {
  var player;
  var _arr = $('.rmovieElement').attr('href').split('/');
  var videoId = _arr[_arr.length - 1].split('?')[0];

  var onPlayerReady = function(event) {
      player.mute();
  };
  var onPlayerStateChange = function(event) {
      if (event.data == YT.PlayerState.ENDED) {
          player.playVideo();
      }
      if (event.data == YT.PlayerState.PLAYING) {

      }
  };
  var initPlayer = function(){
      player = new YT.Player('video', {
          videoId: videoId,
          width: '1280',
          height: '719',
          playerVars: {
            'wmode': 'transparent',
            'autoplay': 1,
            'controls': 0,
            'muted' : 1,
            'rel': 0,
            'showinfo': 0,
            'hd': 1,
            'loop' : 1
          },
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
      });
  };

  initPlayer();

}