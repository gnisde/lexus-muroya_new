jQuery(function ($) {

	$(window).bind("load",function()
	{
		global.init();
	});

	var Global = function(){};
	Global.prototype = 
	{
		isPlayerInit: false,
		waitVideoId: "",
		videoPlayer: null,
		movieCloseSC: "",
		movieCompleteSC: "",
		
		// constructor
		create:function()
		{
	        return this;
	    },
		
		init:function()
		{
			this.setVideoPlayer();
			this.addEventListener();
			this.rolloverInit();
			this.setSNS();
		},
		
		addEventListener:function()
		{
			$("#movie_player .close a").click(function()
			{
				global.hideVideo();
				sc(global.movieCloseSC);
				return false;
			});
		},
		
		setVideoPlayer:function()
		{
			if($("#video_player").length > 0)
			{
				var tag = document.createElement('script'); 
				tag.src = "//www.youtube.com/player_api"; 
				var firstScriptTag = document.getElementsByTagName('script')[0]; 
				firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			}
		},
		
		onYouTubePlayerAPIReady:function() 
		{ 
			this.isPlayerInit = true;
			
			if(this.waitVideoId.length > 0)
			{
				this.showVideo(this.waitVideoId, this.movieCloseSC, this.movieCompleteSC);
				this.waitVideoId = "";
			}
		},
		onVideoPlayerReady:function(event)
		{
			
		},
		onVideoPlayerStateChange:function(event)
		{ 
			switch(event.data)
			{
				case 0:
					// play complete
					global.hideVideo();
					sc(global.movieCompleteSC);
					break;
				case 2:
					// play stop
					break;
				case 1:
					// play start
					break;
			}
		},
		showVideo:function(id, closeSC, completeSC)
		{
			this.movieCloseSC = closeSC;
			this.movieCompleteSC = completeSC;
			
			if(!this.isPlayerInit)
			{
				this.waitVideoId = id;
				return;
			}
			
			if(this.videoPlayer)
			{
				this.videoPlayer.loadVideoById(id);
			}
			else
			{
				this.videoPlayer = new YT.Player('video_player', {
					width: '900',
					height: '535', 
					videoId: id,
					playerVars: {
						rel: 0,
						iv_load_policy: 1,
						showinfo: 1,
						autoplay: 1,
						wmode: "transparent"
					},
					events: {
						'onReady': this.onVideoPlayerReady,
						'onStateChange': this.onVideoPlayerStateChange
					}
				});
			}
			
			var windowHeight = $(window).height();
			var scrollTop = $(window).scrollTop();
			var modalContentHeight = $("#movie_player .container").height();
			
			if(windowHeight < modalContentHeight)
			{
				$("#movie_player .container").css({"top": scrollTop, "position": "absolute"});
			}
			else
			{
				var modalPosY = Math.floor((windowHeight - modalContentHeight) / 2);
				$("#movie_player .container").css({"top": modalPosY, "position": "fixed"});
			}
			
			$("#movie_player").delay(150).fadeIn(300);
		},
		hideVideo:function()
		{
			$("#movie_player").fadeOut({duration: 600, complete: function()
			{
				global.videoPlayer.stopVideo();
			}});
		},
		
		setSNS:function()
		{
			var twitter_url = 'https://twitter.com/share?url='+encodeURI(document.URL)+'&text='+encodeURI($('title').text());
			$(".sns .tw a").attr('href', twitter_url).click(function()
			{
				window.open(twitter_url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
				return false;
			});
			
			var facebook_url = 'https://www.facebook.com/sharer.php?u='+encodeURI(document.URL);
			$(".sns .fb a").attr('href', facebook_url).click(function()
			{
				window.open(facebook_url, 'facebookwindow', 'width=700,height=600,personalbar=0,toolbar=0,scrollbars=1,resizable=1');
				return false;
			});
			
			/*
			var google_url = 'https://plus.google.com/share?url='+encodeURI(document.URL);
			$(".sns .g a").attr('href', google_url).click(function()
			{
				window.open(google_url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
				return false;
			});*/
			$(".sns .g a").hide();
		},
		
		pageScroll:function(target, ajust)
		{
			var targetPos = 0;
			if(target)
			{
				targetPos = $(target).offset().top + ajust;
			}
			
			$('html,body').animate({scrollTop:targetPos}, {duration:600, easing:"easeOutCubic"});
			
			return false;
		},
		
		rolloverInit:function()
		{
			$("img.swap").each(function(i) 
			{
				var imgsrc = this.src;
				var dot = imgsrc.lastIndexOf(".");
				var imgsrc_on = imgsrc.substr(0, dot) + '_over' + imgsrc.substr(dot, 4);
				$("<img>").attr("src", imgsrc_on);
				
				if(!imgsrc.match("_over")) 
				{
					$(this).hover(
						function() { this.src = imgsrc_on; },
						function() { this.src = imgsrc; }
					);
				}
			});
			
			$("img.bright").mouseover(function()
			{
				$(this).css({opacity: 0.25}).fadeTo(250, 1);
			});
			
			$("img.alpha").hover(
				function() { $(this).stop().fadeTo(250, 0.5); },
				function() { $(this).stop().fadeTo(250, 1); }
			);
		},
		
		preloadImage:function(arguments)
		{
			for(var i = 0; i < arguments.length; i++)
			{
				$("<img>").attr("src", arguments[i]);
			}
		}
		
	}
	var global = new Global().create();


	function onYouTubePlayerAPIReady()
	{
		global.onYouTubePlayerAPIReady();
	}
});