/*
 * beijing2016.js
 * 
 */

var ua = navigator.userAgent.toLowerCase();

var lexusjpBeijing2016 = (function() {
    var user = {
        os: 'pc',
        device: 'pc',
        type: 'pc'
    };
    if ((ua.indexOf('iphone') > -1) || (ua.indexOf('ipod') > -1) || (ua.indexOf('ipad') > -1)) {
        user.os = 'ios';
        user.device = 'iphone';
        user.type = 'mobile';
        if (ua.indexOf('ipod') > -1) {
            user.device = 'ipod';
        }
        if (ua.indexOf('ipad') > -1) {
            user.device = 'ipad';
            user.type = 'tablet';
        }
    }
    if (ua.indexOf('android') > -1) {
        user.os = 'android';
        user.device = 'tablet';
        user.type = 'tablet';
        if (ua.indexOf('mobile') > -1) {
            user.device = 'mobile';
            user.type = 'mobile';
        }
    }
    return {
        user: user
    };
})();

/*
 *  MainVisual Movie
 */
// This code loads the IFrame Player API code asynchronously.
(function(){
    if (lexusjpBeijing2016.user.device !== 'pc') {
        return;
    }
    $(document).on('ready', function() {
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    });
}());

// This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
function onYouTubeIframeAPIReady() {

    var $ = window.jQuery;
    var $inner   = $('.hero-visual.w-1600.hero-movie .hero-visual-inner');
    var $mask     = $inner.find(".mask");
    var $playmark = $inner.find(".playmark");
    var $logo     = $inner.find(".logo");
    var $over     = $inner.find(".rollover");
    var $anchor   = $inner.find("#anchorForPopup");

    var player;
    var videoId = 'p2N4R0TVCEc';

    var onPlayerReady = function(event) {
        player.mute();
    };
    var onPlayerStateChange = function(event) {
        if (event.data == YT.PlayerState.ENDED) {
            player.playVideo();
        }
        if (event.data == YT.PlayerState.PLAYING) {
            $mask.fadeIn();
            $playmark.fadeIn();
            $logo.fadeIn();
            $over.fadeIn();
            $anchor.fadeIn();
        }
    };
    var initPlayer = function(){
        player = new YT.Player('tvcm15', {
            videoId: videoId,
            width: '1600',
            height: '900',
            playerVars: {
                'wmode': 'transparent',
                'autoplay': 1,
                'controls': 0,
                'rel': 0,
                'showinfo': 0,
                'hd': 1
            },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    };

    initPlayer();

    $(document).on('cbox_closed', function(){
        var $el = jQuery.colorbox.element();
        if ($el.is("#anchorForPopup")) {
            player.playVideo();
        }
    });

    $(document).on('click', '#anchorForPopup', function() {
        var state = player.getPlayerState();
        if (state === YT.PlayerState.PLAYING) {
            player.pauseVideo();
        }
        else if (state === YT.PlayerState.PAUSED || state === YT.PlayerState.ENDED) {
            player.playVideo();
            return false;
        }
        if (userAgent.indexOf('msie')) {
            $('.hero-movie .hero-visual-wrap').css('visibility', 'hidden');
        }
    });
}

/*
 *  MainVisual Movie Alternative Image
 */
(function($) {
    $(document).ready(function(){
        var $inner = $('.hero-visual.w-1600.hero-movie .hero-visual-inner');
        if (lexusjpBeijing2016.user.device !== 'pc' || $('html').hasClass('lt-ie8')) {
            $inner.addClass('alternative');
            $inner.find(".alt").show();
            $inner.find(".playmark").show();
            $inner.find(".logo").hide();
            $inner.find(".rollover").show();
            $inner.find("#anchorForPopup").show();
        }
    });
})(window.jQuery);

/*
 *  MainVisual Movie Rollover
 */
(function($) {
    $(document).ready(function(){
        var $rollover = $('.hero-visual.w-1600.hero-movie .hero-visual-inner span.rollover');
        $("#anchorForPopup")
        .on('mouseenter', function(){
            $rollover.css({'opacity': 0.5});
        })
        .on('mouseleave', function(){
            $rollover.css({'opacity': 0});
        });
    });
})(window.jQuery);

/*
 *  MainVisual Movie Rollover
 */
$('#slide-exterior .bxslider').bxSlider({
    preloadImages: 'all',
    pagerCustom: '#slide-exterior .bxpager',
    onSliderLoad: function() {
        $('#slide-exterior .bxslider').animate({ opacity: 1.0 });
    }
});

$('#slide-interior .bxslider').bxSlider({
    preloadImages: 'all',
    pagerCustom: '#slide-interior .bxpager',
    onSliderLoad: function() {
        $('#slide-interior .bxslider').animate({ opacity: 1.0 });
    }
});

$('#slide-technology .bxslider').bxSlider({
    preloadImages: 'all',
    pagerCustom: '#slide-technology .bxpager',
    onSliderLoad: function() {
        $('#slide-technology .bxslider').animate({ opacity: 1.0 });
    }
});
