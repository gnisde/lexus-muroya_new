$(function () {
  var w = $(window).width();
  var h = window.innerHeight;
  if(w/h > 16/9) {
    w = h * 16/9;
  }
  //if(w > 980) w = 980;


  var ua = navigator.userAgent;
  if (ua.indexOf('iPhone') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0){
  } else if (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0) {
  } else {
    var href = $(".rmovieElement2").attr('href');
    //www.youtube.com/embed/wdpxibKpFts?rel=0&amp;version=3&amp;hl=ja_JP&amp;autoplay=1
    if(href) {
      href = href.replace('https://youtu.be/', '//www.youtube.com/embed/');
      href += '?rel=0&amp;version=3&amp;hl=ja_JP&amp;autoplay=1';
      $(".rmovieElement2").attr('href', href);
      $(".rmovieElement2").colorbox({
        iframe:true,
        maxWidth:"100%",
        maxHeight:"100%",
        transition: "none",
        scalePhotos: false,
        returnFocus: false,
        width: w,
        height: w * 9/16,
        fixed : true
      });
    }
  }
  
  $(".rmovieElement").colorbox({
    iframe:true,
    maxWidth:"100%",
    maxHeight:"100%",
    transition: "none",
    scalePhotos: false,
    returnFocus: false,
    width: w,
    height: w * 9/16,
    fixed : true
  });

  $(window).on('cbox_open', function() {
    setTimeout(function() {
      $(window).trigger('resize')
    }, 10)
  })
  $(window).resize(function(){
    var w = $(window).width();
    var h = window.innerHeight;
    if(w/h > 16/9) {
      w = h * 16/9;
    }
    var opt = {
      width: w,
      height: w * 9/16,
      top : ($(window).height() - (w * 9/16)) * 0.5
    }
    //if(w > 980) w = 980;
    
    $.colorbox.resize(opt);
  }).trigger('resize');
  

})

