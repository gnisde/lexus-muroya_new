function initNav(window, $, _, APP) {

(function(window, $, _, APP) {
  /**
   * Contents navigation include
   */

  var firstCategoryData = {
    'brand'            : { 'name': 'BRAND', 'url': '/brand/' }
  };

  var secondCategoryData = {
    'yoshihide_muroya' : { 'name': 'YOSHIHIDE MUROYA', 'url': '/brand/yoshihide_muroya/' }
  };

  var thirdCategoryData = {
    'top'          : { 'name': 'TOP', 'url': '/brand/yoshihide_muroya/' },
    'event_topics' : { 'name': 'EVENTS & TOPICS', 'url': '/brand/yoshihide_muroya/events/' },
    'races'        : { 'name': 'RACES', 'url': '/brand/yoshihide_muroya/races/2019/' },
    'redbullairrace'        : { 'name': 'Red Bull Air Race', 'url': '/brand/yoshihide_muroya/redbullairrace/' },
    'movie'        : { 'name': 'MOVIE', 'url': '/brand/yoshihide_muroya/movie/' },
    'crossing'        : { 'name': 'CROSSING', 'url': '/brand/yoshihide_muroya/crossing/' }
  };

  var navHTML = ''
      +'<div class="brand-head_wrap u-clearfix">'
      +'<div class="brand-breadcrumbs st-breadcrumbs">'
      +'<ul class="brand-breadcrumbs_list st-breadcrumbs_list">'
      +'<li class="st-breadcrumbs_item"><a href="/">ホーム</a></li>'
      +'</ul>'
      +'</div>'
      +'<div class="brand-head_inner">'
      +'<div class="brand-head_content u-clearfix">'
      +'<div class="brand-head_detail u-clearfix">'
      +'<h1 class="brand-head_title"><a href="/brand/yoshihide_muroya/">YOSHIHIDE MUROYA</a></h1>'
      +'<button class="brand-head_button">select</button>'
      +'</div>'
      +'<div class="brand-head_control">'
      +'<ul class="brand-category_list">'

      +'<li class="brand-category_item ln-top" data-category-third="top">'
      +'<a class="brand-category_link" href="/brand/yoshihide_muroya/"><span class="brand-category_text">TOP</span></a>'
      
      +'<ul>'
      +'<li class="brand-category_item ln-event" data-category-third="event_topics">'
      +'<a class="brand-category_link" href="/brand/yoshihide_muroya/events/"><span class="brand-category_text">EVENTS & TOPICS</span></a>'
      +'</li>'

      +'<li class="brand-category_item ln-movie" data-category-third="movie">'
      +'<a class="brand-category_link" href="/brand/yoshihide_muroya/movie/"><span class="brand-category_text">MOVIE</span></a>'
      +'</li>'

      +'<li class="brand-category_item ln-profile">'
      +'<a class="brand-category_link js-scrollTo" data-anchor="#profile"><span class="brand-category_text">PROFILE</span></a>'
      +'</li>'
      +'</ul>'

      +'</li>'
/*
      +'<li class="brand-category_item ln-races" data-category-third="races">'
      +'<a class="brand-category_link js-scrollTo" href="/brand/yoshihide_muroya/races/2019/"><span class="brand-category_text">RACES</span></a>'
      +'</li>'
*/
      +'<li class="brand-category_item ln-red-bull">'
      +'<a class="brand-category_link js-scrollTo" href="/brand/yoshihide_muroya/redbullairrace/"><span class="brand-category_text">Red Bull Air Race</span></a>'

      +'<ul>'
      +'<li class="brand-category_item ln-about-red-bull" data-category-third="redbullairrace">'
      +'<a class="brand-category_link js-scrollTo" href="/brand/yoshihide_muroya/redbullairrace/about/"><span class="brand-category_text">ABOUT Red Bull Air Race</span></a>'
      +'</li>'

      +'<li class="brand-category_item ln-races" data-category-third="races">'
      +'<a class="brand-category_link js-scrollTo" href="/brand/yoshihide_muroya/races/2019/"><span class="brand-category_text">RACES</span></a>'
      +'</li>'
      
      +'<li class="brand-category_item ln-schedule" data-category-third="schedule">'
      +'<a class="brand-category_link js-scrollTo" href="/brand/yoshihide_muroya/redbullairrace/#schedule"><span class="brand-category_text">SCHEDULE</span></a>'
      +'</li>'
      +'</ul>'

      +'</li>'

      +'</ul>'
      +'</div>'
      +'</div>'
      +'</div>'
      +'</div>';

  var $nav = $('.brand-head');
  if ($nav.length > 0) {
    init();
  }

  function init() {
    var $nabBody = $(navHTML);
    var firstCategory = $nav.attr('data-category-first');
    var secondCategory = $nav.attr('data-category-second');
    var thirdCategory = $nav.attr('data-category-third');
    var firstData = firstCategoryData[firstCategory];
    var secondData = secondCategoryData[secondCategory];
    var thirdData = thirdCategoryData[thirdCategory];
    var breadHTHML = '';

    $nabBody.find('[data-category-first="' + firstCategory + '"]').addClass('is-selected');
    $nabBody.find('[data-category-second="' + secondCategory + '"]').addClass('is-selected');
    $nabBody.find('[data-category-third="' + thirdCategoryData + '"]').addClass('is-selected');

    if (firstData !== undefined) {
      if (secondData !== undefined) {
          if(thirdData !== undefined ){
              //第3階層の場合
              breadHTHML += '<li class="st-breadcrumbs_item"><a href="' + firstData.url + '">' + firstData.name + '</a></li>';
              breadHTHML += '<li class="st-breadcrumbs_item"><a href="' + secondData.url + '">' + secondData.name + '</a></li>';
              breadHTHML += '<li class="st-breadcrumbs_item">' + thirdData.name + '</li>';
          }
          else{
              //第2階層の場合
              breadHTHML += '<li class="st-breadcrumbs_item"><a href="' + firstData.url + '">' + firstData.name + '</a></li>';
              breadHTHML += '<li class="st-breadcrumbs_item">' + secondData.name + '</li>';
          }
      } else {
          //第１階層の場合
          breadHTHML += '<li class="st-breadcrumbs_item">' + firstData.name + '</li>';
      }

      $nabBody.find('.brand-breadcrumbs_list').append(breadHTHML);
    }

    $nav.append($nabBody);

    if (location.pathname !== '/brand/yoshihide_muroya/' && location.pathname !== '/brand/yoshihide_muroya/index.html') {
      $('.ln-top').find('a').attr('href', '/brand/yoshihide_muroya/');
      $('.ln-event').find('a').attr('href', '/brand/yoshihide_muroya/events/');
      $('.ln-profile').find('a').attr('href', '/brand/yoshihide_muroya/#profile');
      $('.ln-movie').find('a').attr('href', '/brand/yoshihide_muroya/movie/');
      $('.ln-red-bull').find('a').attr('href', '/brand/yoshihide_muroya/redbullairrace/');
      $('.ln-about-red-bull').find('a').attr('href', '/brand/yoshihide_muroya/redbullairrace/about/');
      $('.ln-races').find('a').attr('href', '/brand/yoshihide_muroya/races/2019/');
      $('.ln-schedule').find('a').attr('href', '/brand/yoshihide_muroya/redbullairrace/#schedule');
    };
  }
})(window, jQuery, _, APP);

(function(window, $, _, APP) {
  // contents navigation
  var mq = APP.module.media;
  var unit = APP.module.unit;

  var $win = $(window);
  var $contentWrap;
  var scrollLeft = 0;

  init();

  function init() {
    if (mq.currentType === 'PC') { init4PC(); }
    if (mq.currentType === 'SP') { init4SP(); }
    mq.on('PC', init4PC);
    mq.on('SP', init4SP);

    $contentWrap = $('.brand-head_wrap');

    var scrollController = new ScrollMagic.Controller();
    new ScrollMagic.Scene({
      triggerElement: '.brand-head',
      triggerHook: 'onLeave'
    })
      .setClassToggle('.brand-head', 'is-fixed')
      .on('enter', onFixScroll)
      .on('leave', offFixScroll)
      .addTo(scrollController);
  }

  function onFixScroll() {
    var windowLeft;

    // 初回の横スクロール対応
    if (mq.currentType === 'PC') {
      windowLeft = $win.scrollLeft();
      $contentWrap.css({'left': -windowLeft});
      scrollLeft = windowLeft;
    }

    $win.on('scroll.yPosition', function(){
      if (mq.currentType === 'PC') {
        windowLeft = $win.scrollLeft();
        // 横スクロール対応
        if (scrollLeft !== windowLeft) {
          $contentWrap.css({'left': -windowLeft});
          scrollLeft = windowLeft;
        }
      }
    });
  }

  function offFixScroll() {
    $win.off('scroll.yPosition');
    $contentWrap.css({'left': ''});
  }

  function init4PC() {
    $('.brand-head_button').off('click.dropdown');
  }

  function init4SP() {
    $('.brand-head_button').on('click.dropdown', onClickDropdown);
  }

  // SP用 ドロップダウンメニューの開閉
  function onClickDropdown() {
    var $target = $(this);
    var $head = $target.closest('.brand-head_detail');
    var $content = $('.brand-head_control');
    if ($target.hasClass('is-open') === true) {
      $target.removeClass('is-open');
      $head.removeClass('is-open');
      $content.removeClass('is-open');
    } else {
      $target.addClass('is-open');
      $head.addClass('is-open');
      $content.addClass('is-open');
    }
  }
}(window, jQuery, _, APP));

} // initNav

if (typeof (commonMode) === 'undefined') { // for v3 only
  initNav(window, $, _, APP);
}
