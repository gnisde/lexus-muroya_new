
(function() {
  var $ = window.jQuery;

  var currentIndex = 0;


  $('.crContent').each(function(j) {
    var n = 50;
    var _c = 0;
    var l = $(this).find('.crContent__header').find('span');
    $(this).find('.crContent__header').find('span').each(function (i) {
      if(i != l.length) {
        _c = i;

        $(this).addClass('d' + (n * _c));
      }
    })

    _c += 10;
    $(this).find('.crContent__header .bar').addClass('d' + (n * _c));

    _c += 10;
    $(this).find('.crContent__body').addClass('d' + (n * _c));
  })
  
  
  
  $(window).on('scroll', function(e){
    var sl = $(window).scrollLeft();
    $('#wrapper').css('margin-left', -sl);
    var st = $(window).scrollTop();

    $('.screen-layout').each(function () {
      var top = $(this).offset().top;


      if(top < st + $(window).height() - 50 && !$(this).hasClass('show')) {
        
        var num = 100;
        if(currentIndex == 2) num = 100;
        var len = $(this).find('.buildActor').length;

        $(this).addClass('show');
        $(this).find('.buildActor').each(function(i, el){
          setTimeout((function(el){
            return function(){
              //console.log(el)
              $(el).addClass("show");
            }
          })(el), i * num)
        })
        $(this).find('.buildActorD').each(function(i, el){
          setTimeout((function(el){
            return function(){
              //console.log(el)
              $(el).addClass("show");
            }
          })(el), len * num + i * 30)
        })
      }
    })


    $('.crContent').each(function () {
      var h = $(window).height();
      var c_top = $(this).offset().top - 100;
      var c_bottom = $(this).offset().top + $(this).height() + 400;


      if(c_top < st + h && c_bottom > st) {
        if(!$(this).hasClass('is-active')) {
          $(this).addClass('is-active');
        }
      } else {
        $(this).removeClass('is-active');
      }

      var d  = (st) - $(this).offset().top;
      
      $(this).find('.photoItem1').css({
        "transform" : "translate3d(0, "  + (-d * 0.02) + "px, 0)",
        "-webkit-transform" : "translate3d(0, "  + (-d * 0.02) + "px, 0)"
      })
      

      $(this).find('.photoItem2').css({
        "transform" : "translate3d(0, "  + (d * 0.15) + "px, 0)",
        
        "-webkit-transform" : "translate3d(0, "  + (d * 0.15) + "px, 0)"
      })
      
      $(this).find('.photoItem3').css({
        "transform" : "translate3d(0, "  + (d * 0.05) + "px, 0)",

        
        "-webkit-transform" : "translate3d(0, "  + (d * 0.05) + "px, 0)"
      })
    })


    var ct1 = $('#content').offset().top - $(window).height() * 0.5;
    var ct2 = $('#appearance').offset().top - $(window).height() * 0.5;
    if(st < ct1) {
      var o = st / (ct1);
      $('#black').css('opacity', o * 0.6)
    } else {
      var o = (st - ct1) / (ct2-ct1);
      $('#black').css('opacity', o * 0.4 + 0.6)

    }
  }).trigger('scroll')


  objectFitImages('.ofi');

  $(window).on('resize', function(){
    var _h = 80 + 72;
    if($(window).width() < 768) _h = 44 + 72;
    $('#section01').css('height', window.innerHeight - _h)

    var w = $(window).width();
    var h = $(window).height();
    var s = 1/0.75;
    var p = (16/0.75)/9;
    if(w/h > p) {
      $('.video-foreground').css({
        width : w * s,
        height : w * s,
        top : -(w*s - h) * 0.5,
        left : -(w*s - w) * 0.5
      });
    } else {
      $('.video-foreground').css({
        width : h * s * p,
        height : h * s,
        left : -(h * s * p - w) * 0.5,
        top : -(h * s - h) * 0.5
      });
    }
  }).trigger('resize')



  var ua = navigator.userAgent;
  var playableInlineVideo = false;

  //Androidの判定
  if (/Android/.test(ua)) {
      //標準ブラウザかどうか判定
      if ((/Android/.test(ua) && /Linux; U;/.test(ua) && !/Chrome/.test(ua)) || (/Android/.test(ua) && /Chrome/.test(ua) && /Version/.test(ua)) || (/Android/.test(ua) && /Chrome/.test(ua) && /SamsungBrowser/.test(ua))) {
          playableInlineVideo = false;
      } else {
          //Chrome for Androidのバージョン取得
          var chromever = ua.match(/Chrome\/([\d]+)/).slice(1);
          //53以上か判定
          if(chromever >= 53){
              playableInlineVideo = true;
          }
      }
  //iPhone-iPad-iPodの判定
  } else if (/iP(hone|od|ad)/.test(ua)) {
      //iOSバージョンの取得
      //alert(ua)
      console.log(ua)
      try{
        var iOSver = ua.match(/OS ([\d]+)/).slice(1)[0];
        console.log(iOSver)
        //iOS10以上か判定
        if(iOSver >= 10){
            playableInlineVideo = true;
        }
      }catch(e){

      }
  //いずれにも該当しない場合
  } else {
    // PC
    playableInlineVideo = true;
  }


  //if(playableInlineVideo) {
    //
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  //} else {
    
  //}

  
})();




function onYouTubeIframeAPIReady() {
  var player;
  var videoId = $('#youtube').attr('data-youtubeId');

  var onPlayerReady = function(event) {
    event.target.mute();
    event.target.playVideo();
  };
  var onPlayerStateChange = function(event) {
      //player.mute();
      if (event.data == YT.PlayerState.ENDED) {
          player.playVideo();
      }
      if (event.data == YT.PlayerState.PLAYING) {

      }
  };
  var initPlayer = function(){
      player = new YT.Player('youtube', {
          videoId: videoId,
          width: '1280',
          height: '719',
          playerVars: {
            rel:0,
            playsinline:1,
            controls: 0,
            autoplay: 1,
            origin: location.host,
            modestbranding:1,
            showinfo: 0
          },
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
      });
  };

  initPlayer();

}