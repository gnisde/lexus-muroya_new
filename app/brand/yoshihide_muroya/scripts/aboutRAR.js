$(function () {

  $(window).on('scroll', function () {
    var wt = $(window).scrollTop();
    var tp = $('.content-wrapper').offset().top;
    var h = 50;
    if($(window).width() < 768) {
      h = 40;
    }
    if(wt + (72) <= tp - h) {
      $('.content-anchors').removeClass('fixed');
      $('.content-anchors').css("left",0);
    } else {
      $('.content-anchors').addClass('fixed');
      $('.content-anchors').css("left",-$(window).scrollLeft());
    }
  }).trigger('scroll')

  $('.content-anchors a').each(function() {
    $(this).on('click', function(e) {
      $('html,body').animate({
        scrollTop : $($(this).attr('href')).offset().top
      }, 200)
      e.stopPropagation();
      e.preventDefault();
      return;
    })
  })


  $('.begginer').each(function () {
    $(this).find('.begginer__header').on('click', function() {
      $(this).parent().addClass('is-active');
    })

    $(this).find('.begginer__body .header').on('click', function() {
      $(this).parent().parent().removeClass('is-active');
    })
  })

})