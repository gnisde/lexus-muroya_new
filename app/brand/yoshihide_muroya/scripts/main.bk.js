
(function() {
  var $ = window.jQuery;

  var WHEEL_EVENT = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
  var currentIndex = 0;
  var bool = false;
  var pScrollY = 0;
  var pBottomCoords = false;
  var pTopCoords = false;
  var mm = 0;
  var intervalID = setInterval(function(){
    mm += (0 - mm) * 0.4;
  }, 1000/60)
  var mw = 0;
  var mc = 0;
  $(document).on('mousewheel', function(e){
    if(bool) return;
    var deltaY = e.originalEvent.wheelDeltaY;
    if(!deltaY) deltaY = e.originalEvent.wheelDelta;
    var current = $('.screen-layout').get(currentIndex);
    var isFixed = $(current).hasClass('screen-layout--fixed');

    var scrollAble = false;
    var _scrollAble = false;

    var pt = 0;
    if(!isFixed){
      pt = 72;
    }

    var isTopCoords = false;
    var isBottomCoords = false;
      

    if(isFixed){
      scrollAble = true;
      isTopCoords = true;
      isBottomCoords = true;
    }else{
      var _bst = $('.st-main').scrollTop();
      var _y = _bst - deltaY * 0.5;
      $('.st-main').scrollTop(_y);
      
      if($(current).offset().top > 0) {
        _y += $(current).offset().top;
        isTopCoords = true;
      }else if($('.st-main').scrollTop() > $('.st-main').scrollTop() + $(current).offset().top + $(current).find('.screen-layout__inner').height() + pt - window.innerHeight){
        _y = $('.st-main').scrollTop() + $(current).offset().top + $(current).find('.screen-layout__inner').height() + pt - window.innerHeight;
        isBottomCoords = true;
      }
      $('.st-main').scrollTop(_y);

      if(isTopCoords || isBottomCoords){
        _scrollAble = true;
      }
      if(_scrollAble){
        if(isTopCoords && deltaY >= 120){
          scrollAble = true;
        }
        if(isBottomCoords && deltaY <= -120){
          scrollAble = true;
        }
        _scrollAble = false;
      }
    }

    mw += Math.abs(deltaY);
    mc ++;
    
    if(Math.abs(deltaY) < (mw/mc)) scrollAble = false;
    console.log(deltaY, (mw/mc))

    if(Math.abs(deltaY) > Math.abs(pScrollY)){
      mm = Math.abs(deltaY);
    }else{
      scrollAble = false;
    }

    var checkBottomCoords = (isBottomCoords == pBottomCoords);
    var checkTopCoords = (isTopCoords == pTopCoords);
    
    pBottomCoords = isBottomCoords;
    pTopCoords = isTopCoords;
    

    pScrollY = deltaY;

    if(scrollAble && !bool){
      if(deltaY <= -120){
        if(currentIndex == $('.screen-layout').length - 1){
        }else{
          if(!isFixed && !checkBottomCoords){
            return;
          }
          bool = true;
          // bottom
          var st = $('.st-main').scrollTop();
          var top = $($('.screen-layout').get(currentIndex + 1)).offset().top + st;
          console.log(top, $($('.screen-layout').get(currentIndex + 1)).offset().top)
          $('.st-main').delay(100).stop().animate({
            scrollTop : top
          }, {
            duration : 400,
            easing: "swing",
            complete: function(){
              if(bool){
                currentIndex++;
                showCurrentContent();
                setTimeout(function(){
                  console.log(currentIndex, "_")
                  bool = false;
                }, 800)
              }
            }
          })
          e.stopPropagation();
          e.preventDefault();
          return false;
        }
      }else if(deltaY >= 120){
        if(currentIndex == 0){

        }else{
          console.log(Math.ceil($(current).scrollTop()) + " -")
          if(!isFixed && !checkTopCoords){
            return;
          }
          bool = true;
          console.log(currentIndex)
          // top
          var st = $('.st-main').scrollTop();
          var top = $($('.screen-layout').get(currentIndex - 1)).offset().top + ($($('.screen-layout').get(currentIndex- 1)).height() - window.innerHeight) + st;
          $('.st-main').delay(100).stop().animate({
            scrollTop : top
          }, {
            duration : 400,
            easing: "swing",
            complete: function(){
              if(bool){
                currentIndex--;
                showCurrentContent();
                setTimeout(function(){
                  bool = false;
                }, 800)
              }
            }
          })
          e.stopPropagation();
          e.preventDefault();
          return false;
        }
      }else{
        e.stopPropagation();
        e.preventDefault();
        return false;
      }
    }else{
      return;
    }
    return;
  })

  var isTouch = false;
  var _isTouch = false;
  var pY = 0;
  var dY = 0;
  var thresholdY = 40;

  var TOUCH_START = 'mousedown';
  var TOUCH_MOVE = 'mousemove';
  var TOUCH_END = 'mouseup';
  var TOUCH_TARGET = window;
  if(typeof document.ontouchstart != "undefined"){
    var TOUCH_START = 'touchstart';
    var TOUCH_MOVE = 'touchmove';
    var TOUCH_END = 'touchend';
  }

  var dt = 0;
  var startTouchY = 0;


  $(TOUCH_TARGET).on(TOUCH_START, function(e){
    var point  = e.originalEvent.touches ? e.originalEvent.touches[0] : e.originalEvent;
    
    pY = point.pageY;
    dY = 0;
    var current = $('.screen-layout').get(currentIndex);
    var isFixed = $(current).hasClass('screen-layout--fixed');
    _isTouch = true;

    dt = (new Date()).getTime();
    startTouchY = point.pageY;
  })
  $(TOUCH_TARGET).on(TOUCH_MOVE, function(e){
    if(bool) return;
    var point  = e.originalEvent.touches ? e.originalEvent.touches[0] : e.originalEvent;
    var current = $('.screen-layout').get(currentIndex);
    var isFixed = $(current).hasClass('screen-layout--fixed');
    var d = (point.pageY) - pY;
    var deltaY = d;
    pY = (point.pageY);
    var pt = 0;
    if(!isFixed){
      pt = 72;
    }

    if(_isTouch){
      if(!isFixed){
        var _bst = $('.st-main').scrollTop();
        var _y = _bst - deltaY;
        $('.st-main').scrollTop(_y);
        
        var isTopCoords = false;
        var isBottomCoords = false;
        
        if($(current).offset().top > 0) {
          _y += $(current).offset().top;
          isTopCoords = true;
        }else if($('.st-main').scrollTop() > $('.st-main').scrollTop() + $(current).offset().top + $(current).find('.screen-layout__inner').height() + pt - window.innerHeight){
          _y = $('.st-main').scrollTop() + $(current).offset().top + $(current).find('.screen-layout__inner').height() + pt - window.innerHeight;
          isBottomCoords = true;
        }
        
        $('.st-main').scrollTop(_y);

        if(isTopCoords && deltaY >= thresholdY){
          isTouch = true;
        }
        if(isBottomCoords && deltaY <= -thresholdY){
          isTouch = true;
        }
      }else{
        isTouch = true;
      }
    }

    e.stopPropagation();
    e.preventDefault();
    

    if(isTouch){
      if(deltaY < -thresholdY && currentIndex != $('.screen-layout').length - 1){
        if(currentIndex == $('.screen-layout').length - 1){
          isTouch = false;
        }else{
          if(!isFixed && !isBottomCoords){
            return;
          }
          _isTouch = false;
          bool = true;
          // bottom
          var st = $('.st-main').scrollTop();
          var top = $($('.screen-layout').get(currentIndex + 1)).offset().top + st;
          
          $('.st-main').delay(100).stop().animate({
            scrollTop : top
          }, {
            duration : 400,
            easing: "swing",
            complete: function(){
              if(bool){
                currentIndex++;
                showCurrentContent();
                setTimeout(function(){
                  bool = false;
                  _isTouch = false;
                }, 800)
              }
            }
          })
          return false;
        }
      }else if(deltaY > thresholdY && currentIndex != 0){
        if(currentIndex == 0){
          isTouch = false;

        }else{
          console.log(Math.ceil($(window).scrollTop()) + " -")
          if(!isFixed && !isTopCoords){
            return;
          }
          _isTouch = false;
          bool = true;
          console.log(currentIndex)
          // top
          var st = $('.st-main').scrollTop();
          var top = $($('.screen-layout').get(currentIndex - 1)).offset().top + ($($('.screen-layout').get(currentIndex- 1)).height() - window.innerHeight) + st;
          
          $('.st-main').delay(100).stop().animate({
            scrollTop : top
          }, {
            duration : 400,
            easing: "swing",
            complete: function(){
              if(bool){
                currentIndex--;
                showCurrentContent();
                setTimeout(function(){
                  bool = false;
                  _isTouch = false;
                }, 800)
              }
            }
          })
          return false;
        }
      }
      return false;
    }
    return false;
  })
  $(window).on(TOUCH_END, function(e){
    var ct = (new Date()).getTime();
    if(ct - dt < 1000){
      var DY = pY - startTouchY;

    }
    isTouch = false;
    _isTouch = false;
    pY = 0;
    dY = 0;
  })

  $('.brand-category_link,.anchorLink').each(function(i){
    $(this).on("click", (function(index){
      return function(){
        var href = $(this).attr("data-anchor");
        if(href == "#wrapper") href = "#section01";
        var _index = 0;
        $('.screen-layout').each(function(i){
          if('#' + $(this).attr("id") == href){
            _index = i;
          }
        })
        
        var st = $('.st-main').scrollTop();
        var top = $(href).offset().top + st;

        $('.st-main').delay(100).stop().animate({
          scrollTop : top
        }, {
          duration : 400,
          easing: "swing",
          complete: function(){
            currentIndex=_index;
            bool = false;
            _isTouch = false;
            showCurrentContent();
          }
        })
      }
    })(i))
  })

  var showCurrentContent = function(){
    $('#contentNavigation a').removeClass("selected");
    $($('#contentNavigation a').get(currentIndex)).addClass("selected");
    
    $('.buildActor').removeClass("show");
    $('.buildActorD').removeClass("show");
    //$($('.screen-layout').get(currentIndex)).find('.buildActor').addClass('show');
    var num = 100;
    if(currentIndex == 2) num = 100;
    var len = $($('.screen-layout').get(currentIndex)).find('.buildActor').length;
    

    $($('.screen-layout').get(currentIndex)).find('.buildActor').each(function(i, el){
      setTimeout((function(el){
        return function(){
          console.log(el)
          $(el).addClass("show");
        }
      })(el), i * num)
    })
    $($('.screen-layout').get(currentIndex)).find('.buildActorD').each(function(i, el){
      setTimeout((function(el){
        return function(){
          console.log(el)
          $(el).addClass("show");
        }
      })(el), len * num + i * 30)
    })
  }

  window.onhashchange = function(e){
    hashChanged();
    e.preventDefault();
    return false;
  }

  

  setTimeout(function(){
    $('.container').show();
    hashChanged();
  }, 100)
  

  //setTimeout(function(){

  var hashChanged = function(){
    $('html,body').scrollTop(0);

    href = location.hash;
    //console.log(href)
    if(href == "#wrapper") href = "#section01";
    var _index = 0;

    $('.screen-layout').each(function(i){
      //console.log(i, $(this).attr("id"))
      console.log($(this).attr("id"))
      if('#' + $(this).attr("id") == href){
        _index = i;
      }
    })
    console.log(_index)
    
    if(_index != 0){
      var st = $('.st-main').scrollTop();
      var top = $(href).offset().top + st;
      console.log(top)
    }else{
      var st = $('.st-main').scrollTop();
      var top = 0;
    }

    $('.st-main').stop().animate({
      scrollTop : top
    }, {
      duration : 0,
      easing: "swing",
      complete: function(){
          currentIndex=_index;
          bool = false;
          _isTouch = false;
          showCurrentContent();
        //}
      }
    })
  }
  
  $(window).on("scroll", function(){
    $('.st-topAnchor_button').show();
  })
  $(".st-topAnchor_button").on('mousedown', function(){
    $('.st-main').stop().animate({
      scrollTop : 0
    }, {
      duration : 400,
      easing: "swing",
      complete: function(){
        currentIndex = 0;
        showCurrentContent();
      }
    })
  })
  $(".st-footer_additionalTopanchor").on('mousedown', function(){
    $('.st-main').stop().animate({
      scrollTop : 0
    }, {
      duration : 400,
      easing: "swing",
      complete: function(){
        currentIndex = 0;
        showCurrentContent();
      }
    })
    return false;
  })


  objectFitImages('.ofi');

  $(window).on('resize', function(){
    //
    var current = $('.screen-layout').get(currentIndex);
    var st = $('.st-main').scrollTop();
    var dy = $(current).offset().top;
    var top = $($('.screen-layout').get(currentIndex)).offset().top + st - dy;
    var _y = top;
    if(!$(current).hasClass('screen-layout--fixed')){
      $('.st-main').scrollTop(top);

      if($(current).offset().top > 0) {
        _y += $(current).offset().top;
        isTopCoords = true;
      }else if($('.st-main').scrollTop() > $('.st-main').scrollTop() + $(current).offset().top + $(current).find('.screen-layout__inner').height() - window.innerHeight){
        _y = $('.st-main').scrollTop() + $(current).offset().top + $(current).find('.screen-layout__inner').height() - window.innerHeight;
        isBottomCoords = true;
      }
      $('.st-main').scrollTop(_y);

    }else{
      var top = $($('.screen-layout').get(currentIndex)).offset().top + st;
      if($(current).offset().top > 0) {
        _y += $(current).offset().top;
        isTopCoords = true;
      }
      $('.st-main').scrollTop(top);
    }
  })
  $(window).on('scroll', function(e){
    var sl = $(window).scrollLeft();
    $('#wrapper').css('margin-left', -sl);
    var st = $(window).scrollTop();
  })

  $('.scroller.prev').each(function(i, el){

    $(el).on("click", function(){
      bool = true;
      console.log(currentIndex)
      var st = $('.st-main').scrollTop();
      var top = $($('.screen-layout').get(currentIndex - 1)).offset().top + ($($('.screen-layout').get(currentIndex- 1)).height() - window.innerHeight) + st;
      $('.st-main').delay(100).stop().animate({
        scrollTop : top
      }, {
        duration : 400,
        easing: "swing",
        complete: function(){
          if(bool){
            currentIndex--;
            bool = false;
            _isTouch = false;
            showCurrentContent();
          }
        }
      })
    })
    

  })

  $('.scroller.next').each(function(i, el){

    $(el).on("click", function(){
      bool = true;
      // bottom
      var st = $('.st-main').scrollTop();
      var top = $($('.screen-layout').get(currentIndex + 1)).offset().top + st;
      console.log(top, $($('.screen-layout').get(currentIndex + 1)).offset().top)
      $('.st-main').delay(100).stop().animate({
        scrollTop : top
      }, {
        duration : 400,
        easing: "swing",
        complete: function(){
          if(bool){
            currentIndex++;
            bool = false;
            _isTouch = false;
            showCurrentContent();
          }
        }
      })
    })
    

  })


  var mX = 0;
  var mY = 0;
  
  ///*
  if(location.hash == '#move') {
    $(window).on('mousemove', function(e){
      var event = e.originalEvent;
      mX = event.pageX;
      mY = event.pageY;

    })
    setInterval(function(){
      $($('.screen-layout').get(currentIndex)).find('.buildActor').each(function(i, el){
        var dx = ($(window).width()*0.5) - mX;//$(el).offset().left - mX;
        var dy = ($(window).height()*0.5) - mY;//$(el).offset().top - mY;
        $(el).css({
          "transform": "rotateY("+((-dx)/($(window).width()*0.5)) * 20 +"deg) " + "rotateX("+((-dy)/($(window).height()*0.5)) * 20 +"deg) translate3d(" + dx * 0.012 + "px," + dy * 0.12 + "px,0)",
          
        })
      })
      $($('.screen-layout').get(currentIndex)).find('.buildActorD').each(function(i, el){
        /*
        var dx = $(el).offset().left - mX;
        var dy = $(el).offset().top - mY;
        */
        var dx = ($(window).width()*0.5) - mX;//$(el).offset().left - mX;
        var dy = ($(window).height()*0.5) - mY;//$(el).offset().top - mY;
        $(el).css({
          "transform": "rotateY("+((-dx)/($(window).width()*0.5)) * 20 +"deg) " + "rotateX("+((-dy)/($(window).height()*0.5)) * 20 +"deg) translate3d(" + dx * 0.06 + "px," + dy * 0.06 + "px,0)",
          
        })
      })
    },1000/30)
  //*/
  }else if(location.hash == '#move2') {
    /*
    $(window).on('mousemove', function(e){
      var event = e.originalEvent;
      mX = event.pageX;
      mY = event.pageY;

    })*/
    var angle1 = 0;
    var angle2 = 0;
    
    setInterval(function(){
      angle1 += 2;
      angle2 += 1;
      mX = Math.cos(angle1 * Math.PI/180) * 300;
      mY = Math.sin(angle2 * Math.PI/180) * 120;
      $($('.screen-layout').get(currentIndex)).find('.buildActor').each(function(i, el){
        var dx = mX;//$(el).offset().left - mX;
        var dy = mY;//$(el).offset().top - mY;
        $(el).css({
          "transform": "rotateY("+((-dx)/(1000*0.5)) * 20 +"deg) " + "rotateX("+((-dy)/(1000*0.5)) * 20 +"deg) translate3d(" + dx * 0.012 + "px," + dy * 0.12 + "px,0)",
          
        })
      })
      $($('.screen-layout').get(currentIndex)).find('.buildActorD').each(function(i, el){
        /*
        var dx = $(el).offset().left - mX;
        var dy = $(el).offset().top - mY;
        */
        var dx = mX;//$(el).offset().left - mX;
        var dy = mY;//$(el).offset().top - mY;
        $(el).css({
          "transform": "rotateY("+((-dx)/(1000*0.5)) * 20 +"deg) " + "rotateX("+((-dy)/(1000*0.5)) * 20 +"deg) translate3d(" + dx * 0.06 + "px," + dy * 0.06 + "px,0)",
          
        })
      })
    },1000/30)
  //*/
  }


  
})();
