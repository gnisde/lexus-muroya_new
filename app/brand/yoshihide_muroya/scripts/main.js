
(function() {
  var $ = window.jQuery;

  var WHEEL_EVENT = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
  var currentIndex = 0;
  var bool = false;
  var pScrollY = 0;
  var pBottomCoords = false;
  var pTopCoords = false;
  var mm = 0;
  var intervalID = setInterval(function(){
    mm += (0 - mm) * 0.4;
  }, 1000/60)
  var mw = 0;
  var mc = 0;
  var WHEEL_DELTA = 0;


  
var __cookie = {
  getExpDate : function(a, c, d) {
    var b = new Date;
    if ("number" == typeof a && "number" == typeof c && "number" == typeof d) return b.setDate(b.getDate() + parseInt(a)), b.setHours(b.getHours() + parseInt(c)), b.setMinutes(b.getMinutes() + parseInt(d)), b.toGMTString()
  },
  setItem : function(a, c, d, b, e, f) {
    document.cookie = a + "=" + escape(c) + (d ? "; expires=" + d : "") + (b ? "; path=" + b : "") + (e ? "; domain=" + e : "") + (f ? "; secure" : "")
  },
  getItem : function(a) {
    a += "=";
    for (var c = a.length, d = document.cookie.length, b = 0; b < d;) {
      var e = b + c;
      if (document.cookie.substring(b, e) == a) return a = e, c = document.cookie.indexOf(";", a), -1 == c && (c = document.cookie.length), unescape(document.cookie.substring(a, c));
      b = document.cookie.indexOf(" ", b) + 1;
      if (0 == b) break
    }
    return "";
  },
  removeItem : function (a, c, d) {
    __cookie.getItem(a) && (document.cookie = a + "=" + (c ? "; path=" + c : "") + (d ? "; domain=" + d : "") + "; expires=Thu, 01-0Jan-70 00:00:01 GMT")
  }
}


  $('.brand-category_link,.anchorLink').each(function(i){
    $(this).on("click", (function(index){
      return function(){
        clearInterval(moveIntervalID);
        var href = $(this).attr("data-anchor");
        if(href == "#wrapper") href = "#top";
        var _index = 0;
        $('.screen-layout').each(function(i){
          if('#' + $(this).attr("id") == href){
            _index = i;
          }
        })
        if(currentIndex == _index) return;
        var st = $('.st-main').scrollTop();
        var top = $(href).offset().top + st;

        $('.st-main').delay(100).stop().animate({
          scrollTop : top
        }, {
          duration : 400,
          easing: "swing",
          complete: function(){
            currentIndex=_index;
            bool = false;
            _isTouch = false;
            showCurrentContent();
          }
        })
      }
    })(i))
  })

  var showCurrentContent = function(){
    clearInterval(moveIntervalID);
    $('#contentNavigation a').removeClass("selected");
    $($('#contentNavigation a').get(currentIndex)).addClass("selected");
    
    $('.buildActor').removeClass("show");
    $('.buildActorD').removeClass("show");
    var num = 100;
    if(currentIndex == 2) num = 100;
    var len = $($('.screen-layout').get(currentIndex)).find('.buildActor').length;

    
  }

  
  $(window).on('scroll', function(e){
    var sl = $(window).scrollLeft();
    //$('#wrapper').css('margin-left', -sl);
    var st = $(window).scrollTop();

    $('.screen-layout').each(function () {
      var top = $(this).offset().top;


      if(top < st + $(window).height() - 200 && !$(this).hasClass('show')) {
        
        var num = 100;
        if(currentIndex == 2) num = 100;
        var len = $(this).find('.buildActor').length;

        $(this).addClass('show');
        $(this).find('.buildActor').each(function(i, el){
          setTimeout((function(el){
            return function(){
              //console.log(el)
              $(el).addClass("show");
            }
          })(el), i * num)
        })
        $(this).find('.buildActorD').each(function(i, el){
          setTimeout((function(el){
            return function(){
              //console.log(el)
              $(el).addClass("show");
            }
          })(el), len * num + i * 30)
        })
      }
    })
  }).trigger('scroll')

  
  var hashChanged = function(){
    //clearInterval(moveIntervalID);
    $('html,body').scrollTop(0);

    href = location.hash;
    //console.log(href)
    if(href == "#wrapper") href = "#top";
    var _index = 0;

    $('.screen-layout').each(function(i){
      //console.log(i, $(this).attr("id"))
      console.log($(this).attr("id"))
      if('#' + $(this).attr("id") == href){
        _index = i;
      }
    })
    //console.log(_index)
    
    if(_index != 0){
      var st = $('.st-main').scrollTop();
      var top = $(href).offset().top + st;
      //console.log(top)
    }else{
      var st = $('.st-main').scrollTop();
      var top = 0;
    }

    $('.st-main').stop().animate({
      scrollTop : top
    }, {
      duration : 0,
      easing: "swing",
      complete: function(){
          currentIndex=_index;
          bool = false;
          _isTouch = false;
          showCurrentContent();
        //}
      }
    })
  }
  

  objectFitImages('.ofi');

  $(window).on('resize', function(){
    var _h = 80 + 72;
    if($(window).width() < 768) _h = 44 + 72;
    var _wh = window.innerHeight - _h

    if($(window).width() < 768) {
      if(_wh <  452) {
        _wh = 452;
        $('#top').css('height', _wh)
      } else {
        $('#top').css('height', _wh)
      }
    } else {
      if(_wh <  640) _wh = 640;
      $('#top').css('height', _wh)
    }

    var w = $('#top').width();
    var h = $('#top').height();
    var s = 1/0.75;
    var p = (16/0.75)/9;
    if(w/h > p) {
      $('.video-foreground').css({
        width : w * s,
        height : w * s,
        top : -(w*s - h) * 0.5,
        left : -(w*s - w) * 0.5
      });
    } else {
      $('.video-foreground').css({
        width : h * s * p,
        height : h * s,
        left : -(h * s * p - w) * 0.5,
        top : -(h * s - h) * 0.5
      });
    }
  }).trigger('resize')



  var ua = navigator.userAgent;
  var playableInlineVideo = false;

  //Androidの判定
  if (/Android/.test(ua)) {
      //標準ブラウザかどうか判定
      if ((/Android/.test(ua) && /Linux; U;/.test(ua) && !/Chrome/.test(ua)) || (/Android/.test(ua) && /Chrome/.test(ua) && /Version/.test(ua)) || (/Android/.test(ua) && /Chrome/.test(ua) && /SamsungBrowser/.test(ua))) {
          playableInlineVideo = false;
      } else {
          //Chrome for Androidのバージョン取得
          var chromever = ua.match(/Chrome\/([\d]+)/).slice(1);
          //53以上か判定
          if(chromever >= 53){
              playableInlineVideo = true;
          }
      }
  //iPhone-iPad-iPodの判定
  } else if (/iP(hone|od|ad)/.test(ua)) {
      //iOSバージョンの取得
      //alert(ua)
      try{
        var iOSver = ua.match(/OS ([\d]+)/).slice(1)[0];
        console.log(iOSver)
        //iOS10以上か判定
        if(iOSver >= 10){
            playableInlineVideo = true;
        }
      }catch(e){

      }
  //いずれにも該当しない場合
  } else {
    // PC
    playableInlineVideo = true;
  }

  if(playableInlineVideo) {
    //
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  } else {
    $('#youtube').append('<img src="images/top_crossing.gif" />')
  }

  
})();




function onYouTubeIframeAPIReady() {
  var player;
  var videoId = $('#youtube').attr('data-youtubeId');

  var onPlayerReady = function(event) {
    event.target.mute();
    event.target.playVideo();
  };
  var onPlayerStateChange = function(event) {
      if (event.data == YT.PlayerState.ENDED) {
          player.playVideo();
      }
      if (event.data == YT.PlayerState.PLAYING) {

      }
  };
  var initPlayer = function(){
      player = new YT.Player('youtube', {
          videoId: videoId,
          width: '1280',
          height: '719',
          playerVars: {
            'wmode': 'transparent',
            'autoplay': 1,
            'controls': 0,
            'muted' : 1,
            'rel': 0,
            'showinfo': 0,
            playsinline:1,
            'loop' : 1
          },
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
      });
  };

  initPlayer();

}