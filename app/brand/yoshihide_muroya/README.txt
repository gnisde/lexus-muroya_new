

★共通読み込みファイル
---------------------------------

■<head>


<link rel="stylesheet" href="/common/v3/css/app.css"><link rel="stylesheet" href="/brand/yoshihide_muroya/css/nav.css">

<script type="text/javascript" src="/common/v3/js/s_webbeacon.js"></script>
<script type="text/javascript" src="/common/v3/js/sc_click.js"></script>
<script src="/common/v3/js/head.js"></script>
---------------------------------

■<body>

<script src="/common/v3/js/bundle.js"></script>
<script src="/common/v3/js/app.js"></script>
<script src="/brand/yoshihide_muroya/js/nav.js"></script>

<script type="text/javascript">
<!--
  SCoutput_bc();
//-->
</script>

---------------------------------


★ヘッダー、フッター出力タグ
---------------------------------

■ヘッダー

<header class="st-header"></header>

---------------------------------

■フッター

<footer class="st-footer"></footer>

---------------------------------
