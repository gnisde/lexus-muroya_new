/**
 * main.js
 *
 * - include and header and footer append
 * - hide address bar for iPhone
 * - to smartphone
 * - Set viewport for iPad
 * - Set Mode
 * - scroll to content and scroll to page top
 * - slide toggle more contents
 * - rollover clip
 * - lightbox for iframe youtube video and etc
 * - lightbox for gallery
 * - colorbox - Overlay Close for #cboxContent margin
 * - fixed table's thead
 * - tooltip
 * - add class for table first row
 * - spec table show toggle
 * - spec table cell highlight
 * - visual fixed width
 * - rollover (.rollover)
 * - tabs toggle
 * - accordion toggle
 * - tab-01 toggle
 * - content toggle
 * - Font Detector
 * - Fix PNG alpha for IE6
 * - News Table
 * - No Scroll Wrapper
 * - No Fixed for Capture
 * - Beacon
 * - Google Tag Manager
 */
(function() {
    $('#header').addClass('st-header').attr('id', '');
    $('#footer').addClass('st-footer u-mt-m').attr('id', '');
    $('#main').addClass('st-main');
    $('.to-page-top').hide();
}());

var viewMode;
var v2OTbodyLoaded;

(function($, requirejs, WINDOW_APP) {
    /**
     * Touch and Mouse Browser
     */
    Modernizr.touch_exclude_mouse = (function() {
        if (Modernizr.touch == false) {
            return false;
        }
        var ua = navigator.userAgent.toLowerCase();
        if (ua.indexOf('iphone') > -1 || ua.indexOf('ipad') > -1 ||
            ua.indexOf('ipod') > -1 || ua.indexOf('android') > -1) {
            return true;
        }
        return false;
    })();

    // variables
    var $body = $('body');
    var includeJsFiles;
    var i;
    var args = "v=" + "20160601";

    // config requirejs
    requirejs.config({
        baseUrl: '/',
        urlArgs: args,
        waitSeconds: 30,
        paths: {
            'common': 'common/v2/js',
            'common/inc': 'common/v2/js/inc'
        }
    });

    // header, footer
    //includeJsFiles = ['common/inc/header', 'common/inc/footer'];

    // analytics
    //includeJsFiles.push('common/analytics');
    includeJsFiles = ['common/analytics'];

    $(function() {
        /**
         * include and header and footer append
         */
        requirejs(includeJsFiles, function(header, footer) {
            var INIT_DURATION = 500;
            var $wrapper = $('#wrapper');
            var $header = $wrapper.find('#header');
            var $localNav = $wrapper.find('#local-nav');
            var $footer = $wrapper.find('#footer');

            if ($header.length > 0) {
                $header.append(header.html);
                if (header.onAppendComplete) {
                    if ($body.hasClass('responsive')) {
                        $('head').append('<link rel="stylesheet" href="/smp/common/css/menu.css?20160425">');
                        $('head').append('<link rel="stylesheet" href="/smp/common/css/flexslider.css">');
                        $('<script src="/smp/common/js/iscroll.js"></script>').appendTo('body');
                        $('<script src="/smp/common/js/flipsnap.js"></script>').appendTo('body');
                        $('<script src="/smp/common/js/inc/menu.js?20160425"></script>').appendTo('body');
                        $('<script src="/smp/common/js/inc/footer.js?20160425"></script>').appendTo('body');
                    }
                    header.onAppendComplete();
                }
            }
            if ($footer.length > 0) {
                $footer.append(footer.html);
                window.setTimeout(function() {
                    footer.onAppendComplete();
                }, 1000);
            }
        });
    });

    /**
     * hide address bar for iPhone
     */
    //if (Modernizr.touch) {
    var ua = navigator.userAgent.toLowerCase();
    if ((ua.indexOf('iphone') == -1) && (ua.indexOf('ipod') == -1) &&
        (ua.indexOf('android') > -1 && ua.indexOf('mobile') > -1) == true) {
        if (window.location.hash.length === 0) {
            window.onload = function(){
                window.setTimeout("scrollTo(0,1)", 100);
            };
        }
    }
    /**
     * to smartphone
     */
    (function() {
        var ua = navigator.userAgent.toLowerCase();
        if ((ua.indexOf('iphone') == -1) && (ua.indexOf('ipod') == -1) &&
            (ua.indexOf('android') > -1 && ua.indexOf('mobile') > -1) == false) {
            return;
        }
        var getCookies = function() {
            var result = {};
            var allcookies = document.cookie;
            if( allcookies != '' ) {
                var cookies = allcookies.split('; ');
                for( var i = 0; i < cookies.length; i++ ) {
                    var cookie = cookies[ i ].split( '=' );
                    result[ cookie[ 0 ] ] = cookie[ 1 ];
                }
            }
            return result;
        };
        var cookieName = 'pc_only';
        var cookies = getCookies();
        if (window.location.search.search(/smp_to_pc/i) > -1 &&
            typeof cookies[cookieName] == 'undefined') {
            // Cookie のセット
            var t = new Date();
            t.setDate(t.getDate() + 1); // 1 day
            document.cookie = [
                cookieName, '=', '1;',
                'expires=', t.toUTCString(), ';',
                'path=/;'
            ].join('');
        }

        var modelsTops = [
            'ls',
            'gs',
            'gsf',
            'is',
            'hs',
            'rc',
            'rcf',
            'ct',
            'lx',
            'rx',
            'nx',
            'f',
            'lfa',
            'suv'
        ];

        var target = (function() {
            var pathname = window.location.pathname;
            if (pathname.search(/^\/(index\.html|)$/) > -1) {
                return 'top';
            }
            if (pathname.search(/^\/models\/(\w+)\/(index\.html|)$/) > -1) {
                var flag=false;
                $.each(modelsTops, function(k,v){
                    if(RegExp.$1 == v){
                        flag = 'modelstop';
                    }
                });
                return flag;
            }
            if (pathname.search(/^\/10th.+$/) > -1) {
                return '10th';
            }
            return false;
        })();
        if (target === false) {
            return;
        }

        var isRedirect = function() {
            if (ua.indexOf('android') > -1 && ua.indexOf('mobile') > -1) {
                return false;
            }
            if (window.location.search.search(/smp_to_pc/i) > -1) {
                return false;
            }
            if (cookies[cookieName]) {
                return false;
            }
            return true;
        };
        var redirect = function() {
            window.location.href = '/smp' + window.location.pathname;
            return false;
        };
        var html = '<div id="to-smartphone"><p><a href="#" class="to-smartphone button larger"><span>スマートフォン版を表示する</span></a></p></div>';
        if (target == 'top' || target == '10th') {
            if (isRedirect()) {
                redirect();
            }
            html = $(html).hide();
        }

        $('body').prepend(html);
        $('#to-smartphone a.to-smartphone').on('click', redirect);
    })();

    /**
     * Set viewport for iPad
     */
    (function() {
        if ($('body').hasClass('non-viewport')) {
            return false;
        }
        if (!Modernizr.touch) {
            return;
        }
        var setViewportContent = function(orientationchange, swidth) {
            var orientation = window.orientation;
            var width;
            var zoom;
            var viewportContent = '';

            var screen_width;
            if ((orientation % 180) == 0) {
                width = 1024;
                screen_width = window.screen.width;
            } else {
                width = 1280;
                screen_width = window.screen.height;
            }
            zoom = screen_width / width;
            if (orientationchange === false) {
                if (typeof swidth === 'undefined') {
                    viewportContent = 'width='+ width +', initial-scale=' + zoom + ', user-scalable=yes, minimum-scale=' + zoom + ', maximum-scale=3.0';
                } else {
                    viewportContent = 'width='+ 1024 +', user-scalable=yes';
                }
            } else {
                viewportContent = 'width='+ width +', initial-scale=' + zoom + ', user-scalable=yes, minimum-scale=' + zoom + ', maximum-scale=3.0';
            }

            $('meta[name="viewport"]').attr('content', viewportContent);
            $(window).trigger('scroll');
        };

        var ua = navigator.userAgent.toLowerCase();
        var v = [99, 0, 0];
        if (/ip(hone|od|ad)/.test(ua)) {
            v = (navigator.appVersion).match(/OS ([\d])_([\d]+)_([\d]+)/);
            if (!v) {
                v = (navigator.appVersion).match(/OS ([\d]+)_([\d]+)/);
            }
            v = [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
            return v[0];
        }
        if (ua.indexOf('ipad') > -1 && v[0] > 5) {
            $(window).on('orientationchange', setViewportContent);
            setViewportContent();
        } else if(ua.indexOf('ipad') > -1 && v[0] == 5) {
            setViewportContent(false);
            (function(doc) {
                var addEvent = 'addEventListener',
                    type = 'gesturestart',
                    qsa = 'querySelectorAll',
                    scales = [1, 1],
                    meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [];

                function fix() {
                    meta.content = 'width=device-width,minimum-scale=' + scales[0] + ',maximum-scale=' + scales[1];
                    doc.removeEventListener(type, fix, true);
                }

                if ((meta = meta[meta.length - 1]) && addEvent in doc) {
                    fix();
                    scales = [.25, 1.6];
                    doc[addEvent](type, fix, true);
                }
            })(document);
        } else {
            if (!$body.hasClass('responsive')) {
                setViewportContent(false, 1024);
            }
        }
    })();

    /**
     * Set Mode
     */
    $(function() {
        viewMode = (Modernizr.mq('only screen and (max-width: 800px)')) ? 'sp' : 'pc';
        
        $(window).on('resize', function() {
            viewMode = (Modernizr.mq('only screen and (max-width: 800px)')) ? 'sp' : 'pc';
        });
    });

    /**
     * scroll to content and scroll to page top
     */
    (function() {
        var scrollToContent = function(e) {

            $('body').append($('<div></div>').addClass('iosfix'));
            window.setTimeout(function() {
                $('.iosfix').remove();
            }, 510);

            var anchor = '#' + $(this).attr('href').split('#')[1];
            var target = {
                top: 0,
                left: 0
            };
            var opt = {
                easing: 'swing',
                duration: 500
            };

            var detectHeaderHeight = function() {
                var headerHeight = $('#local-nav').height();
                if (headerHeight <= 0) {
                    headerHeight = $('#local-nav').children().height();
                }
                return headerHeight;
            };
            var detectTop = function($anchor) {
                var targetOffsetTop;
                targetOffsetTop = $anchor.offset().top;
                return targetOffsetTop - detectHeaderHeight();
            };
            if ($(anchor).length > 0) {
                target.top = detectTop($(anchor));

                if($(anchor).is('.scroll-offset')){
                    target.top -= 40;
                }
                if($(anchor).is('.scroll-offset-100')){
                    target.top -= 100;
                }
                opt.onAfter = function(){
                    $(window).scrollTop(target.top);
                };
                $.scrollTo(target, opt);
            }
            if (anchor == '#to-page-top') {
                $.scrollTo(0, 400, {easing:"swing"});
            }
            return false;
        };
        $("a[href^='#'], area[href^='#']").not('.lightbox, .lightbox-inline, body#lexus-ae a, body#carSelect #carSelect_section a').click(scrollToContent);
    })();

    /**
     * slide toggle more contents
     */
    (function(){
        $(window).load(function() {

            $(".more-content").each(function(){
                var $content = $(this).find( ".content" );
                var $btn     = $(this).find( ".btn-more" );
                var contentHeight = $content.height();
                var moreText = $btn.text();
                var lessText = $btn.data('lessText');

                $content.height(0).hide();

                if ($('html.lt-ie7').length > 0) {
                    $btn.css({opacity:"1"});
                }

                $btn.click(function(){
                    if($content.is(":visible")){
                        $content.stop().animate( { height:0 }, { duration:500, easing:"easeOutExpo", complete:function(){
                            $(this).hide();
                            $btn.text(moreText);
                        }});
                    }else{
                        $content.show().stop().animate( { height:contentHeight }, { duration:500, easing:"easeOutCubic", complete:function(){
                            $btn.text(lessText);
                        }});
                    }
                });
            });
        });

    })();


    /**
     *  rollover clip
     */
    (function() {
        $(function() {
            $( "img.roll-clip" ).rolloverClip();
        });
    })();

    /**
     * rollover
     */
    (function() {
        var rollover = {
            hoverin : function(){
                $(this).stop().animate({opacity:0.75});
            },
            hoverout: function(){
                $(this).stop().animate({opacity:1.0});
            }
        };
        $(document).on('mouseenter', '.rollover', rollover.hoverin);
        $(document).on('mouseleave', '.rollover', rollover.hoverout);
    })();

    /**
     * carousel
     */
    (function() {
        $( "div.carousel:not(.w-980, .non-carousel)" ).carousel({
            fixCenter: true,
            minWidth: 1000,
            ui: 'div.ui'
        });
        $( "div.carousel.w-980:not(.non-carousel)" ).carousel({
            fixCenter: true,
            minWidth: 980,
            ui: 'div.ui'
        });
    })();

    /**
     * lightbox get colorbox resizeData
     */
    var getColorboxResizeData = function () {
        // ウインドウサイズを取得
        var $window = $(window);
        var hmargin = 40,
            vmargin = 40,
            minWidth = 640,
            minHeight = 360,
            maxWidth = 1280,
            maxHeight = 720;

        var w = $window.width() - hmargin * 2;
        var h = $window.height() - vmargin * 2;

        if(w < minWidth){
            w = minWidth;
        }else if(w > maxWidth + hmargin * 2){
            w = maxWidth;
        }

        if(h < minHeight){
            h = minHeight;
        }else if(h > maxHeight + vmargin * 2){
            h = maxHeight;
        }

        // 適用できる最大サイズと最大サイズの割合から、最終的な割合を算出
        var sx = w / minWidth;
        var sy = h / minHeight;
        var s = Math.min( sx, sy );

        // 割合から整数値の幅、高さを算出
        w = Math.round( minWidth * s );
        h = Math.round( minHeight* s );

        w = w + hmargin * 2;
        h = h + vmargin * 2;
        return {
            width: w,
            height: h
        };
    };

    /**
     * lightbox for iframe youtube video and etc
     */
    (function() {
        var resizeData = getColorboxResizeData();
        var onColorboxResize = function() {
            var $iframe = $('#colorbox').find('iframe');
            if ($iframe.length > 0 && $iframe.attr('src').search(/youtube\.com/i) > -1) {
                resizeData = getColorboxResizeData();
                $.colorbox.resize(resizeData);
            }
        };
        // youtube movie
        var $youtube = $('a.movie').filter(function() {
            return $(this).attr('href').search(/youtube\.com/i) > -1;
        });
        $youtube.each(function() {
            var $self = $(this);
            $self.colorbox({
                overlayClose: true,
                opacity: 0.85,
                transition: "fade",
                fixed: true,
                title: function() {
                    return $self.next().find('.discription').html();
                },
                onOpen : function () {
                    resizeData = getColorboxResizeData();
                    $('#cboxClose').hide();
                },
                onComplete: function() {
                    $('#cboxClose').show();
                },
                iframe: function() {
                    return true;
                },
                width: function() {
                    return resizeData.width;
                },
                height: function() {
                    return resizeData.height;
                },
                innerWidth: function() {
                    return resizeData.width;
                },
                innerHeight: function() {
                    return resizeData.height;
                },
                initialWidth: function() {
                    return resizeData.width;
                },
                initialHeight: function() {
                    return resizeData.height - 30;
                }
            });
        });
        // etc
        var $etc = $('a.lightbox');
        $etc.colorbox({
            overlayClose: true,
            opacity: 0.85,
            transition: "fade",
            fixed: false,
            onOpen : function () {
                $('#cboxClose').hide();
            },
            onComplete: function() {
                $('#cboxClose').show();
            },
            iframe: function() {
                return false;
            }
        });
        // inline
        var $inline = $('.lightbox-inline');
        if ($inline.length <= 0) return;
        $inline.each(function() {
            var $this = $(this);
            var target;
            if ($this.get(0).tagName === 'A') {
                target = $($this.attr('href'));
            } else {
                target = $($this.data('id'));
            }
            $this.colorbox({
                overlayClose: true,
                opacity: 0.64,
                transition: "fade",
                fixed: true,
                inline: true,
                iframe: false,
                href: target,
                onOpen : function () {
                    $('#cboxClose').hide();
                    if ($(this).get(0).tagName === 'A') {
                        return $($(this).attr('href')).show();
                    } else {
                        return $($(this).data('id')).show();
                    }
                },
                onComplete: function() {
                    $('#cboxClose').show();
                },
                onClosed: function() {
                    if ($(this).get(0).tagName === 'A') {
                        return $($(this).attr('href')).hide();
                    } else {
                        return $($(this).data('id')).hide();
                    }
                },
                iframe: function() {
                    return false;
                }
            });
        });
        // window.resize
        $(document).on('cbox_complete', function(){
            if ($etc.length > 0) {
                var timer = false;
                $(window).on('resize.etc', function() {
                    if (timer !== false) {
                        clearTimeout(timer);
                    }
                    timer = setTimeout(function() {
                        $.colorbox.position(350);
                    }, 500);
                });
            }
        });
        $(document).on('cbox_cleanup', function(){
            $(window).off('resize.etc');
        });
        // window.resize
        if ($youtube.length > 0 || $etc.length > 0) {
            $(window).on('resize.youtube', onColorboxResize);
        }
    })();

    /**
     * lightbox for gallery
     */
    (function() {
        /**
         * create carousel from gallery
         */
        var $gallerys = $('div.gallery');
        if ($gallerys.length <= 0) return;
        var showTitle = $gallerys.hasClass('title');
        var $colorbox = $('#cboxLoadedContent');
        var $colorboxTitle = $('#cboxTitle');
        $gallerys.each(function() {
            var $this = $(this);
            var carousel = $("<div></div>").addClass('carousel');
            var list = $('<ul></ul>').addClass('view');
            carousel.append(list);

            $this.find('ul.thumbs > li > a:not(.movie)').each(function(){
                var $this = $(this);
                var title = $this.data('title') || $this.attr('title');
                var item = $('<li data-title="'+(title ? title: '')+'"><img src="' + $(this).attr('href') + '"/></li>');
                list.append(item);
            });
            carousel.appendTo($this);
            carousel.carousel({
                fixCenter: true,
                minWidth: 600,
                changeTitleTarget: (showTitle ? $colorboxTitle: false),
                titleClass: 'galleryTitle'
            });
            carousel
                .css({visibility: 'hidden'})
                .hide();
        });

        var resizeData = getColorboxResizeData();
        var onColorboxResize = function() {
            resizeData = getColorboxResizeData();
            var carousel = $('#colorbox').find('.carousel');
            if(carousel.length > 0 && carousel.data('carousel')){
                var vmargin = parseInt($('#cboxContent').css('margin-top')) +
                        parseInt($('#cboxContent').css('margin-bottom'));
                var hmargin = parseInt($('#cboxContent').css('margin-left')) +
                        parseInt($('#cboxContent').css('margin-right'));
                resizeData =
                        carousel.data('carousel').onColorboxResize(
                            resizeData, vmargin, hmargin);
                $.colorbox.resize(resizeData);
            }
        };
        $gallerys.each(function() {
            var $gallery = $(this);
            $gallery.find('ul.thumbs > li > a:not(.movie)')
                    .append('<div class="hover" style="cursor:pointer;"></div>')
                    .colorbox({
                        overlayClose: true,
                        opacity: 0.85,
                        transition: "fade",
                        fixed: true,
                        inline: true,
                        iframe: false,
                        href: $gallery.find('div.carousel'),
                        title: function() {
                            var carousel = $gallery.find('div.carousel').data('carousel');
                            return carousel.getCurrent().title;
                        },
                        onOpen : function () {
                            $gallery.find('div.carousel')
                                .show()
                                .css({visibility: 'visible'});
                            resizeData = getColorboxResizeData();
                            var src = $(this).attr('href');
                            var index = $gallery.find('div.carousel').find('img[src$="' + src + '"]').parent().index();
                            var carousel = $gallery.find('div.carousel').data('carousel');
                            carousel.setCurrentPos(index);
                        },
                        onCleanup : function(){
                            $('#colorbox').find('div.carousel')
                                .css({visibility: 'hidden'})
                                .hide();
                        },
                        onComplete: function() {
                            $('#cboxClose').show();
                            onColorboxResize();
                            $('#cboxLoadedContent').css('overflow', 'hidden');
                            if($.browser.msie){
                                setTimeout(function(){
                                    $('#cboxLoadingOverlay').hide();
                                    $('#cboxLoadingGraphic').hide();
                                }, 1000);
                            }
                        },
                        width: function() {
                            return resizeData.width;
                        },
                        height: function() {
                            return resizeData.height;
                        },
                        innerWidth: function() {
                            return resizeData.width;
                        },
                        innerHeight: function() {
                            return resizeData.height;
                        },
                        initialWidth: function() {
                            return resizeData.width;
                        },
                        initialHeight: function() {
                            return resizeData.height;
                        }
                    });
        });

        // window.resize
        $(window).on('resize.colorbox', onColorboxResize);
    })();

    /**
     * colorbox - Overlay Close for #cboxContent margin
     */
    (function() {
        $(document).on({
            'cbox_open': function(e) {
                $('#cboxWrapper').css({
                    cursor: 'pointer'
                });
                $(document).on('click.colorbox.close', function(e) {
                    if ($(e.target).parents('#cboxLoadedContent').length > 0) {
                        return false;
                    }
                    $.colorbox.close();
                    return false;
                });
            },
            'cbox_cleanup': function() {
                $('#cboxWrapper').css({
                    cursor: ''
                });
                $(document).off('click.colorbox.close');
            }
        });
    })();

    /**
     * fixed table's thead
     */
    (function($) {

        var getCookies = function() {
            var result = {};
            var allcookies = document.cookie;
            if (allcookies != '') {
                var cookies = allcookies.split('; ');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = cookies[i].split('=');
                    result[cookie[0]] = cookie[1];
                }
            }
            return result;
        };
        var cookieName = 'no_fixed';
        var cookies = getCookies();
        if (cookies[cookieName] === '1') {
            return false;
        }

        if (Modernizr.touch || $('html.lt-ie8').length > 0) {
            return;
        }

        var $tableWrapper = $('div.table-wrapper.fixed-thead');

        if ($tableWrapper.length <= 0) {
            return;
        }

        var detectHeaderHeight = function() {
            var headerHeight = $('#local-nav').height();
            if (headerHeight <= 0) {
                headerHeight = $('#local-nav').children().height();
            }
            return headerHeight;
        };
        var detectFooterHeight = function() {
            return $('#footer').find('div.primary').height();
        };
        var detectTop = function($anchor) {
            var targetOffsetTop;
            targetOffsetTop = $anchor.offset().top;
            return targetOffsetTop - detectHeaderHeight();
        };

        // generate fixed thead clone
        var $tableThead = $tableWrapper.find('table.thead');
        var $tableFix;
        var $notifyFix = $tableWrapper.find('p.push-tabs').clone();

        if ($tableThead.length <= 0) {
            $tableThead = $tableWrapper.find('thead');
            $tableFix = $tableThead.parent('table').clone();
            $tableFix.children('tbody').remove();
        } else {
            $tableFix = $tableThead.clone();
        }

        var $wrapperFix = $('<div>').append($('<div>').append($notifyFix).append($tableFix));
        $wrapperFix.prependTo($tableWrapper);
        $tableWrapper.css({
            position: 'relative',
            clear: 'both'
        });
        $wrapperFix.css({
            position: 'absolute',
            top: 0,
            left: 0,
            zIndex: 1,
            width: '100%',
            padding: marginTop + 'px 0 ' + paddingBottom + 'px 0',
            margin: '-' + marginTop + 'px 0 0 0',
            backgroundColor: '#fff'
        });
        $wrapperFix.children().css({
            width: $tableWrapper.width(),
            margin: '0 auto'
        });

        // detect top position
        var tableTheadHeight = $wrapperFix.children().height();
        var marginTop = 10;
        var paddingBottom = 5;

        if($('body.specifications.color').length > 0){
            paddingBottom = 1;
        }

        $(window).on('scroll.fixed-thead', function() {
            var windowTop = $(window).scrollTop();
            var targetOffsetTop = $tableWrapper.offset().top;
            var tableWrapperHeight = $tableWrapper.height();
            var windowHeight = $(window).height();
            var headerHeight = detectHeaderHeight();
            var footerHeight = detectFooterHeight();
            var tableBodyHeight = tableWrapperHeight - tableTheadHeight;
            var top = headerHeight;
            var widthFlag;
            var scrollCheck = $wrapperFix.css('position');
            
            if($(window).width() <= 980) {
                widthFlag = true;
            } else {
                widthFlag = false;
            }
            
            if (windowTop <= targetOffsetTop - (headerHeight + marginTop)) {
                $wrapperFix.css({
                    position: 'absolute',
                    top: 0,
                    'padding-left': 0
                });
            } else {
                var minHeight = 0;
                if (windowTop >= targetOffsetTop + tableBodyHeight - minHeight - (headerHeight + marginTop)) {
                    var offsetlastTop
                            = tableWrapperHeight - tableTheadHeight - minHeight;
                    $wrapperFix.css({
                        position: 'absolute',
                        top: offsetlastTop,
                        'padding-left': 0
                    });
                } else {
                    if(widthFlag == true) {
                        $wrapperFix.css({
                            position: 'fixed',
                            top: top + marginTop,
                            left: 0,
                            width: '100%',
                            padding: marginTop + 'px 0 ' + paddingBottom + 'px 10px',
                            margin: '-' + marginTop + 'px 0 0 0',
                            backgroundColor: '#fff'
                        });
                    } else {
                        $wrapperFix.css({
                            position: 'fixed',
                            top: top + marginTop,
                            left: 0,
                            width: '100%',
                            padding: marginTop + 'px 0 ' + paddingBottom + 'px 0',
                            margin: '-' + marginTop + 'px 0 0 0',
                            backgroundColor: '#fff'
                        });
                    }
                }
            }
        });
        
        //980px以下のテーブル固定
        $(window).on('resize', function() {
            var tableW = $(window).width(),
                scrollCheck = $wrapperFix.css('position');
            
            if(tableW <= 980 && scrollCheck == 'fixed') {
                $wrapperFix.css({
                    padding: marginTop + 'px 0 ' + paddingBottom + 'px 10px'
                });
            } 
        });
        
    })($);

    /**
     * tooltip
     */
    (function ($) {

        // for color and etc.
        $('a.tooltip').powerTip({
            smartPlacement: true
        }).on({
            powerTipPreRender: function() {
                $('#powerTip').addClass('white');
            },
            powerTipRender: function() {
                $('#powerTip').addClass('white');
            }
        });

        $('#powerTip').css({
            fontSize: '10',
            textAlign: 'left'
        });

        // if html has table.tooltip-table, tooltip is required
        var $table = $('table.tooltip-table');
        if ($table.length <= 0) {
            return;
        }

        // for *? notification
        var notes = [];
        $('dl.disclaimer').find('dt').each(function() {
            var
            $dt = $(this),
            $dd = $dt.next('dd'),
            num,
            text = $dt.text();

            if ($dt.text().match(/^＊/)) {
                num = parseInt($dt.text().substr(1));
                notes[num] = $dd.html();
            }
        });

        $table.find('em').each(function() {
            var
            $em = $(this),
            text = $em.text();

            if (text.match(/^＊/)) {
                $em.powerTip({
                    smartPlacement: true
                });
                $em.data('powertip', notes[text.substr(1)]);
            }
        }).on({
            powerTipPreRender: function() {
                $('#powerTip').removeClass('white');
            }
        });
        if (Modernizr.touch){
            $('tr td:last-child em').powerTip({placement:'nw'});
        }

    })($);

    /**
     *  add class for table first row
     */
    (function ($) {
        var $table = $('table.tooltip-table');
        if ($table.length <= 0) {
            return;
        }
        $table.find('tbody tr:first:not(.dummy)').addClass('first-child');
        $table.find('tbody tr:first.dummy').next().addClass('first-child');
    })($);

    /**
     * spec table show toggle
     */
    (function($) {
        var $tableEq = $('table.toggle-table');
        if ($tableEq.length <= 0) {
            return;
        }

        $('.table-toggle').click(function(){
            var $caption = $(this);
            $caption.next('.table-wrapper').slideToggle(function(){
                if($(this).is(':visible')){
                    $caption.removeClass('close');
                }else{
                    $caption.addClass('close');
                }
            });
        });
    })($);

    /**
     * spec table cell highlight
     */
    (function($) {
        var $tableEq = $('table.highlight-table');
        if ($tableEq.length <= 0) {
            return;
        }

        // 横行ハイライトのイベント処理
        $tableEq.find('tbody > tr').hoverIntent(function(){
            $(this).addClass('hoverRow');
        },function(){
            $(this).removeClass('hoverRow');
        });


        var HOVER_TAG = '__hoverTd';
        // 縦列ハイライト用のクラスを付与
        $tableEq.find('tbody > tr').each(function(){
            var i = 0;
            $(this).children('td').each(function(){
                var colspan = $(this).attr('colSpan') || 1;
                // 結合されているTDには、通し番が途切れないようにHOVER_TAGを複数付与する
                for(var j=0; j<colspan; ++j){
                    $(this).addClass(HOVER_TAG+i);
                    ++i;
                }
            });
        });

        // 縦列ハイライトのイベント処理
        // 縦は重いので hoverIntent を使う
        $tableEq.find('tbody > tr > td').hoverIntent(function(){
            // thisのclassからhoverTd...を探し、同じclassを持つtdにhover_colを付与する。

            // 自身の持つtagsを取得
            var self_tags = $.grep(
                $(this).attr('class').split(' '),
                function(value, index){ return (value.indexOf(HOVER_TAG)==0); }
            );
            var target_tags = self_tags;

            // HOVER_TAGが1つだけ付いている<TD>で、
            if(self_tags.length == 1){
                var $colspaned = $tableEq.find('.' + self_tags[0] + '[colSpan]');

                // 同じHOVER_TAGが付いているほかのTDが、1つでもセル結合されていたら、
                if($colspaned.length > 0){
                    var temp = [];

                    // 結合されている他のセルのクラスを全てとってきて、
                    $colspaned.each(function(){
                        var other_tags = $.grep(
                            $(this).attr('class').split(' '),
                            function(value, index){
                                return (value.indexOf(HOVER_TAG)==0);
                            });
                        // UNIQUEする
                        temp = $.unique(temp.concat(other_tags));
                    });
                    target_tags = temp;
                }
            }
            // 対象となるHOVER_TAGのいずれかを持つTDにhoverColを付与
            var selector = $.map(target_tags, function(v){ return '.' +v; }).join(', ');
            $(selector).addClass('hoverCol');

        }, function(){
            $tableEq.find('tbody > tr > td').removeClass('hoverCol');
        });

        // th,tdにlast-childクラスを付与
        $tableEq.find('thead > tr > th:last-child').addClass('last-child');
        $tableEq.find('thead > tr > td:last-child').addClass('last-child');
        $tableEq.find('tbody > tr').each(function(){
            var $this = $(this).find('th');
            $this.first().addClass('first-child');
            $this.last().addClass('last-child');

            // 3個以上のときは、2つ目にsecond-childを付与
            if($this.length > 2)
                $this.eq(1).addClass('second-child');
        });
        $tableEq.find('tbody > tr > td:last-child').addClass('last-child');

        // zero-baseのためoddにevenを付与する。
        $tableEq.find('tbody tr:not(.dummy)').each(function(i){
            if(i%2!=0){
                $(this).addClass('even');
            }
        });
    })($);

    /**
     * visual fixed width
     */
    (function() {
        var minWidth = 1000;

        var $heroVisual = $(".hero-visual");
        var $flushVisual = $(".flush-visual");

        var $figure = $([
            "section.hero-visual:not(.w-1600):not(.w-980) .container",
            ".flush-visual:not(.w-1600):not(.w-980) .container",
            "section.hero-visual:not(.w-1600):not(.w-980) figure",
            ".flush-visual:not(.w-1600):not(.w-980) figure"
        ].join(','));
        var $figure1600 = $([
            "section.hero-visual.w-1600 .container",
            ".flush-visual.w-1600 .container",
            "section.hero-visual.w-1600 figure",
            ".flush-visual.w-1600 figure"
        ].join(','));
        var detectMargin =  function(width) {
            var windowWidth = $(window).width();
            var margin = 0;
            if ((width < windowWidth) == false) {
                if (windowWidth < minWidth) {
                    windowWidth = minWidth;
                }
                margin = -1 * (width - windowWidth) / 2;
            }
            return {
                'margin-left': margin,
                'margin-right': margin
            };
        };
        var $html = $('html');
        var onWindowResize =  function() {
            var i = 0;
            // for image width is 1280px
            $figure.css(detectMargin(1280));
            // for image width is 1600px
            $figure1600.css(detectMargin(1600));
            if ($html.hasClass('lt-ie8')) {
                $figure1600.parent().css({position: 'relative'});
                $figure.parent().css({position: 'relative'});
            }
        };
        if (($html.hasClass('lt-ie8') &&
             $('body').prop('id') == 'models' &&
             $('body.top').hasClass('top')) == false){
            $heroVisual.css({visibility: "visible"});
            $flushVisual.css({visibility: "visible"});
        }

        $(window).on('resize.fixedMainVisual', onWindowResize);
        onWindowResize();

    })();




    /**
     *  tabs toggle
     */
    (function() {

        $(document).ready(function(){

            var $el = $(".tabs-toggle");

            $el.each(function(){

                var hideItem = function($obj){
                    $obj.stop().slideUp({
                        "duration":500,
                        "easing":"easeOutExpo",
                        "complete":function(){
                            tabToggleCurrent();
                        }
                    });
                };

                var $tabs = $(this).find("ul.tabs");

                var tabToggleCurrent = function(){
                    $tabs.children().each(function(index, tab){
                        var box_id = $(tab).children("a").attr("href");
                        var $box = $(box_id);
                        if($box.is(':visible')){
                            $(tab).addClass('current');
                        }else{
                            $(tab).removeClass('current');
                        }
                    });
                };

                var self = this;


                $tabs.children().each(function(index, tab){

                    var box_id = $(tab).children("a").attr("href");

                    var $box = $(box_id);

                    $box.find(".btn-close").off("click").on("click", function(){
                        hideItem($box);
                        return false;
                    });
                });

                $tabs.children().each(function(index, tab){
                    $(tab).children("a").off("click").on("click", function(){

                        var tab_id = $(this).attr("href");

                        var $opened_box = $(self).find(".item:visible");

                        if($opened_box.length == 0){
                            $(tab_id).slideDown({
                                "duration":500,
                                "easing":"easeOutExpo",
                                "complete":function(){
                                    tabToggleCurrent();
                                }
                            });
                        }else{
                            $opened_box.slideUp({
                                "duration":500,
                                "easing":"easeOutExpo",
                                "complete":function(){

                                    tabToggleCurrent();

                                    if('#'+$opened_box[0].id == tab_id){
                                        return;
                                    }
                                    $(tab_id).slideDown({
                                        "duration":500,
                                        "easing":"easeOutExpo",
                                        "complete":function(){
                                            tabToggleCurrent();
                                        }
                                    });
                                }
                            });
                        }
                        return false;
                    });
                });

            });
        });
    })();


    /*
     * accordion toggle
     */
    (function(){
        $(function(){

            $('.accordion').each(function(){

                var $title = $(this).find('h6,h4');

                $title.css("cursor","pointer");
                $title.find("a").off("click");
                $title.each(function(){

                    var $title = $(this);
                    var $desc  = $(this).next();

                    $desc.hide();
                    $title.addClass('close').removeClass('open');

                    $title.on("click", function(){
                        $desc.stop().slideToggle({
                            "duration":500,
                            "easing":"easeOutExpo",
                            "complete":function(){
                                if($(this).is(':visible')){
                                    $title.removeClass('close').addClass('open');
                                }else{
                                    $title.addClass('close').removeClass('open');
                                }
                            }
                        });
                        return false;
                    });
                });
            });
        });
    })();



    /*
     *  tab-01 toggle
     */
    (function(){
        $(function() {

            var $el = $(".toggle-carousel");

            $el.each(function(ii){

                var $container = $(this);

                var tabs = $(this).find("ul.tabs-01");

                var _class_current = "current";

                var _anime_duration = 1000;
                var _anime_easing   = "easeOutExpo";

                var boxes = (function(el){
                    var arr = [];
                    $(el).find(".carousel").each(function(i, box){
                        var $obj = $(box);
                        arr.push({
                            index:$obj.index(),
                            id:$obj.attr("id"),
                            obj:$obj,
                            height:$obj.height(),
                            margin_top:$obj.css("margin-top"),
                            margin_bottom:$obj.css("margin-bottom")
                        });
                    });
                    return arr;
                }(this));

                var showBox = function(box){
                    box.obj
                    .css({ "z-index":1 })
                    .show()
                    .stop()
                    .animate({
                        scale:1,
                        opacity:1,
                        height:box.height,
                        "margin-top":box.margin_top,
                        "margin-bottom":box.margin_bottom
                    },{
                        duration:_anime_duration,
                        easing:_anime_easing,
                        complete:function(){
                            var ui_height = box.obj.find(".ui").height();
                            var ov_height = box.height - ui_height;
                            box.obj.find(".carousel-overlay-left").css({height:ov_height});
                            box.obj.find(".carousel-overlay-right").css({height:ov_height});
                        }
                    });


                };
                var hideBox = function(box){
                    box.obj
                    .css({ "z-index":0 })
                    .stop()
                    .animate({
                        scale:0,
                        opacity:0,
                        height:0,
                        "margin-top":0,
                        "margin-bottom":0
                    },{
                        duration:_anime_duration,
                        easing:_anime_easing
                    }).queue(function(){
                        $(this).animate({scale:1},{duration:0});
                        $container.append($(this));
                        $(this).dequeue();
                    });
                };

                var current = tabs.children("."+_class_current).index();

                // initialize
                for(var i=0; i<boxes.length; i++){
                    var box = boxes[i];

                    if (box.index == 1){

                        box.obj.show();
                    }else{
                        box.obj.css({
                            scale:0,
                            opacity:0,
                            height:0,
                            "margin-top":0,
                            "margin-bottom":0
                        })
                        .hide();
                    }
                }

                // click event
                tabs.children().each(function(index, tab){
                    $(tab).children("a").off("click").on("click", function(){
                        tabs.children().removeClass(_class_current);
                        $(this).parent().addClass(_class_current);

                        $(this).attr("href").match(/^#([0-9a-zA-Z_-]+)/);
                        var tab_id = RegExp.$1;

                        if(tab_id.length > 0){

                            for(var i=0; i<boxes.length; i++){
                                var box = boxes[i];

                                if (box.id == tab_id){
                                    showBox(box);
                                }else{
                                    if(box.obj.is(':visible') == true){
                                        hideBox(box);
                                    }
                                }
                            }

                        }
                        return false;
                    });
                });

            });
        });
    })();


    /*
     *  content toggle
     */
    (function() {
        $(function() {

            var open = function($obj){
                $obj.stop().slideDown({
                    "duration":1500,
                    "easing":"easeOutExpo"
                });
            };

            var close = function($obj){
                $obj.stop().slideUp({
                    "duration":500,
                    "easing":"easeOutExpo"
                });
            };

            var $el = $(".content-toggle");

            $el.each(function(){
                $(this).find(".toggle > a").off("click").on("click", function(){
                    var id = $(this).attr("href");
                    var box = $(id);
                    if(box.is(':visible')){
                        close(box);
                        $(this).parent().removeClass("open");
                    }else{
                        open(box);
                        $(this).parent().addClass("open");
                    }
                    return false;
                });
            });
        });
    })();


    /* Force to load css */
    function loadCss(href, check) {
        var head = document.getElementsByTagName('head')[0];
        var link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = href;
        head.appendChild(link);
    }

    $(function() {
        /* Font Detector */
        var detector = new Detector();
        if (!detector.detect('メイリオ')) {
            loadCss('/common/v2/css/font_mspg.css');
        }
    });


    /*
     *  Fix PNG alpha for IE6
     */
    (function() {
        $(function() {
           if ($('html.lt-ie7').length > 0) {
                DD_belatedPNG.fix('.png_bg, .external-link, .ico-external-link, #input_search_button');
            }
        });
    })();


    /*
     * News Table
     */
    (function() {
        $(function() {

            var $table = $("body#top #news table.table-01 tbody");
            if($table.length == 0){
                return;
            }

            $table.find("tr").each(function(i){
                var $tr = $(this);
                var $a = $tr.find('a').eq(0);

                if(i%2 != 0){
                    $tr.addClass('even');
                }
                $tr.on({
                    'mouseenter':function(){
                        $(this).addClass('highlight');
                    },
                    'mouseleave':function(){
                        $(this).removeClass('highlight');
                    }
                });
                if($a.length > 0){
                    var href = $a.prop('href');
                    var target = $a.prop('target');
                    var handler = $a.prop('onclick');
                    $a.prop('onclick', null);
                    $tr.on({
                        'click':function(){
                            if(typeof handler == 'function'){
                                handler();
                            }
                            if (target == '_blank') {
                                window.open(href);
                            } else {
                                window.location.href = href;
                            }
                            return false;
                        }
                    });
                }
            });
        });
    })();

    /*
     * No Scroll Wrapper
     */
    (function() {
        var
        init = function() {
            $('.no-scroll-wrapper').each(function() {
                makeDom($(this));
            });
        },
        makeDom = function($elem) {
            var
            displayWidth = parseInt($elem.data('display-width'), 10),
            width = parseInt($elem.data('width'), 10),
            position = $elem.data('position'),
            $wrapper = $('<div>').css({
                position: 'relative',
                margin: 0,
                width: '100%',
                'min-width': displayWidth,
                overflow: 'hidden'
            }),
            $display = $('<div>').css({
                position: 'relative',
                margin: 'auto',
                width: displayWidth,
                overflow: 'visible'
            }),
            $erea = $('<div>').css({
                position: 'relative',
                width: width,
                margin: 0,
                'margin-left': -1 * (width - displayWidth) / 2,
                overflow: 'visible'
            });

            $elem
                .wrap($wrapper)
                .wrap($display);

            if (position == 'center') {
                $elem.wrap($erea);
            }
        },
        EOF;

        $(init);
    })();

    /*
     * No Fixed for Capture
     */
    (function() {
        var getCookies = function() {
            var result = {};
            var allcookies = document.cookie;
            if (allcookies != '') {
                var cookies = allcookies.split('; ');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = cookies[i].split('=');
                    result[cookie[0]] = cookie[1];
                }
            }
            return result;
        };
        var cookieName = 'no_fixed';
        var cookies = getCookies();
        if (window.location.search.search(/no_fixed/i) > -1 && typeof cookies[cookieName] === 'undefined') {
            // Cookie のセット
            document.cookie = [
                cookieName, '=', '1;',
                'path=/;'
            ].join('');
        }

        if (cookies[cookieName] === '1') {
            var DebugMode = function() {
                $('body').css('background-color', '#fff');
                $('#header').css({ 'position': 'static', 'height': '71px' });
                $('#local-nav').css('position', 'static');
                $('#main').css('cssText', 'padding-top: 0 !important\;');
            };
            /*window.onload = function() {
                window.setTimeout(function() {
                    //DebugMode();
                }, 1000);
            };*/
        }
    })();

    /*
     * Beacon
     */
    (function() {
        $(window).load(function() {
            window.setTimeout(function() {
                $("img[src^='//r.turn.com/r/beacon']").hide();
            }, 3000);
        });
    })();
    
    // Google Tag Manager
    if (window.location.pathname.search(/^\/srvc\/tc\/top$/) > -1) return false;
    if (window.location.pathname.search(/^\/srvc\/tc\/mailmagazine$/) > -1) return false;
    if (window.location.pathname.search(/^\/srvc\/tc\/sales_memo$/) > -1) return false;
    if (window.location.pathname.search(/^\/srvc\/tc\/store_simulation$/) > -1) return false;
    if (window.location.pathname.search(/^\/srvc\/tc\/contract_document$/) > -1) return false; 

    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-T9NWLH');
    // Toyota onetag
    //document.write('<script type="text/javascript" src="//onetag.tws.toyota.jp/onetag/body"><\/script>');
    document.write('<script type="text/javascript" src="//onetag.tws.toyota.jp/onetag/body"><\/script>');
    v2OTbodyLoaded = true;

})(window.jQuery, window.requirejs, window.WINDOW_APP);

(function() {
    if (typeof (appEOF) !== 'undefined') return false;

    function enterConflict() {
        window._noConflict = $.noConflict(true);
        window._noConflictDefine = define;
        define = undefined;
    }

    function leaveConflict() {
        $ = window._noConflict;
        jQuery = window._noConflict;
        define = window._noConflictDefine;
    }

    function jsSyncLoad(srces, callback) {
        if (srces.length === 0) {
            callback();
            return;
        }
        var sc = document.createElement('script');
        sc.type = 'text/javascript';
        sc.charset = 'UTF-8';
        sc.src = srces.shift();
        sc.onload = function() { jsSyncLoad(srces, callback); };
        var body = document.getElementsByTagName('body')[0];
        body.appendChild(sc);
    }

    $('head')
        .append('<link rel="stylesheet" href="/common/v3/css/app.css">')
        .append('<link rel="stylesheet" href="/common/v3/css/v2overwrite.css">');

    $(window).on('load', function() {
        enterConflict();
        jsSyncLoad(['/common/v3/js/bundle.js', '/common/v3/js/app.js'], function() {
            if (typeof (initNav) === 'function') initNav(window, $, _, APP);
            leaveConflict();
        });
    });
}());
