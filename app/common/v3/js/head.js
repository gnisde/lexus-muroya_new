/**
 * head.js
 */

var twpId;

// FONTPLUS
(function() {
  if (window.FONTPLUS) return;
  document.write("<script type=\"text/javascript\" src=\"//webfont.fontplus.jp/accessor/script/fontplus.js?H-c8Tbw89pk%3D&aa=1&ab=2\" charset=\"utf-8\"></script>");
}());

(function() {
	var pathname = location.pathname;
	if (pathname.match('\/magazine\/')) {
		//Facebook Pixel Tag
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '120990251782652'); // Insert your pixel ID here.
		fbq('track', 'PageView');
	}
}());