/** @global */
var APP = APP || {};
APP.config = APP.config || {};
APP.util = APP.util || {};
APP.module = APP.module || {};
APP.class = APP.class || {};
if (location.hostname === 'l-nenpi.jp' ||
    location.hostname === 'stg.l-nenpi.jp' ||
    location.hostname === 'site-search.lexus.jp' ||
    location.hostname === 'mf2demo.marsflag.com') {
  APP.config.host = 'lexus.jp';
} else {
  APP.config.host = location.host;
}
APP.config.breakpoint = 767;
APP.config.news = [
  // { date: '2017.04.27', detail: '重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト' },
  // { date: '2017.04.27', detail: '重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト' },
  // { date: '2017.04.27', detail: '重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト重要なお知らせテキスト' }
];
APP.config.protocolPath = (APP.config.host.match(/lexus.jp/)) ? 'https://' + APP.config.host : 'http://' + APP.config.host ;

var jpHeader = ''
    + '<div class="st-header_inner">'
    + '<button class="st-header_spMenu">menu</button>'
    + '<div class="st-header_main u-clearfix">'
    + '  <h1 class="st-header_mainLogo"><a href="' + APP.config.protocolPath + '" onclick="sc(\'lexus:header:top\')"><img src="/common/v3/images/logo_lexus.png" alt="LEXUS"></a></h1>'
    + '  <nav class="st-header_mainNav">'
    + '    <ul class="st-header_mainNavList u-clearfix">'
    + '      <li class="st-header_mainNavItem">'
    + '        <a id="btn-header-lineup" href="/#lineup" class="st-header_mainNavLink js-megaNav" data-nav-index="0" onclick="sc(\'lexus:header:top-lineup\')">'
    + '          <span class="st-header_mainNavText">LINE UP</span>'
    + '        </a>'
    + '      </li>'
    + '      <li class="st-header_mainNavItem">'
    + '        <a id="btn-header-brand" class="st-header_mainNavLink js-megaNav" href="/brand/" data-nav-index="1" onclick="sc(\'lexus:header:brand\')">'
    + '          <span class="st-header_mainNavText">BRAND</span>'
    + '        </a>'
    + '      </li>'
    + '      <li class="st-header_mainNavItem">'
    + '        <a id="btn-header-magazine" href="/magazine/" class="st-header_mainNavLink" data-nav-index="2" target="_blank" onclick="sc(\'lexus:header:magazine\')">'
    + '          <span class="st-header_mainNavText">MAGAZINE</span>'
    + '        </a>'
    + '      </li>'
    + '    </ul>'
    + '  </nav>'
    + '  <ul class="st-header_control u-clearfix">'
    + '    <li class="st-header_controlItem st-header_controlItem-share">'
    + '      <button class="st-header_controlIcon st-header_controlIcon-share">share</button>'
    + '      <div class="st-header_share">'
    + '        <p class="st-header_shareTitle">SHARE ON</p>'
    + '        <ul class="st-header_shareList">'
    + '          <li class="st-header_shareItem"><a class="st-header_shareText st-header_shareText-fb" href="#" onclick="sc(\'lexus:header:share-facebook\')">FACEBOOK</a></li>'
    + '          <li class="st-header_shareItem"><a class="st-header_shareText st-header_shareText-tw" href="#" onclick="sc(\'lexus:header:share-twitter\')">TWITTER</a></li>'
    + '          <li class="st-header_shareItem"><a class="st-header_shareText st-header_shareText-google" href="#" onclick="sc(\'lexus:header:share-google\')">GOOGLE+</a></li>'
    + '        </ul>'
    + '      </div>'
    + '    </li>'
    + '    <li class="st-header_controlItem">'
    + '      <button class="st-header_controlIcon st-header_controlIcon-search js-search" onclick="sc(\'lexus:header:search\')">search</button>'
    + '    </li>'
    + '  </ul>'
    + '</div>'
    + '<div class="st-header_nav">'
    + '  <div class="st-header_megaNav st-megaNav">'
    + '    <div class="st-megaNav_Item st-megaNav_Item-models">'
    + '      <div class="st-megaNav_itemInner">'
    + '        <p class="st-header_navTitle">'
    + '          <span class="st-header_navText st-header_navText-dropdown" onclick="sc(\'lexus:header:lineup\')">LINE UP</span>'
    + '        </p>'
    + '        <div class="st-megaNav_content st-header_navContent">'
    + '          <div class="st-models">'
    + '            <div class="st-models_head js-iScroll">'
    + '              <ul class="st-models_list js-iScroll_inner">'
    + '                <li id="btn-header-all" class="st-models_item sw-btn is-selected" data-type="all" onclick="sc(\'lexus:header:lineup-all\')">'
    + '                  <span class="sw-btn_text">ALL</span>'
    + '                </li>'
    + '                <li id="btn-header-sedan" class="st-models_item sw-btn" data-type="sedan" onclick="sc(\'lexus:header:lineup-sedan\')">'
    + '                  <span class="sw-btn_text">SEDAN</span>'
    + '                </li>'
    + '                <li id="btn-header-coupe" class="st-models_item sw-btn" data-type="coupe" onclick="sc(\'lexus:header:lineup-coupe\')">'
    + '                  <span class="sw-btn_text">COUPE</span>'
    + '                </li>'
    + '                <li id="btn-header-suv" class="st-models_item sw-btn" data-type="suv" onclick="sc(\'lexus:header:lineup-suv\')">'
    + '                  <span class="sw-btn_text">SUV</span>'
    + '                </li>'
    + '                <li id="btn-header-hybrid" class="st-models_item sw-btn" data-type="hybrid" onclick="sc(\'lexus:header:lineup-hybrid\')">'
    + '                  <span class="sw-btn_text">HYBRID</span>'
    + '                </li>'
    + '                <li id="btn-header-fsport" class="st-models_item sw-btn" data-type="fsport" onclick="sc(\'lexus:header:lineup-fsport\')">'
    + '                  <span class="sw-btn_text">F / F SPORT</span>'
    + '                </li>'
    + '              </ul>'
    + '            </div>'
    + '            <div class="st-models_inner">'
    + '              <div class="st-modelsGroup">'
    + '                <ul class="st-models_content sw-modelsGroup_item  sw-modelsGroup_lineup u-clearfix st-modelsGroup_item"></ul>'
    + '                <ul class="sw-modelsGroup_bnr sw-modelsGroup_item st-modelsGroup_item u-pc">'
    + '                  <li><a href="/models/ux/" onclick="sc(\'lexus:header:lineup-bnr-ux\');"><img class="sw-modelsGroup_item-img" src="/common/v3/images/header/lineup/nav_100b.jpg" alt=""></a></li>'
    + '                  <li><a href="/models/f/" onclick="sc(\'lexus:header:lineup-bnr-f\');"><img class="sw-modelsGroup_item-img" src="/common/v3/images/header/lineup/nav_ffsport.jpg" alt=""></a></li>'
    + '                  <li><a href="/models/suv/" onclick="sc(\'lexus:header:lineup-bnr-suv\');"><img class="sw-modelsGroup_item-img" src="/common/v3/images/header/lineup/nav_suv.jpg" alt=""></a></li>'
    + '                </ul>'
    + '                <ul class="sw-modelsGroup_bnr sw-modelsGroup_item st-modelsGroup_item u-sp u-mb-30">'
    + '                  <li><a href="/models/ux/" onclick="sc(\'lexus:header:lineup-bnr-ux\');"><img class="sw-modelsGroup_item-img" src="/common/top/images/lineup/img_100b.jpg" alt=""></a></li>'
    + '                  <li><a href="/models/f/" onclick="sc(\'lexus:header:lineup-bnr-f\');"><img class="sw-modelsGroup_item-img" src="/common/top/images/lineup/img_ffsport.jpg" alt=""></a></li>'
    + '                  <li><a href="/models/suv/" onclick="sc(\'lexus:header:lineup-bnr-suv\');"><img class="sw-modelsGroup_item-img" src="/common/top/images/lineup/img_suv.jpg" alt=""></a></li>'
    + '                </ul>'
    + '              </div>'
    + '              <div class="st-modelsGroup_pager st-modelsGroup_prev"></div>'
    + '              <div class="st-modelsGroup_pager st-modelsGroup_next"></div>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <span class="st-header_navDivider"></span>'
    + '      </div>'
    + '    </div>'
    + '    <div class="st-megaNav_Item st-megaNav_Item-brand">'
    + '      <div class="st-megaNav_itemInner">'
    + '        <p class="st-header_navTitle">'
    + '          <span class="st-header_navText st-header_navText-dropdown" onclick="sc(\'lexus:header:brand\')">BRAND</span>'
    + '        </p>'
    + '        <div class="st-megaNav_content st-header_navContent">'
    + '<div class="st-brandCategoryCardGroup">'
    + '  <div class="st-brandCategoryCardGroup_pager st-brandCategoryCardGroup_prev"></div>'
    + '  <div class="st-brandCategoryCardGroup_pager st-brandCategoryCardGroup_next"></div>'
    + '  <div class="st-brandCategoryCardGroup_list">'
    + '    <div class="st-brandCategoryCardGroup_item">'
    + '      <div class="st-brandCategoryCard">'
    + '        <a class="st-brandCategoryCard_link" href="/brand/" onclick="sc(\'lexus:header:brand-brand\')">'
    + '          <div class="st-brandCategoryCard_media st-brandCategoryCard_media-long">'
    + '            <div class="st-brandCategoryCard_image st-brandCategoryCard_image-brandExperience"></div>'
    + '            <div class="st-brandCategoryCard_cover"></div>'
    + '            <div class="st-brandCategoryCard_note">'
    + '              <div class="st-brandCategoryCard_noteWrap">'
    + '                <span class="st-brandCategoryCard_noteText">世界に興奮と変化を<br>もたらす革新を<br>デザインします</span>'
    + '              </div>'
    + '            </div>'
    + '          </div>'
    + '          <div class="st-brandCategoryCard_body st-brandCategoryCard_body-notBorder">'
    + '            <div class="st-brandCategoryCard_title">BRAND STORIES</div>'
    + '          </div>'
    + '        </a>'
    + '      </div>'
    + '    </div>'
    + '    <div class="st-brandCategoryCardGroup_item st-brandCategoryCardGroup_item-wide">'
    + '      <div class="st-brandCategoryCard">'
    + '        <a class="st-brandCategoryCard_link" href="/models/concept-cars/" onclick="sc(\'lexus:header:brand-concept-cars\')">'
    + '          <div class="st-brandCategoryCard_media">'
    + '            <div class="st-brandCategoryCard_image st-brandCategoryCard_image-conceptCar"></div>'
    + '            <div class="st-brandCategoryCard_cover"></div>'
    + '            <div class="st-brandCategoryCard_note">'
    + '              <div class="st-brandCategoryCard_noteWrap">'
    + '                <span class="st-brandCategoryCard_noteText">飽くなき革新から<br>生まれるLEXUSの<br>コンセプトカー</span>'
    + '              </div>'
    + '            </div>'
    + '          </div>'
    + '          <div class="st-brandCategoryCard_body">'
    + '            <div class="st-brandCategoryCard_title">CONCEPT CARS</div>'
    + '          </div>'
    + '        </a>'
    + '      </div>'
    + '      <div class="st-brandCategoryCard">'
    + '        <a class="st-brandCategoryCard_link" href="/brand/motor_show/" onclick="sc(\'lexus:header:brand-motor-show\')">'
    + '          <div class="st-brandCategoryCard_media">'
    + '            <div class="st-brandCategoryCard_image st-brandCategoryCard_image-motorShow"></div>'
    + '            <div class="st-brandCategoryCard_cover"></div>'
    + '            <div class="st-brandCategoryCard_note">'
    + '              <div class="st-brandCategoryCard_noteWrap">'
    + '                <span class="st-brandCategoryCard_noteText">国内外で開催の<br>モーターショー情報</span>'
    + '              </div>'
    + '            </div>'
    + '          </div>'
    + '          <div class="st-brandCategoryCard_body">'
    + '            <div class="st-brandCategoryCard_title">MOTOR SHOW</div>'
    + '          </div>'
    + '        </a>'
    + '      </div>'
    + '    </div>'
    + '    <div class="st-brandCategoryCardGroup_item st-brandCategoryCardGroup_item-wide">'
    + '      <div class="st-brandCategoryCard">'
    + '        <a class="st-brandCategoryCard_link" href="/brand/collection/" onclick="sc(\'lexus:header:brand-collection\')">'
    + '          <div class="st-brandCategoryCard_media">'
    + '            <div class="st-brandCategoryCard_image st-brandCategoryCard_image-collection"></div>'
    + '            <div class="st-brandCategoryCard_cover"></div>'
    + '            <div class="st-brandCategoryCard_note">'
    + '              <div class="st-brandCategoryCard_noteWrap">'
    + '                <span class="st-brandCategoryCard_noteText">LEXUSのこだわりを<br>凝縮して生まれた<br>アイテム</span>'
    + '              </div>'
    + '            </div>'
    + '          </div>'
    + '          <div class="st-brandCategoryCard_body">'
    + '            <div class="st-brandCategoryCard_title">LEXUS collection</div>'
    + '          </div>'
    + '        </a>'
    + '      </div>'
    + '      <div class="st-brandCategoryCard">'
    + '        <a class="st-brandCategoryCard_link" href="/brand/technology/" onclick="sc(\'lexus:header:brand-technology\')">'
    + '          <div class="st-brandCategoryCard_media">'
    + '            <div class="st-brandCategoryCard_image st-brandCategoryCard_image-techonology"></div>'
    + '            <div class="st-brandCategoryCard_cover"></div>'
    + '            <div class="st-brandCategoryCard_note">'
    + '              <div class="st-brandCategoryCard_noteWrap">'
    + '                <span class="st-brandCategoryCard_noteText">人を見つめ、<br>飽くなき革新を続ける<br>LEXUSのテクノロジー</span>'
    + '              </div>'
    + '            </div>'
    + '          </div>'
    + '          <div class="st-brandCategoryCard_body">'
    + '            <div class="st-brandCategoryCard_heading">BRAND STORIES</div>'
    + '            <div class="st-brandCategoryCard_title">TECHNOLOGY</div>'
    + '          </div>'
    + '        </a>'
    + '      </div>'
    + '    </div>'
    + '  </div>'
    + '  <div class="st-brandCategoryCardGroup_dots"></div>'
    + '</div>'
    + '<div class="st-brandCardGroup">'
    + '  <div class="st-brandCardGroup_pager st-brandCardGroup_prev"></div>'
    + '  <div class="st-brandCardGroup_pager st-brandCardGroup_next"></div>'
    + '  <div class="st-brandCardGroup_list">'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/global_brandad/" onclick="sc(\'lexus:header:brand-global-brandad\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-global"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">LEXUSが<br>グローバルで展開する<br>ブランド広告</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body st-brandCard_body-leftBorderPC">'
    + '          <div class="st-brandCard_title st-brandCard_title-jp"><span class="st-brandCard_text">グローバルブランド広告</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/intersect/tokyo/" onclick="sc(\'lexus:header:brand-intersect\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-intersect"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">LEXUSブランドを<br>体感できる空間</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body st-brandCard_body-leftBorderPC">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">INTERSECT BY<br>LEXUS</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/intersect/tokyo/crafted-for-lexus-index/" onclick="sc(\'lexus:header:brand-crafted-for-lexus\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-crafted"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">日本の若き匠との<br>コラボレーション<br>アイテム</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">CRAFTED FOR LEXUS</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/lexus-design/design_award/" onclick="sc(\'lexus:header:brand-design-award\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-lda"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">次世代デザイナーの<br>国際デザイン<br>コンペティション</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">LEXUS DESIGN<br>AWARD</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/lexus-design/design_events/" onclick="sc(\'lexus:header:brand-design-events\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-lde"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">”DESIGN”の<br>可能性を発信する<br>デザインイベント</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">LEXUS DESIGN<br>EVENT</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/beyond/" onclick="sc(\'lexus:header:brand-beyond\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-beyond"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">ライフスタイル<br>提案型マガジン</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">BEYOND BY LEXUS</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/amazing_experience/" onclick="sc(\'lexus:header:brand-amazing-experience\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-lae"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">革新的で<br>驚きに満ちた体験を<br>お届けしています</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">LEXUS AMAZING<br>EXPERIENCE</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/shortfilms/" onclick="sc(\'lexus:header:brand-shortfilms\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-lsf"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">LEXUSが目指す<br>世界観を描写する<br>ショートフィルム</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">LEXUS SHORT FILMS</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/motorsport/" onclick="sc(\'lexus:header:brand-motorsport\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-motorsport"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">LEXUSの<br>モータースポーツ活動</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">MOTORSPORT</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/hideki_matsuyama/" onclick="sc(\'lexus:header:brand-hideki-matsuyama\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-hidekimatsuyama"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">プロゴルフ選手<br>松山英樹</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">HIDEKI<br>MATSUYAMA</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/yoshihide_muroya/" onclick="sc(\'lexus:header:brand-yoshihide-muroya\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-yoshihidemuroya"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">エアロバティック・<br>パイロット<br>室屋義秀選手</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">YOSHIHIDE<br>MUROYA</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/dining_out/" onclick="sc(\'lexus:header:brand-dining-out\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-diningout"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">日本のどこかで数日<br>開店するプレミアムな<br>野外レストラン</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body st-brandCard_body-notBorderSP">'
    + '          <div class="st-brandCard_title"><span class="st-brandCard_text">DINING OUT</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/new-takumi/" onclick="sc(\'lexus:header:brand-new-takumi\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-lntp"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">日本のモノづくり<br>若き匠応援<br>プロジェクト</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body st-brandCard_body-notBorderSP">'
    + '          <div class="st-brandCard_title st-brandCard_title-exception"><span class="st-brandCard_text">LEXUS NEW TAKUMI<br>PROJECT</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '    <div class="st-brandCardGroup_item">'
    + '      <a class="st-brandCard" href="/brand/collaboration/" onclick="sc(\'lexus:header:brand-collaboration\')">'
    + '        <div class="st-brandCard_media">'
    + '          <div class="st-brandCard_image st-brandCard_image-collaboration"></div>'
    + '          <div class="st-brandCard_cover"></div>'
    + '          <div class="st-brandCard_note">'
    + '            <div class="st-brandCard_noteWrap">'
    + '              <span class="st-brandCard_noteText">LEXUSが展開するコラボレーションサービス</span>'
    + '            </div>'
    + '          </div>'
    + '        </div>'
    + '        <div class="st-brandCard_body st-brandCard_body-notBorderSP">'
    + '          <div class="st-brandCard_title st-brandCard_title-exception"><span class="st-brandCard_text">COLLABORATION</span></div>'
    + '        </div>'
    + '      </a>'
    + '    </div>'
    + '  </div>'
    + '  <div class="st-brandCardGroup_dots"></div>'
    + '</div>'
    + '        </div>'
    + '        <span class="st-header_navDivider"></span>'
    + '      </div>'
    + '    </div>'
    + '    <div class="st-megaNav_Item st-megaNav_Item-magazine">'
    + '      <div class="st-megaNav_itemInner">'
    + '        <p class="st-header_navTitle">'
    + '          <a class="st-header_navText st-header_navText-link" href="/magazine/" target="_blank" onclick="sc(\'lexus:header:magazine\')">MAGAZINE</a>'
    + '        </p>'
    + '        <span class="st-header_navDivider"></span>'
    + '      </div>'
    + '    </div>'
    + '  </div>'
    + '  <ul class="st-header_subNav">'
    + '    <li class="st-header_subNavItem">'
    + '      <span class="st-header_subNavLink st-header_subNavLink-tooltip">'
    + '        <p class="st-header_subNavTitle">'
    + '          <a class="st-header_subNavText u-pc" href="/technology/" onclick="sc(\'lexus:header:technology\')">先進技術</a>'
    + '          <span class="st-header_subNavText st-header_subNavText-dropdown" onclick="sc(\'lexus:header:technology\')">先進技術</span>'
    + '        </p>'
    + '        <ul class="st-header_subNavTooltip sw-tooltip">'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/technology/safety/" onclick="sc(\'lexus:header:technology-safety\')"><span class="sw-tooltip_text">予防安全パッケージ</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/technology/" onclick="sc(\'lexus:header:technology\')"><span class="sw-tooltip_text">TECHNOLOGY</span></a>'
    + '          </li>'
    + '        </ul>'
    + '      </span>'
    + '    </li>'
    + '    <li class="st-header_subNavItem">'
    + '      <span class="st-header_subNavLink st-header_subNavLink-tooltip">'
    + '        <p class="st-header_subNavTitle">'
    + '          <a class="st-header_subNavText u-pc" href="/total_care/" onclick="sc(\'lexus:header:total-care\')">サービス</a>'
    + '          <span class="st-header_subNavText st-header_subNavText-dropdown" onclick="sc(\'lexus:header:total-care\')">サービス</span>'
    + '        </p>'
    + '        <ul class="st-header_subNavTooltip sw-tooltip">'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/total_care/" onclick="sc(\'lexus:header:total-care-total-care\')"><span class="sw-tooltip_text">レクサストータルケア</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/total_care/owners_desk/" onclick="sc(\'lexus:header:total-care-owners-desk\')"><span class="sw-tooltip_text">レクサスオーナーズデスク</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/total_care/g-link/" onclick="sc(\'lexus:header:total-care-g-link\')"><span class="sw-tooltip_text">G-Link</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/total_care/lcmp/" onclick="sc(\'lexus:header:total-care-lcmp\')"><span class="sw-tooltip_text">レクサスケア メンテナンスプログラム</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/total_care/warranty/" onclick="sc(\'lexus:header:total-care-warranty\')"><span class="sw-tooltip_text">新車保証</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/total_care/hdn/" onclick="sc(\'lexus:header:total-care-hdn\')"><span class="sw-tooltip_text">ハーモニアスドライビングナビゲーター</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/total_care/others/" onclick="sc(\'lexus:header:total-care-others\')"><span class="sw-tooltip_text">その他のサービス</span></a>'
    + '          </li>'
    + '        </ul>'
    + '      </span>'
    + '    </li>'
    + '    <li class="st-header_subNavItem">'
    + '      <span class="st-header_subNavLink st-header_subNavLink-tooltip">'
    + '        <p class="st-header_subNavTitle">'
    + '          <a class="st-header_subNavText u-pc" href="/request/" onclick="sc(\'lexus:header:request\')">ご購入検討サポート</a>'
    + '          <span class="st-header_subNavText st-header_subNavText-dropdown" onclick="sc(\'lexus:header:request\')">ご購入検討サポート</span>'
    + '        </p>'
    + '        <ul class="st-header_subNavTooltip sw-tooltip">'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/request/" onclick="sc(\'lexus:header:request-request\')"><span class="sw-tooltip_text">ご購入検討サポート</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/request/simulation/" onclick="sc(\'lexus:header:request-simulation\')"><span class="sw-tooltip_text">見積りシミュレーション</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/request/catalog/service/catalogselect" onclick="sc(\'lexus:header:request-catalog\')"><span class="sw-tooltip_text">カタログ請求</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/request/trial/" onclick="sc(\'lexus:header:request-trial\')"><span class="sw-tooltip_text">試乗予約</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/request/estimate/" onclick="sc(\'lexus:header:request-estimate\')"><span class="sw-tooltip_text">見積り依頼</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/request/consult/" onclick="sc(\'lexus:header:request-consult\')"><span class="sw-tooltip_text">ご購入相談</span></a>'
    + '          </li>'
    + '        </ul>'
    + '      </span>'
    + '    </li>'
    + '    <li class="st-header_subNavItem">'
    + '      <span class="st-header_subNavLink st-header_subNavLink-tooltip">'
    + '        <p class="st-header_subNavTitle">'
    + '          <a class="st-header_subNavText u-pc" href="/dealership/" onclick="sc(\'lexus:header:dealership\')">販売店</a>'
    + '          <span class="st-header_subNavText st-header_subNavText-dropdown" onclick="sc(\'lexus:header:dealership\')">販売店</span>'
    + '        </p>'
    + '        <ul class="st-header_subNavTooltip sw-tooltip">'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/dealership/" onclick="sc(\'lexus:header:dealership-dealership\')"><span class="sw-tooltip_text">販売店検索</span></a>'
    + '          </li>'
    + '          <li class="sw-tooltip_item">'
    + '            <a class="sw-tooltip_link" href="/dealership/lexus_gallery/" onclick="sc(\'lexus:header:dealership-lexus-gallery\')"><span class="sw-tooltip_text">LEXUS GALLERY</span></a>'
    + '          </li>'
    + '        </ul>'
    + '      </span>'
    + '    </li>'
    + '    <li class="st-header_subNavItem">'
    + '      <span class="st-header_subNavLink">'
    + '        <a class="st-header_subNavText" href="http://cpo.lexus.jp/" target="_blank" onclick="sc(\'lexus:header:cpo-lexus-jp\')"><span class="u-letter-n">認定中古車</span> CPO</a>'
    + '      </span>'
    + '    </li>'
    + '    <li class="st-header_subNavItem">'
    + '      <span class="st-header_subNavLink">'
    + '        <a class="st-header_subNavText" href="/srvc/tc/top" onclick="sc(\'lexus:header:mypage\')"><span class="u-letter-n">マイページ</span></a>'
    + '      </span>'
    + '    </li>'
    + '    <li class="st-header_subNavItem">'
    + '      <span class="st-header_subNavLink">'
    + '        <a class="st-header_subNavText" href="https://lexus.g-book.com/LoginTop.aspx" target="_blank" onclick="sc(\'lexus:header:lexus-g-book-com\')"><span class="u-letter-n">オーナーズ</span> / G-Linkサイト</a>'
    + '      </span>'
    + '    </li>'
    + '  </ul>'
    + '  <div class="st-header_search">'
    + '    <form class="st-header_searchBox" action="https://site-search.lexus.jp/ja_all/search.x">'
    + '      <input type="submit" class="st-header_searchSubmit" onclick="sc(\'lexus:header:search-submit\')">'
    + '      <input class="st-header_searchInput" id="MF_form_phrase" type="text" name="q" placeholder="フリーワード検索">'
    + '      <input type="submit" class="st-header_searchSubmitSP">'
    + '    </form>'
    + '  </div>'
    + '</div>'
    + '</div>';

    var hibiyaHeader = ''
    + '<div class="st-header_inner hibiya-header">'
    + '    <div class="st-header_main u-clearfix">'
    + '        <h1 class="st-header_mainLogo"><a href="https://lexustokyo.jp" onclick="sc(\'lexus:header:lexustokyo:top\')"><img src="/common/v3/images/logo_lexus.png" alt="LEXUS"></a></h1>'
    + '    </div>'
    + '    <button class="st-header_spMenu">menu</button>'
    + '    <span class="hibiya-header_title">LEXUS TOKYO</span>'
    + '    <div class="st-header_nav">'
    + '      <ul>'
    + '        <li class="st-header_navTitle"><a href="/models/ls/" class="st-header_navText" onclick="sc(\'lexus:header:lineup-ls\');">a</a></li><span class="st-header_navDivider"></span>'
    + '        <li class="st-header_navTitle"><a href="/models/ls/" class="st-header_navText" onclick="sc(\'lexus:header:lineup-ls\');">a</a></li><span class="st-header_navDivider"></span>'
    + '      </ul>'
    + '      <ul>'
    + '        <li class="st-header_navTitle"><a href="/models/ls/" class="st-header_navText" onclick="sc(\'lexus:header:lineup-ls\');">ABOUT</a></li><span class="st-header_navDivider"></span>'
    + '        <li class="st-header_navTitle"><a href="/models/ls/" class="st-header_navText" onclick="sc(\'lexus:header:lineup-ls\');">CAFE</a></li><span class="st-header_navDivider"></span>'
    + '        <li class="st-header_navTitle"><a href="/models/ls/" class="st-header_navText" onclick="sc(\'lexus:header:lineup-ls\');">EVENT</a></li><span class="st-header_navDivider"></span>'
    + '        <li class="st-header_navTitle"><a href="/models/ls/" class="st-header_navText" onclick="sc(\'lexus:header:lineup-ls\');">BOUTIQUE</a></li><span class="st-header_navDivider"></span>'
    + '      </ul>'
    + '      <ul>'
    + '        <li class="st-header_navTitle"><a href="/models/ls/" class="st-header_navText" onclick="sc(\'lexus:header:lineup-ls\');">a</a></li><span class="st-header_navDivider"></span>'
    + '        <li class="st-header_navTitle"><a href="/models/ls/" class="st-header_navText" onclick="sc(\'lexus:header:lineup-ls\');">a</a></li><span class="st-header_navDivider"></span>'
    + '      </ul>'
    + '    </div>';
    + '</div>';

    var jpFooter = ''
    + '<div class="st-topAnchor">'
    + '  <button class="st-topAnchor_button js-scrollTo" data-anchor="#">Go to top</button>'
    + '</div><div class="st-footer_top">'
    + '  <div class="st-footer_topWrap">'
    + '    <ul class="st-footer_groupList u-clearfix">'
    + '      <li class="st-footer_groupItem st-footer_groupItem-models  st-footer_groupItem-2grid">'
    + '        <p class="st-footer_groupTitle st-footer_groupTitle-mb">'
    + '          <span class="st-footer_groupTitleText">'
    + '            <a class="st-footer_groupTitleLink u-pc" href="/#lineup" onclick="sc(\'lexus:footer:lineup\')">LINE UP</a>'
    + '            <span class="st-footer_groupTitleLink st-footer_groupTitleLink-dropdown" onclick="sc(\'lexus:footer:lineup\')">LINE UP</span>'
    + '          </span>'
    + '        </p>'
    + '        <div class="st-footer_groupContent">'
    + '          <ul class="st-footer_categoryList st-footer_categoryList-models">'
    + '            <li class="st-footer_categoryItem">'
    + '              <ul class="st-footer_linkList">'
    + '                <li class="st-footer_linkItem"><a href="/models/ls/" onclick="sc(\'lexus:footer:lineup-ls\')">LS</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/models/gs/" onclick="sc(\'lexus:footer:lineup-gs\')">GS</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/models/gsf/" onclick="sc(\'lexus:footer:lineup-gsf\')">GS F</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/models/is/" onclick="sc(\'lexus:footer:lineup-is\')">IS</a></li>'
    + '              </ul>'
    + '            </li>'
    + '            <li class="st-footer_categoryItem">'
    + '              <ul class="st-footer_linkList">'
    + '                <li class="st-footer_linkItem"><a href="/models/lc/" onclick="sc(\'lexus:footer:lineup-lc\')">LC</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/models/rc/" onclick="sc(\'lexus:footer:lineup-rc\')">RC</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/models/rcf/" onclick="sc(\'lexus:footer:lineup-rcf\')">RC F</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/models/ct/" onclick="sc(\'lexus:footer:lineup-ct\')">CT</a></li>'
    + '              </ul>'
    + '            </li>'
    + '            <li class="st-footer_categoryItem">'
    + '              <ul class="st-footer_linkList">'
    + '                <li class="st-footer_linkItem"><a href="/models/lx/" onclick="sc(\'lexus:footer:lineup-lx\')">LX</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/models/rx/" onclick="sc(\'lexus:footer:lineup-rx\')">RX</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/models/nx/" onclick="sc(\'lexus:footer:lineup-nx\')">NX</a></li>'
    + '              </ul>'
    + '            </li>'
    + '          </ul>'
    + '          <ul class="st-footer_categoryList">'
    + '            <li class="st-footer_categoryItem st-footer_categoryItem-hybrid">'
    + '              <ul class="st-footer_linkList">'
    + '                <li class="st-footer_linkItem"><a href="/models/suv/" onclick="sc(\'lexus:footer:lineup-suv\')">3SUV</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/models/f/" onclick="sc(\'lexus:footer:lineup-fsport\')">F / F SPORT</a></li>'
    + '              </ul>'
    + '            </li>'
    + '          </ul>'
    + '        </div>'
    + '      </li>'
    + '      <li class="st-footer_groupItem st-footer_groupItem-6grid">'
    + '        <p class="st-footer_groupTitle st-footer_groupTitle-mb">'
    + '          <span class="st-footer_groupTitleText">'
    + '            <a class="st-footer_groupTitleLink u-pc" href="/brand/" onclick="sc(\'lexus:footer:brand\')">BRAND</a>'
    + '            <span class="st-footer_groupTitleLink st-footer_groupTitleLink-dropdown" onclick="sc(\'lexus:footer:brand\')">BRAND</span>'
    + '          </span>'
    + '        </p>'
    + '        <div class="st-footer_groupContent">'
    + '          <div class="st-footer_categoryGroup st-footer_categoryGroup-brand u-clearfix">'
    + '            <ul class="st-footer_categoryList st-footer_categoryList-brand1">'
    + '              <li class="st-footer_categoryItem">'
    + '                <ul class="st-footer_linkList">'
    + '                  <li class="st-footer_linkItem"><a href="/brand/experiences/" onclick="sc(\'lexus:footer:brand-experiences\')">EXPERIENCES</a></li>'
    + '                  <li class="st-footer_linkItem"><a href="/brand/design/" onclick="sc(\'lexus:footer:brand-design\')">DESIGN</a></li>'
    + '                  <li class="st-footer_linkItem"><a href="/brand/technology" onclick="sc(\'lexus:footer:brand-technology\')">TECHNOLOGY</a></li>'
    + '                  <li class="st-footer_linkItem"><a href="/brand/craftsmanship/" onclick="sc(\'lexus:footer:brand-craftsmanship\')">CRAFTSMANSHIP</a></li>'
    + '                </ul>'
    + '              </li>'
    + '              <li class="st-footer_categoryItem">'
    + '                <ul class="st-footer_linkList">'
    + '                  <li class="st-footer_linkItem"><a href="/brand/quality/" onclick="sc(\'lexus:footer:brand-quality\')">QUALITY</a></li>'
    + '                  <li class="st-footer_linkItem"><a href="/brand/omotenashi/" onclick="sc(\'lexus:footer:brand-omotenashi\')">OMOTENASHI</a></li>'
    + '                </ul>'
    + '              </li>'
    + '            </ul>'
    + '            <ul class="st-footer_categoryList st-footer_categoryList-brand2">'
    + '              <li class="st-footer_categoryItem">'
    + '                <p class="st-footer_categoryTitle">ACTIVITIES</p>'
    + '                <div class="st-footer_linkGroup u-clearfix">'
    + '                  <ul class="st-footer_linkList">'
    + '                    <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/brand/global_brandad/" onclick="sc(\'lexus:footer:brand-global-brandad\')">グローバルブランド広告</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/models/concept-cars/" onclick="sc(\'lexus:footer:brand-concept-cars\')">CONCEPT CARS</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/motor_show/" onclick="sc(\'lexus:footer:brand-motor-show\')">MOTOR SHOW</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/collection/" onclick="sc(\'lexus:footer:brand-collection\')">LEXUS collection</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/intersect/tokyo/" onclick="sc(\'lexus:footer:brand-intersect\')">INTERSECT BY LEXUS</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/intersect/tokyo/crafted-for-lexus-index/" onclick="sc(\'lexus:footer:brand-crafted-for-lexus\')">CRAFTED FOR LEXUS</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/lexus-design/design_award/" onclick="sc(\'lexus:footer:brand-design-award\')">LEXUS DESIGN AWARD</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/lexus-design/design_events/" onclick="sc(\'lexus:footer:brand-design-events\')">LEXUS DESIGN EVENT</a></li>'
    + '                  </ul>'
    + '                  <ul class="st-footer_linkList">'
    + '                    <li class="st-footer_linkItem"><a href="/brand/beyond/" onclick="sc(\'lexus:footer:brand-beyond\')">BEYOND BY LEXUS</a></li>'
    + '                    <li class="st-footer_linkItem st-footer_letter-stuffing"><a href="/brand/amazing_experience/" onclick="sc(\'lexus:footer:brand-amazing-experience\')">LEXUS AMAZING EXPERIENCE</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/shortfilms/" onclick="sc(\'lexus:footer:brand-shortfilms\')">LEXUS SHORT FILMS</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/hideki_matsuyama/" onclick="sc(\'lexus:footer:brand-hideki-matsuyama\')">HIDEKI MATSUYAMA</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/yoshihide_muroya/" onclick="sc(\'lexus:footer:brand-yoshihide-muroya\')">YOSHIHIDE MUROYA</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/motorsport/" onclick="sc(\'lexus:footer:brand-motorsport\')">MOTORSPORT</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/dining_out/" onclick="sc(\'lexus:footer:brand-dining_out\')">DINING OUT</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/new-takumi/" onclick="sc(\'lexus:footer:brand-new-takumi\')">LEXUS NEW TAKUMI PROJECT</a></li>'
    + '                    <li class="st-footer_linkItem"><a href="/brand/collaboration/" onclick="sc(\'lexus:footer:brand-collaboration\')">COLLABORATION</a></li>'
    + '                  </ul>'
    + '                </div>'
    + '              </li>'
    + '            </ul>'
    + '          </div>'
    + '        </div>'
    + '      </li>'
    + '      <li class="st-footer_groupItem st-footer_groupItem-models  st-footer_groupItem-2grid">'
    + '        <p class="st-footer_groupTitle st-footer_groupTitle-mb">'
    + '          <span class="st-footer_groupTitleText">'
    + '            <span class="st-footer_groupTitleLink u-pc">TECHNOLOGY</span>'
    + '            <span class="st-footer_groupTitleLink st-footer_groupTitleLink-dropdown" onclick="sc(\'lexus:footer:technology\')">先進技術</span>'
    + '          </span>'
    + '        </p>'
    + '        <div class="st-footer_groupContent">'
    + '          <ul class="st-footer_categoryList">'
    + '            <li class="st-footer_categoryItem">'
    + '              <ul class="st-footer_linkList">'
    + '                <li class="st-footer_linkItem"><a href="/technology/safety/" onclick="sc(\'lexus:footer:technology-safety\')">予防安全パッケージ</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/technology/" onclick="sc(\'lexus:footer:technology\')">TECHNOLOGY</a></li>'
    + '                <li class="st-footer_linkItem"><a href="/brand/technology/itsconnect/" onclick="sc(\'lexus:footer:technology-itsconnect\')">ITS CONNECT</a></li>'
    + '              </ul>'
    + '            </li>'
    + '          </ul>'
    + '        </div>'
    + '      </li>'
    + '    </ul>'
    + '  </div>'
    + '</div>'
    + '<div class="st-footer_bottom">'
    + '  <div class="st-footer_bottomWrap">'
    + '    <ul class="st-footer_groupList u-clearfix">'
    + '      <li class="st-footer_groupItem st-footer_groupItem-2grid">'
    + '        <p class="st-footer_groupTitle st-footer_groupTitle-mb">'
    + '          <span class="st-footer_groupTitleText st-footer_groupTitleText-jp">'
    + '            <span class="st-footer_groupTitleLink u-pc">お知らせ</span>'
    + '            <span class="st-footer_groupTitleLink st-footer_groupTitleLink-dropdown" onclick="sc(\'lexus:footer:notice\')">お知らせ</span>'
    + '          </span>'
    + '        </p>'
    + '        <div class="st-footer_groupContent">'
    + '          <ul class="st-footer_categoryList">'
    + '            <li class="st-footer_categoryItem">'
    + '              <ul class="st-footer_linkList">'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/pressrelease/" onclick="sc(\'lexus:footer:notice-pressrelease\')">プレスリリース</a></li>'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/news/" onclick="sc(\'lexus:footer:notice-news\')">トピックス＆イベント</a></li>'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/recall/" onclick="sc(\'lexus:footer:notice-recall\')"><span class="u-letter-n">リコール情報</span></a></li>'
    + '              </ul>'
    + '            </li>'
    + '          </ul>'
    + '        </div>'
    + '      </li>'
    + '      <li class="st-footer_groupItem st-footer_groupItem-2grid">'
    + '        <p class="st-footer_groupTitle st-footer_groupTitle-mb">'
    + '          <span class="st-footer_groupTitleText st-footer_groupTitleText-jp">'
    + '            <span class="st-footer_groupTitleLink u-pc">サービス</span>'
    + '            <span class="st-footer_groupTitleLink st-footer_groupTitleLink-dropdown" onclick="sc(\'lexus:footer:total-care\')">サービス</span>'
    + '          </span>'
    + '        </p>'
    + '        <div class="st-footer_groupContent">'
    + '          <ul class="st-footer_categoryList">'
    + '            <li class="st-footer_categoryItem">'
    + '              <ul class="st-footer_linkList">'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/total_care/" onclick="sc(\'lexus:footer:total-care-total-care\')"><span class="u-letter-n">レクサストータルケア</span></a></li>'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/total_care/owners_desk/" onclick="sc(\'lexus:footer:total-care-owners-desk\')"><span class="u-letter-n">レクサスオーナーズデスク</span></a></li>'
    + '                <li class="st-footer_linkItem"><a href="/total_care/g-link/" onclick="sc(\'lexus:footer:total-care-g-link\')"><span class="u-letter-n">G-Link</span></a></li>'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/total_care/lcmp/" onclick="sc(\'lexus:footer:total-care-lcmp\')"><span class="u-letter-n">レクサスケア<br class="u-pc">メンテナンスプログラム</span></a></li>'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/total_care/warranty/" onclick="sc(\'lexus:footer:total-care-warranty\')"><span class="u-letter-n">新車保証</span></a></li>'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/total_care/hdn/" onclick="sc(\'lexus:footer:total-care-hdn\')"><span class="u-letter-n">ハーモニアス<br class="u-pc">ドライビングナビゲーター</span></a></li>'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/total_care/others/" onclick="sc(\'lexus:footer:total-care-others\')"><span class="u-letter-n">その他のサービス</span></a></li>'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp st-footer_linkItem-exception"><a href="/total_care/trouble/" onclick="sc(\'lexus:footer:total-care-trouble\')"><span class="u-letter-n">困った時の対応</span></a></li>'
    + '              </ul>'
    + '            </li>'
    + '          </ul>'
    + '        </div>'
    + '      </li>'
    + '      <li class="st-footer_groupItem st-footer_groupItem-4grid">'
    + '        <p class="st-footer_groupTitle st-footer_groupTitle-mb">'
    + '          <span class="st-footer_groupTitleText st-footer_groupTitleText-jp">'
    + '            <span class="st-footer_groupTitleLink u-pc">ご購入検討サポート</span>'
    + '            <span class="st-footer_groupTitleLink st-footer_groupTitleLink-dropdown" onclick="sc(\'lexus:footer:request\')">ご購入検討サポート</span>'
    + '          </span>'
    + '        </p>'
    + '        <div class="st-footer_groupContent">'
    + '          <div class="st-footer_categoryGroup u-clearfix">'
    + '            <ul class="st-footer_categoryList">'
    + '              <li class="st-footer_categoryItem">'
    + '                <ul class="st-footer_linkList">'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/request/" onclick="sc(\'lexus:footer:request-request\')"><span class="u-letter-n">ご購入検討サポート</span></a></li>'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/request/finance/" onclick="sc(\'lexus:footer:request-finance\')"><span class="u-letter-n">ファイナンシャルサービス</span></a></li>'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/request/finance/simulation/" onclick="sc(\'lexus:footer:request-finance-simulation\')"><span class="u-letter-n">ローン支払い額シミュレーション</span></a></li>'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/request/policyplan/" onclick="sc(\'lexus:footer:request-policyplan\')"><span class="u-letter-n">自動車保険プラン</span></a></li>'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/request/zeisei/" onclick="sc(\'lexus:footer:request-zeisei\')"><span class="u-letter-n">エコカー減税</span></a></li>'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="http://l-nenpi.jp/" onclick="sc(\'lexus:footer:request-l-nenpi-jp\')"><span class="u-letter-n">他車比較シミュレーション</span></a></li>'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/request/simulation/" onclick="sc(\'lexus:footer:request-simulation\')"><span class="u-letter-n">見積りシミュレーション</span></a></li>'
    + '                </ul>'
    + '              </li>'
    + '            </ul>'
    + '            <ul class="st-footer_categoryList">'
    + '              <li class="st-footer_categoryItem">'
    + '                <ul class="st-footer_linkList">'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/mailnews/" onclick="sc(\'lexus:footer:request-mailnews\')"><span class="u-letter-n">メールニュース</span></a></li>'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/request/catalog/service/catalogselect" onclick="sc(\'lexus:footer:request-catalog\')"><span class="u-letter-n">カタログ請求</span></a></li>'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/request/trial/" onclick="sc(\'lexus:footer:request-trial\')"><span class="u-letter-n">試乗予約</span></a></li>'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/request/estimate/" onclick="sc(\'lexus:footer:request-estimate\')"><span class="u-letter-n">見積り依頼</span></a></li>'
    + '                  <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/request/consult/" onclick="sc(\'lexus:footer:request-consult\')"><span class="u-letter-n">ご購入相談</span></a></li>'
    + '                </ul>'
    + '              </li>'
    + '            </ul>'
    + '          </div>'
    + '        </div>'
    + '      </li>'
    + '      <li class="st-footer_groupItem st-footer_groupItem-2grid">'
    + '        <p class="st-footer_groupTitle st-footer_groupTitle-mb">'
    + '          <span class="st-footer_groupTitleText st-footer_groupTitleText-jp">'
    + '            <span class="st-footer_groupTitleLink u-pc">販売店</span>'
    + '            <span class="st-footer_groupTitleLink st-footer_groupTitleLink-dropdown" onclick="sc(\'lexus:footer:dealership\')">販売店</span>'
    + '          </span>'
    + '        </p>'
    + '        <div class="st-footer_groupContent">'
    + '          <ul class="st-footer_categoryList">'
    + '            <li class="st-footer_categoryItem">'
    + '              <ul class="st-footer_linkList">'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/dealership/" onclick="sc(\'lexus:footer:dealership-dealership\')"><span class="u-letter-n">販売店検索</span></a></li>'
    + '                <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/dealership/lexus_gallery/" onclick="sc(\'lexus:footer:dealership-lexus-gallery\')">LEXUS GALLERY</a></li>'
    + '              </ul>'
    + '            </li>'
    + '          </ul>'
    + '        </div>'
    + '      </li>'
    + '      <li class="st-footer_groupItem st-footer_groupItem-other">'
    + '        <ul class="st-footer_categoryList">'
    + '          <li class="st-footer_categoryItem">'
    + '            <ul class="st-footer_linkList">'
    + '              <li class="st-footer_linkItem"><a href="http://cpo.lexus.jp/" target="_blank" onclick="sc(\'lexus:footer:cpo-lexus-jp\')"><span class="u-letter-n">認定中古車</span> CPO</a></li>'
    + '              <li class="st-footer_linkItem st-footer_linkItem-jp"><a href="/srvc/tc/top" onclick="sc(\'lexus:footer:mypage\')">マイページ</a></li>'
    + '              <li class="st-footer_linkItem"><a href="https://lexus.g-book.com/LoginTop.aspx" target="_blank" onclick="sc(\'lexus:footer:lexus-g-book-com\')"><span class="u-letter-n">レクサスオーナーズ</span> / G-Linkサイト</a></li>'
    + '            </ul>'
    + '          </li>'
    + '        </ul>'
    + '      </li>'
    + '    </ul>'
    + '    <div class="st-footer_additional">'
    + '      <div class="st-footer_sns">'
    + '        <ul class="st-footer_snsList">'
    + '          <li class="st-footer_snsItem"><a class="st-footer_snsIcon st-footer_snsIcon-fb" href="https://www.facebook.com/LexusJP/" target="_blank" onclick="sc(\'lexus:footer:facebook-com\')">facebook</a></li>'
    + '          <li class="st-footer_snsItem"><a class="st-footer_snsIcon st-footer_snsIcon-tw" href="https://twitter.com/lexus_jpn" target="_blank" onclick="sc(\'lexus:footer:twitter-com\')">twitter</a></li>'
    + '          <li class="st-footer_snsItem"><a class="st-footer_snsIcon st-footer_snsIcon-yt" href="https://www.youtube.com/user/lexusjpchannel" target="_blank" onclick="sc(\'lexus:footer:youtube-com\')">youtube</a></li>'
    + '        </ul>'
    + '      </div>'
    + '      <ul class="st-footer_additionalList">'
    + '        <li class="st-footer_additionalItem st-footer_additionalItem-info">'
    + '          <small class="st-footer_copyright">&copy; 2005-2018 LEXUS</small>'
    + '          <span class="st-footer_english"><a href="/international/" onclick="sc(\'lexus:footer:international\')">lexus.jp (EN)</a></span>'
    + '        </li>'
    + '        <li class="st-footer_additionalItem"><a href="/privacy_policy/" onclick="sc(\'lexus:footer:privacy-policy\')">プライバシーポリシー</a></li>'
    + '        <li class="st-footer_additionalItem"><a href="/terms_of_use/" onclick="sc(\'lexus:footer:terms-of-use\')">ご利用に際して</a></li>'
    + '        <li class="st-footer_additionalItem"><a href="/information_desk/" onclick="sc(\'lexus:footer:information-desk\')">お問い合わせ</a></li>'
    + '        <li class="st-footer_additionalItem st-footer_additionalItem-global"><a href="https://www.lexus-int.com" target="_blank" onclick="sc(\'lexus:footer:lexus-int-com\')">LEXUS INTERNATIONAL</a></li>'
    + '      </ul>'
    + '      <button class="st-footer_additionalTopanchor js-scrollTo" data-anchor="#">Go to top</button>'
    + '    </div>'
    + '  </div>'
    + '</div>';

    var hibiyaFooter = ''
    /*
    + '<div class="st-topAnchor">'
    + '  <button class="st-topAnchor_button js-scrollTo" data-anchor="#">Go to top</button>'
    + '</div>'
    */
    + '<div class="hibiya-footer">'
    /*
    + '  <div class="st-footer_bottomWrap">'
    + '    <ul class="st-footer_groupList u-clearfix">'
    + '      <li class="st-footer_linkItem st-footer_linkItem-jp">'
    + '        <ul class="st-footer_categoryList">'
    + '          <li class="st-footer_categoryItem">'
    + '            <ul class="st-footer_linkList">'
    + '              <li class="st-footer_linkItem"><a href="http://cpo.lexus.jp/" target="_blank" onclick="sc(\'lexus:footer:cpo-lexus-jp\')">認定中古車</a></li>'
    + '              <li class="st-footer_linkItem"><a href="http://cpo.lexus.jp/" target="_blank" onclick="sc(\'lexus:footer:cpo-lexus-jp\')">認定中古車</a></li>'
    + '            </ul>'
    + '            <ul class="st-footer_linkList">'
    + '              <li class="st-footer_linkItem"><a href="http://cpo.lexus.jp/" target="_blank" onclick="sc(\'lexus:footer:cpo-lexus-jp\')">ABOUT</a></li>'
    + '              <li class="st-footer_linkItem"><a href="http://cpo.lexus.jp/" target="_blank" onclick="sc(\'lexus:footer:cpo-lexus-jp\')">CAFE</a></li>'
    + '              <li class="st-footer_linkItem"><a href="http://cpo.lexus.jp/" target="_blank" onclick="sc(\'lexus:footer:cpo-lexus-jp\')">EVENT</a></li>'
    + '              <li class="st-footer_linkItem"><a href="http://cpo.lexus.jp/" target="_blank" onclick="sc(\'lexus:footer:cpo-lexus-jp\')">BOUTIQUE</a></li>'
    + '            </ul>'
    + '            <ul class="st-footer_linkList">'
    + '              <li class="st-footer_linkItem"><a href="http://cpo.lexus.jp/" target="_blank" onclick="sc(\'lexus:footer:cpo-lexus-jp\')">認定中古車</a></li>'
    + '              <li class="st-footer_linkItem"><a href="http://cpo.lexus.jp/" target="_blank" onclick="sc(\'lexus:footer:cpo-lexus-jp\')">認定中古車</a></li>'
    + '            </ul>'
    + '          </li>'
    + '        </ul>'
    + '      </li>'
    + '    </ul>'
    + '    <div class="st-footer_additional">'
    + '      <div class="st-footer_sns">'
    + '        <ul class="st-footer_snsList">'
    + '          <li class="st-footer_snsItem"><a class="st-footer_snsIcon st-footer_snsIcon-fb" href="https://www.facebook.com/LexusJP/" target="_blank" onclick="sc(\'lexus:footer:facebook-com\')">facebook</a></li>'
    + '          <li class="st-footer_snsItem"><a class="st-footer_snsIcon st-footer_snsIcon-tw" href="https://twitter.com/lexus_jpn" target="_blank" onclick="sc(\'lexus:footer:twitter-com\')">twitter</a></li>'
    + '          <li class="st-footer_snsItem"><a class="st-footer_snsIcon st-footer_snsIcon-yt" href="https://www.youtube.com/user/lexusjpchannel" target="_blank" onclick="sc(\'lexus:footer:youtube-com\')">youtube</a></li>'
    + '        </ul>'
    + '      </div>'
    + '      <ul class="st-footer_additionalList">'
    + '        <li class="st-footer_additionalItem st-footer_additionalItem-info">'
    + '          <small class="st-footer_copyright">© 2017 LEXUS</small>'
    + '          <span class="st-footer_english"><a href="/international/" onclick="sc(\'lexus:footer:international\')">lexus.jp (EN)</a></span>'
    + '        </li>'
    + '        <li class="st-footer_additionalItem"><a href="/privacy_policy/" onclick="sc(\'lexus:footer:privacy-policy\')">プライバシーポリシー</a></li>'
    + '        <li class="st-footer_additionalItem"><a href="/terms_of_use/" onclick="sc(\'lexus:footer:terms-of-use\')">ご利用に際して</a></li>'
    + '        <li class="st-footer_additionalItem"><a href="/information_desk/" onclick="sc(\'lexus:footer:information-desk\')">お問い合わせ</a></li>'
    + '        <li class="st-footer_additionalItem st-footer_additionalItem-global"><a href="https://www.lexus-int.com" target="_blank" onclick="sc(\'lexus:footer:lexus-int-com\')">LEXUS INTERNATIONAL</a></li>'
    + '      </ul>'
    + '      <button class="st-footer_additionalTopanchor js-scrollTo" data-anchor="#">Go to top</button>'
    + '    </div>'
    + '  </div>'
    */
    + '</div>';

    //URLパラメータを取得
    var param  = new Object;
    var paramUrl = location.search.substring(1).split('&');
    var url   = location.href;
    var paramRef = '',
        paramsymbol = '?p=',
        link,
        addParam,
        prevhash,
        addlink;

    if (paramUrl !== undefined) {
        for (var i = 0; paramUrl[i]; i++) {
            var k = paramUrl[i].split('=');
            param[k[0]] = k[1];
        }
        paramRef = param.p;
    }

    var flag = (paramRef === 'hibiya' || paramRef === 'l-tokyo');

    if (flag) {
      $('a').each(function() {
        link = $(this).attr('href');

        if(link !== undefined) {
          (link.indexOf('?') != -1) ? paramsymbol = '&p=' : paramsymbol = '?p=';
          addParam = link + paramsymbol + paramRef;
          if(link.slice(0,1) !== '#' && !(link.match('javascript'))) {
            if(link.match('#')) {
              prevhash = link.split('#');
              addlink = link.slice(0, -prevhash.length-1);
              $(this).attr('href', addlink + '#' + prevhash[1] + paramsymbol + paramRef);
            } else {
              $(this).attr('href', addParam);
            }
          }
        }
      })
    }

    //ヘッダー,フッター分岐処理
    var Header = jpHeader;
    var Footer = jpFooter;
    var stHeader = $('.st-header');
    var stFooter = $('.st-footer');
    var jpNav = $('.st-header + nav');
    var jpNav2 = $('.rq-head');
    if (flag) Header = hibiyaHeader, Footer = hibiyaFooter, jpNav.addClass('none'), jpNav2.addClass('none'), stFooter.addClass('hibiya-footer'), stHeader.addClass('hibiya-header-line');
    APP.config.headerHTML = Header;
    APP.config.footerHTML = Footer;

/**
 * @constant modelsData
 *
 */
(function(window, $, _, APP) {

  var modelsData = [
    {
      id: 'ls',
      name: 'LS',
      type: ['all', 'sedan', 'hybrid', 'fsport'],
      typeIndex: {
        all: 1,
        sedan: 1,
        hybrid: 1,
        fsport: 3
      },
      grade: {
        all: 'LS500h / LS500',
        sedan:  'LS500h / LS500',
        hybrid: 'LS500h',
        fsport: 'LS500h / LS500'
      },
      size: {
        all: 'D 5,235×W 1,900×H 1,450～',
        sedan: 'D 5,235×W 1,900×H 1,450～',
        hybrid: 'D 5,235×W 1,900×H 1,450～',
        fsport: 'D 5,235×W 1,900×H 1,450～'
      },
      sizeIndex: {
        all: 1,
        sedan: 1,
        hybrid: 1,
        fsport: 1
      },
      price: {
        all: '9,800,000円～',
        sedan: '9,800,000円～',
        hybrid: '11,200,000円～',
        fsport: '12,000,000円～'
      },
      priceIndex: {
        all: 5,
        sedan: 2,
        hybrid: 2,
        fsport: 1
      },
      fuel: {
        all: '9.5～16.4km/L',
        sedan: '9.5～16.4km/L',
        hybrid: '14.4～16.4km/L',
        fsport: '9.5～15.6km/L'
      },
      fuelIndex: {
        all: 8,
        sedan: 4,
        hybrid: 8,
        fsport: 7
      }
    },
    {
      id: 'gs',
      name: 'GS',
      type: ['all', 'sedan', 'hybrid', 'fsport'],
      typeIndex: {
        all: 2,
        sedan: 2,
        hybrid: 2,
        fsport: 4
      },
      grade: {
        all: 'GS450h / GS350 /<br>GS300h / GS300',
        sedan: 'GS450h / GS350 /<br>GS300h / GS300',
        hybrid: 'GS450h / GS300h',
        fsport: 'GS450h / GS350 /<br>GS300h / GS300'
      },
      size: {
        all: 'D 4,880×W 1,840×H 1,455～',
        sedan: 'D 4,880×W 1,840×H 1,455～',
        hybrid: 'D 4,880×W 1,840×H 1,455',
        fsport: 'D 4,880×W 1,840×H 1,455～'
      },
      sizeIndex: {
        all: 5,
        sedan: 3,
        hybrid: 4,
        fsport: 4
      },
      price: {
        all: '5,770,000円～',
        sedan: '5,770,000円～',
        hybrid: '6,153,000円～',
        fsport: '6,599,000円～'
      },
      priceIndex: {
        all: 6,
        sedan: 3,
        hybrid: 3,
        fsport: 4
      },
      fuel: {
        all: '9.9～23.2km/L',
        sedan: '9.9～23.2km/L',
        hybrid: '18.2～23.2km/L',
        fsport: '10.0～23.2km/L'
      },
      fuelIndex: {
        all: 3,
        sedan: 2,
        hybrid: 4,
        fsport: 2
      }
    },
    {
      id: 'gsf',
      name: 'GS F',
      type: ['all', 'sedan', 'fsport'],
      typeF: true,
      typeIndex: {
        all: 3,
        sedan: 3,
        fsport: 1
      },
      grade: {
        all: '',
        sedan: '',
        fsport: ''
      },
      size: {
        all: 'D 4,915×W 1,855×H 1,440',
        sedan: 'D 4,915×W 1,855×H 1,440',
        fsport: 'D 4,915×W 1,855×H 1,440'
      },
      sizeIndex: {
        all: 3,
        sedan: 2,
        fsport: 2
      },
      price: {
        all: '11,120,000円',
        sedan: '11,120,000円',
        fsport: '11,120,000円'
      },
      priceIndex: {
        all: 3,
        sedan: 1,
        fsport: 2
      },
      fuel: {
        all: '8.2km/L',
        sedan: '8.2km/L',
        fsport: '8.2km/L'
      },
      fuelIndex: {
        all: 9,
        sedan: 4,
        fsport: 8
      }
    },
    {
      id: 'is',
      name: 'IS',
      type: ['all', 'sedan', 'hybrid', 'fsport'],
      typeIndex: {
        all: 4,
        sedan: 4,
        hybrid: 3,
        fsport: 5
      },
      grade: {
        all: 'IS350 / IS300h /<br>IS300',
        sedan: 'IS350 / IS300h /<br>IS300',
        hybrid: 'IS300h',
        fsport: 'IS350 / IS300h /<br>IS300'
      },
      size: {
        all: 'D4,680×W 1,810×H 1,430',
        sedan: 'D 4,680×W 1,810×H 1,430',
        hybrid: 'D 4,680×W 1,810×H 1,430',
        fsport: 'D 4,680×W 1,810×H 1,430'
      },
      sizeIndex: {
        all: 9,
        sedan: 4,
        hybrid: 6,
        fsport: 7
      },
      price: {
        all: '4,700,000円～',
        sedan: '4,700,000円～',
        hybrid: '5,150,000円～',
        fsport: '5,204,000円～'
      },
      priceIndex: {
        all: 9,
        sedan: 4,
        hybrid: 6,
        fsport: 7
      },
      fuel: {
        all: '10.4～23.2km/L',
        sedan: '10.0～23.2km/L',
        hybrid: '20.4～23.2km/L',
        fsport: '10.0～23.2km/L'
      },
      fuelIndex: {
        all: 2,
        sedan: 1,
        hybrid: 3,
        fsport: 3
      }
    },
    {
      id: 'lc',
      name: 'LC',
      type: ['all', 'coupe', 'hybrid'],
      typeIndex: {
        all: 5,
        coupe: 1,
        hybrid: 4
      },
      grade: {
        all: 'LC500h / LC500',
        coupe: 'LC500h / LC500',
        hybrid: 'LC500h'
      },
      size: {
        all: 'D 4,770×W 1,920×H 1,345',
        coupe: 'D 4,770×W 1,920×H 1,345',
        hybrid: 'D 4,770×W 1,920×H 1,345'
      },
      sizeIndex: {
        all: 6,
        coupe: 1,
        hybrid: 4
      },
      price: {
        all: '13,000,000円～',
        coupe: '13,000,000円～',
        hybrid: '13,500,000円～'
      },
      priceIndex: {
        all: 1,
        coupe: 1,
        hybrid: 1
      },
      fuel: {
        all: '7.8～15.8km/L',
        coupe: '7.8～15.8km/L',
        hybrid: '15.8km/L',
      },
      fuelIndex: {
        all: 8,
        coupe: 2,
        hybrid: 8
      }
    },
    {
      id: 'rc',
      name: 'RC',
      type: ['all', 'coupe', 'hybrid', 'fsport'],
      typeIndex: {
        all: 6,
        coupe: 2,
        hybrid: 5,
        fsport: 6
      },
      grade: {
        all: 'RC350 / RC300h /<br>RC300',
        coupe: 'RC350 / RC300h /<br>RC300',
        hybrid: 'RC300h',
        fsport: 'RC350 / RC300h /<br>RC300'
      },
      size: {
        all: 'D 4,695×W 1,840×H 1,395',
        coupe: 'D 4,695×W 1,840×H 1,395',
        hybrid: 'D 4,695×W 1,840×H 1,395',
        fsport: 'D 4,695×W 1,840×H 1,395'
      },
      sizeIndex: {
        all: 8,
        coupe: 3,
        hybrid: 5,
        fsport: 6
      },
      price: {
        all: '5,300,000円～',
        coupe: '5,300,000円～',
        hybrid: '5,740,000円～',
        fsport: '5,920,000円～'
      },
      priceIndex: {
        all: 7,
        coupe: 3,
        hybrid: 5,
        fsport: 5
      },
      fuel: {
        all: '10.2～23.2km/L',
        coupe: '10.2～23.2km/L',
        hybrid: '23.2km/L',
        fsport: '10.2～23.2km/L'
      },
      fuelIndex: {
        all: 4,
        coupe: 1,
        hybrid: 2,
        fsport: 4
      }
    },
    {
      id: 'rcf',
      name: 'RC F',
      type: ['all', 'coupe', 'fsport'],
      typeF: true,
      typeIndex: {
        all: 7,
        coupe: 3,
        fsport: 2,
      },
      grade: {
        all: '',
        coupe: '',
        fsport: '',
      },
      size: {
        all: 'D 4,705×W 1,850×H 1,390',
        coupe: 'D 4,705×W 1,850×H 1,390',
        fsport: 'D 4,705×W 1,850×H 1,390',
      },
      sizeIndex: {
        all: 7,
        coupe: 2,
        fsport: 5,
      },
      price: {
        all: '9,824,000円～',
        coupe: '9,824,000円～',
        fsport: '9,824,000円～',
      },
      priceIndex: {
        all: 3,
        coupe: 2,
        fsport: 3,
      },
      fuel: {
        all: '8.2km/L',
        coupe: '8.2km/L',
        fsport: '8.2km/L'
      },
      fuelIndex: {
        all: 10,
        coupe: 3,
        fsport: 9
      }
    },
    {
      id: 'ct',
      name: 'CT',
      type: ['all', 'hybrid', 'fsport'],
      typeIndex: {
        all: 8,
        hybrid: 6,
        fsport: 7
      },
      grade: {
        all: 'CT200h',
        hybrid: 'CT200h',
        fsport: 'CT200h'
      },
      size: {
        all: 'D 4,355×W 1,765×H 1,450～',
        hybrid: 'D 4,355×W 1,765×H 1,450～',
        fsport: 'D 4,355×W 1,765×H 1,460'
      },
      sizeIndex: {
        all: 11,
        hybrid: 8,
        fsport: 9
      },
      price: {
        all: '3,770,000円～',
        hybrid: '3,770,000円～',
        fsport: '4,400,000円～'
      },
      priceIndex: {
        all: 11,
        hybrid: 8,
        fsport: 9
      },
      fuel: {
        all: '26.6～30.4km/L',
        hybrid: '26.6～30.4km/L',
        fsport: '26.6km/L'
      },
      fuelIndex: {
        all: 1,
        hybrid: 1,
        fsport: 1
      }
    },
    {
      id: 'lx',
      name: 'LX',
      type: ['all', 'suv'],
      typeIndex: {
        all: 9,
        suv: 1
      },
      grade: {
        all: 'LX570',
        suv: 'LX570'
      },
      size: {
        all: 'D 5,080×W 1,980×H 1,910',
        suv: 'D 5,080×W 1,980×H 1,910'
      },
      sizeIndex: {
        all: 2,
        suv: 1
      },
      price: {
        all: '11,150,000円',
        suv: '11,150,000円'
      },
      priceIndex: {
        all: 2,
        suv: 1
      },
      fuel: {
        all: '6.5km/L',
        suv: '6.5km/L',
      },
      fuelIndex: {
        all: 11,
        suv: 3
      }
    },
    {
      id: 'rx',
      name: 'RX',
      type: ['all', 'suv', 'hybrid', 'fsport'],
      typeIndex: {
        all: 10,
        suv: 2,
        hybrid: 7,
        fsport: 8
      },
      grade: {
        all: 'RX450hL / RX450h /<br>RX300',
        suv: 'RX450hL / RX450h /<br>RX300',
        hybrid: 'RX450hL / RX450h',
        fsport: 'RX450h / RX300',
      },
      size: {
        all: 'D 4,890×W 1,895×H 1,710～',
        suv: 'D 4,890×W 1,895×H 1,710～',
        hybrid: 'D 4,890×W 1,895×H 1,710～',
        fsport: 'D 4,890×W 1,895×H 1,710'
      },
      sizeIndex: {
        all: 4,
        suv: 2,
        hybrid: 3,
        fsport: 3
      },
      price: {
        all: '4,972,000円～',
        suv: '4,972,000円～',
        hybrid: '6,047,000円～',
        fsport: '5,801,000円～'
      },
      priceIndex: {
        all: 8,
        suv: 2,
        hybrid: 4,
        fsport: 6
      },
      fuel: {
        all: '11.2～18.8km/L',
        suv: '11.2～18.8km/L',
        hybrid: '17.8～18.8km/L',
        fsport: '11.4～18.8km/L'
      },
      fuelIndex: {
        all: 6,
        suv: 2,
        hybrid: 6,
        fsport: 6
      }
    },
    {
      id: 'nx',
      name: 'NX',
      type: ['all', 'suv', 'hybrid', 'fsport'],
      typeIndex: {
        all: 11,
        suv: 3,
        hybrid: 8,
        fsport: 9
      },
      grade: {
        all: 'NX300h / NX300',
        suv: 'NX300h / NX300',
        hybrid: 'NX300h',
        fsport: 'NX300h / NX300',
      },
      size: {
        all: 'D 4,640×W 1,845×H 1,645',
        suv: 'D 4,640×W 1,845×H 1,645',
        hybrid: 'D 4,640×W 1,845×H 1,645',
        fsport: 'D 4,640×W 1,845×H 1,645'
      },
      sizeIndex: {
        all: 10,
        suv: 3,
        hybrid: 7,
        fsport: 8
      },
      price: {
        all: '4,400,000円～',
        suv: '4,400,000円～',
        hybrid: '5,040,000円～',
        fsport: '5,040,000円～'
      },
      priceIndex: {
        all: 9,
        suv: 3,
        hybrid: 7,
        fsport: 8
      },
      fuel: {
        all: '12.4～21.0km/L',
        suv: '12.4～21.0km/L',
        hybrid: '19.8～21.0km/L',
        fsport: '12.4～19.8km/L'
      },
      fuelIndex: {
        all: 5,
        suv: 1,
        hybrid: 5,
        fsport: 5
      }
    }
  ];

  APP.config.modelsData = modelsData;
}(window, jQuery, _, APP));

//計測タグ読み込み
var s = document.createElement('script');
s.type = 'text/javascript';
s.src = '/common/v3/js/analytics.js';
(document.getElementsByTagName('body')[0]).appendChild(s);

/**
 * @module utils
 */
(function(window, $, _, APP) {
  var hasProp = {}.hasOwnProperty;

  /**
   * inherits
   * @param {Function} child
   * @param {Function}  parent
   */
  function inherits (child, parent) {
    for (var key in parent) {
      if (hasProp.call(parent, key)) {
        child[key] = parent[key];
      }
    }

    function ctor() {
      this.constructor = child;
    }

    ctor.prototype = parent.prototype;
    child.prototype = new ctor();
    child.__super__ = parent.prototype;

    return child;
  }

  /**
   * bind
   * @param {Function} fn
   * @param {Object} me
   */
  function bind(fn, me) {
    return function() {
      return fn.apply(me, arguments);
    };
  }

  /**
   * 親要素のボックスサイズに合わせてimgもしくはvideo要素をフィットさせる
   * @param {jQuery} $parent - 親要素
   * @param {jQuery}  $children - ターゲット要素（cssでposition: absoluteの指定が必須）
   */
  function fixedParentElement($wrap, $target) {
    var wrapWidth = $wrap.width();
    var wrapHeight = $wrap.height();
    var wrapRatio = wrapWidth / wrapHeight;
    var targetWidth = $target.attr('width');
    var targetHeight = $target.attr('height');
    var targetRatio = targetWidth / targetHeight;
    var width, height, left, top;

    if (wrapRatio > targetRatio) {
      width = wrapWidth;
      height = width / targetRatio;
    } else {
      height = wrapHeight;
      width = height * targetRatio;
    }

    left = (width > wrapWidth) ? (wrapWidth - width) / 2 : 0;
    top = (height > wrapHeight) ? (wrapHeight - height) / 2 : 0;

    $target.css({
      width: width,
      height: height,
      left: left,
      top: top
    });
  }

  /**
   * 親要素のボックスサイズに合わせてimgもしくはvideo収める
   * @param {jQuery} $parent - 親要素
   * @param {jQuery}  $children - ターゲット要素（cssでposition: absoluteの指定が必須）
   */
  function putInParentElement($wrap, $target) {
    var wrapWidth = $wrap.width();
    var wrapHeight = $wrap.height();
    var wrapRatio = wrapWidth / wrapHeight;
    var targetWidth = $target.attr('width');
    var targetHeight = $target.attr('height');
    var targetRatio = targetWidth / targetHeight;
    var width, height, left, top;

    if (wrapRatio > targetRatio) {
      height = wrapHeight;
      width = height * targetRatio;
      top = 0;
      left = (wrapWidth - width) / 2;
    } else {
      width = wrapWidth;
      height = width / targetRatio;
      top = (wrapHeight - height) / 2;
      left = 0;
    }

    $target.css({
      width: width,
      height: height,
      left: left,
      top: top
    });
  }

  /**
   * ↑max size
   * @param {jQuery} $parent - 親要素
   * @param {jQuery}  $children - ターゲット要素（cssでposition: absoluteの指定が必須）
   */
  function putInParentElementMaxSize($wrap, $target) {
    var wrapWidth = $wrap.width();
    var wrapHeight = $wrap.height();
    var wrapRatio = wrapWidth / wrapHeight;
    var targetWidth = $target.attr('width');
    var targetHeight = $target.attr('height');
    var targetRatio = targetWidth / targetHeight;
    var width, height, left, top;

    if (wrapRatio > targetRatio) {
      height = wrapHeight;
      if (height > targetHeight) { height = targetHeight; }
      width = height * targetRatio;
    } else {
      width = wrapWidth;
      if (width > targetWidth) { width = targetWidth; }
      height = width / targetRatio;
    }

    left = (wrapWidth - width) / 2;
    top = (wrapHeight - height) / 2;

    $target.css({
      width: width,
      height: height,
      left: left,
      top: top
    });
  }

  /**
   * クエリストリング（URLパラメータ）をパースして返す
   * @returns {Object} `{name: value, ...}`にパースする
   */
  function getQueryString() {
    var result = {};

    if (1 < document.location.search.length) {
      var query = document.location.search.substring(1);
      var parameters = query.split('&');

      for (var i = 0, len = parameters.length; i < len; i++) {
        var element = parameters[i].split('=');
        var paramName = decodeURIComponent(element[0]);
        var paramValue = decodeURIComponent(element[1]);
        result[paramName] = decodeURIComponent(paramValue);
      }
    }
    return result;
  }

  /**
   * alignTextWidth
   * @param {jQuery} target - Target in the element a selector.
   * @param {jQuery} text - Text in the element a selector.
   * @private
   */
  function alignTextWidth(target, text) {
    $(target).each(function() {
      var $self = $(this);
      var maxWidth = 0;
      var currentWidth = 0;
      var ary;

      $self.css({width: 500}); // テキストの横幅取得の為に一時的に設定
      ary = $self.find(text).get();
      for (var i = 0, len = ary.length; i < len; i++) {
        currentWidth = ary[i].offsetWidth;
        maxWidth = currentWidth > maxWidth ? currentWidth : maxWidth;
      }

      // 選択式のツールチップか判定
      if ($self.hasClass('sw-tooltip-dropdown') === true) {
        // チェックアイコン分の余白を設ける
        $self.css({width: maxWidth + 60});
      } else {
        $self.css({width: maxWidth + 30});
      }
    });
  }

  /**
   * replace image path to absolute path
   * @param {jQuery} target - Target in the element a selector.
   * @private
   */
  function replaceAbsolutePathOfImage(target) {
    $(target).each(function() {
      var $self = $(this);
      var src = $self.attr('src');
      $self.attr('src', '//' + APP.config.host + src);
    });
  }

  /**
   * replace anchor path to absolute path
   * @param {jQuery} target - Target in the element a selector.
   * @private
   */
  function replaceAbsolutePathOfAnchor(target) {
    $(target).each(function() {
      var $self = $(this);
      var href = $self.attr('href');
      if (href === undefined) { return; }
      if (href.match(/^#/) !== null) { return; }
      if (href.match(/^http/) !== null) { return; }
      $self.attr('href', APP.config.protocolPath + href);
    });
  }

  APP.util.inherits = inherits;
  APP.util.bind = bind;
  APP.util.fixedParentElement = fixedParentElement;
  APP.util.putInParentElement = putInParentElement;
  APP.util.putInParentElementMaxSize = putInParentElementMaxSize;
  APP.util.getQueryString = getQueryString;
  APP.util.alignTextWidth = alignTextWidth;
  APP.util.replaceAbsolutePathOfImage = replaceAbsolutePathOfImage;
  APP.util.replaceAbsolutePathOfAnchor = replaceAbsolutePathOfAnchor;

}(window, jQuery, _, APP));
/**
 * @module media - trigger the event at a breakpoint
 * media.on('PC | SP', callback);
 */
(function(window, $, _, APP) {
  var $win = $(window);
  var ee = new EventEmitter();
  var breakpoint = APP.config.breakpoint + 1;
  /**
   * prevType - previous media type（PC | SP）
   * @type {null|string}
   */
  ee.prevType = null;
  /**
   * currentType - current media type（PC | SP）
   * @type {null|string}
   */
  ee.currentType = null;

  function setMediaType(type) {
    if (ee.currentType !== type) {
      ee.prevType = ee.currentType;
      ee.currentType = type;
      ee.emit(type);
    }
  }

  function checkMediaType() {
    var windowWidth = window.innerWidth;

    if (windowWidth >= breakpoint) {
      setMediaType('PC');
    } else {
      setMediaType('SP');
    }
  }

  // $win.one('load.MediaQueries', checkMediaType);
  checkMediaType();
  $win.on('resize.MediaQueries', _.debounce(checkMediaType, 100));

  APP.module.media = ee;
}(window, jQuery, _, APP));
/**
 * @module unit - Elements divide by unit and add class name, align the heights.
 */
(function(window, $, _, APP) {
  /**
   * init
   * @param {jQuery} $elm
   * @param {number} unit - Number to divide
   * @param {Object} options
   * @param {string} options.selector - Target in the element a selector.
   * @param {boolean} options.alignHeight - Align the heights.
   */
  function init($elm, unit, options) {
    var $target = $($elm);
    var length = $target.length;
    var rightNum = unit - 1;
    var lastNum = length - 1;
    var bottomNum = (Math.ceil(length / unit) - 1) * unit - 1;
    unit = unit || length;
    options = options || {};

    if (options.selector !== undefined) {
      $target = $target.find(options.selector);
    }

    $target.each(function(i) {
      var className = ['is-unit'];

      if (i === 0) { className.push('is-unit-first') }
      if (i === lastNum) { className.push('is-unit-last') }
      if (i < unit) { className.push('is-unit-top') }
      if (i % unit === 0) { className.push('is-unit-left') }
      if (i % unit === rightNum) { className.push('is-unit-right') }
      if (i > bottomNum) { className.push('is-unit-bottom') }

      $(this).addClass(className.join(' '));
    });

    if (options.alignHeight === true) {
      alignHeight($target, unit);
    }
  }

  /**
   * destroy
   * @param {jQuery} $elm
   * @param {Object} options
   * @param {string} options.selector
   * @param {boolean} options.alignHeight
   */
  function destroy($elm, options) {
    var $target = $($elm);
    options = options || {};

    if (options.selector !== undefined) {
      $target = $target.find(options.selector);
    }

    $target.removeClass('is-unit is-unit-first is-unit-last is-unit-top is-unit-bottom is-unit-left is-unit-right');

    if (options.alignHeight === true) {
      $target.height('');
    }
  }

  /**
   * alignHeight
   * @param {jQuery} $target
   * @param {number} unit
   * @private
   */
  function alignHeight($target, unit) {
    unit = unit || $target.length;
    var row = Math.ceil($target.length / unit);
    var l, i, j, maxHeight, targetHeight, $elms, $elm;

    for (i = 0; i < row; i++) {
      $elms = $target.slice((i * unit), (i * unit + unit));
      l = $elms.length;
      j = 0;
      maxHeight = 0;

      for (; j < l; j++) {
        $elm = $elms.eq(j);
        targetHeight = $elm.height();
        maxHeight = maxHeight < targetHeight ? targetHeight : maxHeight;
      }
      $elms.height(maxHeight);
    }
  }

  /**
   * toggleMenu
   * @param {Object} self - Target in the element a selector.
   * @param {Object} obj - {parent: 'selector', body: 'selector'}
   * @private
   */
  function toggleMenu(self, obj) {
    var $target = $(self);
    var $parent = $target.closest(obj.parent);
    var $body = $parent.find(obj.body);
    if ($target.hasClass('is-open') === true) {
      $target.removeClass('is-open');
      $body.removeClass('is-open');
    } else {
      $target.addClass('is-open');
      $body.addClass('is-open');
    }
  };

  APP.module.unit = {
    init: init,
    destroy: destroy,
    alignHeight: alignHeight,
    toggleMenu: toggleMenu
  };
}(window, jQuery, _, APP));
/**
 * @module share
 */
(function(window, $, _, APP) {

  /**
   * windowを開く
   * @param {string} url windowのURL
   * @param {number} width windowの幅
   * @param {number} height windowの高さ
   * @private
   */
  function openWindow(url, width, height) {
    var w = width || 600;
    var h = height || 400;
    var l = (window.screen.width / 2) - (w / 2);
    var t =  (window.screen.height / 2) - (h / 2);
    window.open(url, 'sharewindow', 'scrollbars=yes, width=' + w + ', height=' + h + ', left=' + l + ', top=' + t);
  }

  /**
   * Twitterでシェアする
   * @param {string} url シェアURL
   * @param {string} text シェアテキスト
   * @param {string} hash  シェアのハッシュ
   */
  function twitter(url, text, hash) {
    var shareText = encodeURIComponent(text);
    var shareHash = encodeURIComponent(hash);
    var shareUrl = 'https://twitter.com/share?url=' + url + '&text=' + shareText;
    if (hash !== undefined) { shareUrl += '&hashtags=' + shareHash }
    openWindow(shareUrl, 500, 355);
  }

  /**
   * Facebookでシェアする
   * @param {string} url シェアURL
   */
  function facebook(url) {
    var shareUrl = 'https://www.facebook.com/sharer.php?u=' + url;
    openWindow(shareUrl, 560, 715);
  }

  /**
   * Google
   * @param {string} url シェアURL
   */
  function google(url) {
    var shareUrl = 'https://plus.google.com/share?url=' + url;
    openWindow(shareUrl, 600, 600);
  }

  APP.module.share = {
    twitter: twitter,
    facebook: facebook,
    google: google
  };
}(window, jQuery, _, APP));
/**
 * YouTube IFrame Player API のラップ
 * @extends EventEmitter
 */
(function(window, $, _, APP) {
  var util = APP.util;

  var isLoaded = false;
  var isLoading = false;
  var callbackListAfterLoad = [];

  /**
   * YouTube IFrameを埋め込む
   * @param {DOMElement|string} elm YouTube IFrameに置き換わるDOM要素 or 要素のid
   * @param {string} videoID YouTubeの動画ID
   * @param {number} width 動画の幅
   * @param {number} height 動画の高さ
   * @param {Object} options プレーヤーのカスタマイズに使うパラメータ（https://developers.google.com/youtube/player_parameters?playerVersion=HTML5&hl=ja）
   * @extends EventEmitter
   */
  function YTPlayer(elm, videoID, width, height, options) {
    var self = this;
    width  = width  || '640';
    height = height || '390';
    options = options || {};

    YTPlayer.loadAPI(function() {
      self.player = new YT.Player(elm, {
        width: width,
        height: height,
        videoId: videoID,
        playerVars: options,
        events: {
          'onReady': self._onPlayerReady.bind(self),
          'onStateChange': self._onPlayerStateChange.bind(self)
        }
      });
    });
  }
  util.inherits(YTPlayer, EventEmitter);

  /**
   * YouTube APIのscriptファイルを読み込む
   * @param {Function} callback scriptファイル読み込み完了時のcallback
   * @static
   */
  YTPlayer.loadAPI = function(callback) {
    if (isLoaded === true) {
      if (typeof callback === 'function') { callback(); }
      return;
    }

    if (typeof callback === 'function') { callbackListAfterLoad.push(callback); }
    if (isLoading === true) { return; }
    isLoading = true;

    // load youtube script
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    window.onYouTubeIframeAPIReady = function() {
      isLoaded = true;
      var i = 0, len = callbackListAfterLoad.length;
      for (; i < len; i++) {
        var callback = callbackListAfterLoad.shift();
        callback();
      }
    }
  };

  /**
   * YouTube APIのイベントリスナー
   * @callback
   * @private
   */
  YTPlayer.prototype._onPlayerReady = function() {
    this.emit('ready');
  };

  /**
   * YouTube APIのイベントリスナー
   * @param {Object} state
   * @param {number} state.data {-1: 未開始, 0: 終了, 1: 再生中, 2: 一時停止, 3: バッファリング中, 5: 頭出し済み}
   * @callback
   * @private
   */
  YTPlayer.prototype._onPlayerStateChange = function(state) {
    switch (state.data) {
      case 0:
        this.emit('end');
        break;
      case 1:
        this.emit('play');
        break;
      case 2:
        this.emit('pause');
        break;
    }
  };

  /**
   * 動画を再生
   */
  YTPlayer.prototype.play = function() {
    this.player.playVideo();
  };

  /**
   * 動画を停止
   */
  YTPlayer.prototype.pause = function() {
    this.player.pauseVideo();
  };

  /**
   * ミュートにする
   */
  YTPlayer.prototype.mute = function() {
    this.player.mute();
  };

  /**
   * ミュートを解除する
   */
  YTPlayer.prototype.unMute = function() {
    this.player.unMute();
  };

  /**
   * 動画を指定秒数に進める
   * @param {number} seconds 秒数
   */
  YTPlayer.prototype.seekTo = function(seconds) {
    this.player.seekTo(seconds);
  };

  /**
   * 指定したIDのYouTube動画に切り替える
   * @param {string} id ouTubeの動画ID
   */
  YTPlayer.prototype.loadVideoById = function(id) {
    this.player.loadVideoById(id);
  };

  /**
   * YouTube IFrame Playerを破棄する
   */
  YTPlayer.prototype.destroy = function() {
    this.removeAllListeners();
    this.player.destroy();
  };


  APP.class.YTPlayer = YTPlayer;

})(window, jQuery, _, APP);
(function(window, $, _, APP) {
  // gallery modal
  var util = APP.util;
  var media = APP.module.media;
  var YTPlayer = APP.class.YTPlayer;

  var ytPlayer = null;
  var ytOption =  { rel: 0, showinfo : 0, iv_load_policy: 3 };

  var isInit = false;
  var isOpen = false;
  var isInitSlider = false;
  var isThumbOpen = false;
  var isThumbMotion = false;
  var isDisable = true;
  var thumbWidth = 160;
  var thumbHeight = 90;
  var slideLength = 0;
  var slideInfo = [];
  var currentIndex = 0;
  var currentType = '';

  var $win = $(window);
  var $modal;
  var $viewPort;
  var $main;
  var $close;
  var $cover;
  var $thumb;
  var $list;
  var $image;
  var $youtube = null;

  var scrollEvent = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
  var criteriaY =  70;
  var criteriaX = 60;
  var criteriaT = 2000;
  var startX;
  var startY;
  var endX;
  var endY;
  var startTime;

  // add modal elements
  function init() {
    isInit = true;
    var html =
      '<div class="st-gModal">'
      + '<div class="st-gModal_close"></div>'
      + '<div class="st-gModal_cover"></div>'
      + '<div class="st-gModal_main">'
      + '<div class="st-gModal_viewport">'
      + '<div class="st-gModal_media st-gModal_media-movie"></div>'
      + '<img class="st-gModal_media st-gModal_media-image">'
      + '</div>'
      + '<div class="st-gModal_note"></div>'
      + '<div class="st-gModal_prev"></div>'
      + '<div class="st-gModal_next"></div>'
      + '<div class="st-gModal_invite"></div>'
      + '<div class="st-gModal_pager">'
      + '<span class="st-gModal_pagerCurrent"></span>'
      + '<span class="st-gModal_pagerMax"></span>'
      + '</div>'
      + '</div>'
      + '<div class="st-gModal_thumb">'
      + '<div class="st-gModal_thumbList"></div>'
      + '<div class="st-gModal_thumbPrev"></div>'
      + '<div class="st-gModal_thumbNext"></div>'
      + '</div>'
      + '</div>';

    $('body').append(html);
    $modal = $('.st-gModal');
    $main = $modal.find('.st-gModal_main');
    $viewPort = $modal.find('.st-gModal_viewport');
    $note = $modal.find('.st-gModal_note');
    $close = $modal.find('.st-gModal_close');
    $cover = $modal.find('.st-gModal_cover');
    $thumb = $modal.find('.st-gModal_thumb');
    $list = $modal.find('.st-gModal_thumbList');
  }

  function open(info, index) {
    if (isInit === false) { init(); }
    if (isOpen === true) { return; }
    isOpen = true;
    currentType = '';
    info = info || {};

    var $dfd = $.Deferred();
    var promise;

    isDisable = true;
    $('html').addClass('st-gModal_lock');
    $main.find('.st-gModal_media').hide();
    $cover.css({display: 'block', opacity: 1});
    $modal.css({display: 'block', opacity: 0});

    if ($.isArray(info) === true) {
      initActionBtn();
      promise = initList(info, index);
    } else {
      slideLength = 0;
      exitActionBtn();
      if (info.type === 'movie') {
        promise = initMovie(info);
      } else if (info.type === 'image') {
        promise = initImage(info);
      } else {
        close();
        return;
      }
    }

    TweenMax.to($modal, 0.4, {opacity: 1, onComplete: $dfd.resolve});
    $.when(promise).then(function() {
      onResize();
      if (slideLength > 0) {
        $win.on(scrollEvent, onScroll);
        $main.on('touchstart mousedown', onStart);
      }
      $win.on('resize', onResize);
      $close.on('click', close);
      unCover(0.5).then(function() { isDisable = false; });
    }, function() {
      close();
    });
  }

  function close() {
    if (isOpen === false) { return; }
    isDisable = true;
    $('html').removeClass('st-gModal_lock');
    $win.off('resize', onResize);
    TweenMax.to($modal, 0.4, {opacity: 0, onComplete: function() {
      if (currentType === 'movie') {
        exitMovie();
      } else if (currentType === 'image') {
        exitImage();
      }
      destroyThumbSlider();
      exitActionBtn();
      closeThumb();
      $main.off('touchstart mousedown', onStart);
      $win.off('resize', onResize);
      $win.off(scrollEvent, onScroll);
      $close.off('click', close);
      $modal.css({display: 'none'});
      slideLength = 0;
      isOpen = false;
    }});
  }

  function initList(info, index) {
    index = index === undefined ? 0 : index;
    currentIndex = index;
    slideInfo = info;
    slideLength = info.length;
    var i = 0;
    var len = slideLength;
    var html = [];

    for (; i < len; i++) {
      var addClass = i === index ? 'is-active' : '';
      var itemHTML = '<div class="st-gModal_thumbItem ' + addClass + '" data-index="' + i + '" data-type="' + info[i].type + '">'
        + '<div class="st-gModal_thumbCover"></div>'
        + '<img src="' + info[i].thumbPath + '">'
        + '</div>';
      html.push(itemHTML);
    }

    $list.html(html.join(''));
    setPager();
    initThumbSlider();
    if (info[index].type === 'movie') {
      return initMovie(info[index]);
    } else if (info[index].type === 'image') {
      return initImage(info[index]);
    } else {
      var $dfd = $.Deferred();
      $dfd.reject();
      return $dfd.promise();
    }
  }

  function initMovie(info) {
    currentType = 'movie';
    var $dfd = $.Deferred();
    var $movie = $main.find('.st-gModal_media-movie');
    $movie.show();
    YTPlayer.loadAPI(function() {
      ytPlayer = new YTPlayer($movie[0], info.id, 960, 540, ytOption);
      ytPlayer.on('ready', function() {
        if (typeof info.note === 'undefined') { info.note = '' }
        $note.html(info.note);
        setTimeout(function() { $dfd.resolve(); }, 500);
        $youtube = $main.find('.st-gModal_media-movie');
        setTimeout(function() { initNote($youtube,$note); }, 600);
      });
    });
    return $dfd.promise();
  }

  function exitMovie() {
    ytPlayer.destroy();
    $main.find('.st-gModal_media-movie').hide();
  }

  function initImage(info) {
    currentType = 'image';
    var $dfd = $.Deferred();
    var $img = $main.find('.st-gModal_media-image');
    $img.attr('src', info.imagePath);
    $img.show();
    $note.hide();
    $img.imagesLoaded().done(function() {
      var image = new Image();
      image.onload = function() {
        $img.attr('width', image.width);
        $img.attr('height', image.height);
        setTimeout(function() {
          $image = $img;
          $dfd.resolve();
        }, 500);
      };
      image.src = $img.attr('src');
    }).fail(function() {
      $dfd.reject();
    });
    return $dfd.promise();
  }

  function exitImage() {
    $main.find('.st-gModal_media-image').hide();
  }

  function exitActionBtn() {
    $modal.find('.st-gModal_prev').hide().off('click');
    $modal.find('.st-gModal_next').hide().off('click');
    $modal.find('.st-gModal_invite').hide();
    $modal.find('.st-gModal_pager').hide();
  }

  function initNote($movie,$note) {
    setPositionNote($movie,$note);

    var timer = false;
    $win.resize(function() {
        $note.hide();
      if (timer !== false) {
        clearTimeout(timer);
      }
      timer = setTimeout(function() {
        var $youtube = $main.find('.st-gModal_media-movie');
        setPositionNote($youtube,$note);
      }, 100);
    });

    function setPositionNote($movie,$note) {
        var w = $win.width();
        var h = $win.height();
      var mw = $movie.width();
      var mh = Number($movie.css('height').split('p')[0]);
      var nw = $note.width();
      var l = (mw < w) ? (w / 2) - (mw / 2) + mw - nw : 'auto';
      var r = (mw >= w) ? '10px' : 'auto';
      var b = mh + (h / 2 - mh / 2);
      $note.css({
        'left': l,
        'right': r,
        'top' : b + 15 + 'px',
        'z-index': '10'
      });
      $note.fadeIn();
    }
  }

  function initActionBtn() {
    $modal.find('.st-gModal_prev').show().on('click', function (e) {
      e.stopPropagation();
      prev();
    });
    $modal.find('.st-gModal_next').show().on('click', function (e) {
      e.stopPropagation();
      next();
    });
    $modal.find('.st-gModal_invite').show();
    $modal.find('.st-gModal_pager').show();
  }

  function next() {
    if (isDisable === true) { return; }
    if (currentIndex === slideLength - 1) {
      goTo(0);
    } else {
      goTo(currentIndex + 1);
    }
  }

  function prev() {
    if (isDisable === true) { return; }
    if (currentIndex === 0) {
      goTo(slideLength - 1);
    } else {
      goTo(currentIndex - 1);
    }
  }

  function goTo(index) {
    if (isDisable === true) { return; }
    isDisable = true;
    cover(0.3).then(function() {
      var promise;
      var info = slideInfo[index];
      if (currentType === 'movie') {
        exitMovie();
      } else if (currentType === 'image') {
        exitImage();
      }

      if (info.type === 'movie') {
        promise = initMovie(info);
      } else if (info.type === 'image') {
        promise = initImage(info);
      } else {
        close();
        return;
      }
      currentIndex = index;
      setPager();

      $.when(promise).then(function() {
        onResize();
        $thumb.find('.st-gModal_thumbItem')
          .removeClass('is-active')
          .filter('[data-index="' + index + '"]')
          .addClass('is-active');
        unCover(0.3).then(function() { isDisable = false; });
      }, function() {
        close();
      });
    });
  }

  function setPager() {
    $modal.find('.st-gModal_pagerCurrent').text(currentIndex + 1);
    $modal.find('.st-gModal_pagerMax').text(slideLength);
  }

  function initThumbSlider() {
    isInitSlider = true;

    $list.not('.slick-initialized').slick({
      arrows: false,
      infinite: false,
      draggable: false,
      dots: false,
      variableWidth: true
    });
    checkPager();

    $thumb.on('click', '.st-gModal_thumbItem', function() {
      var index = parseInt($(this).attr('data-index'));
      goTo(index);
    });
    $thumb.find('.st-gModal_thumbNext').addClass('is-show').on('click', nextThumb);
    $thumb.find('.st-gModal_thumbPrev').addClass('is-show').on('click', prevThumb);
    $main.find('.st-gModal_invite').on('mouseenter', openThumb);
  }

  function prevThumb() {
    var windowWidth = window.innerWidth;
    var showThumbNum = Math.floor(windowWidth / thumbWidth);
    var currentSlide = $list.slick('slickCurrentSlide');
    var goToNum = currentSlide - showThumbNum;
    if (goToNum < 0) { goToNum = 0; }
    $list.slick('slickGoTo', goToNum);
    checkPager();
  }

  function nextThumb() {
    var windowWidth = window.innerWidth;
    var showThumbNum = Math.floor(windowWidth / thumbWidth);
    var currentSlide = $list.slick('slickCurrentSlide');
    var goToNum = currentSlide + showThumbNum;
    var maxNum = slideLength - showThumbNum;
    if (goToNum >  maxNum) { goToNum = maxNum; }
    $list.slick('slickGoTo', goToNum);
    checkPager();
  }

  function checkPager() {
    if (slideLength === 0) { return; }
    var windowWidth = window.innerWidth;
    var showThumbNum = Math.floor(windowWidth / thumbWidth);
    var currentSlide = $list.not('.slick-initialized').slick('slickCurrentSlide');
    if (currentSlide === 0) {
      $thumb.find('.st-gModal_thumbPrev').addClass('is-hide');
    } else {
      $thumb.find('.st-gModal_thumbPrev').removeClass('is-hide');
    }
    if (currentSlide + 1 >= slideLength - showThumbNum + 1) {
      $thumb.find('.st-gModal_thumbNext').addClass('is-hide');
    } else {
      $thumb.find('.st-gModal_thumbNext').removeClass('is-hide');
    }
  }

  function destroyThumbSlider() {
    if (isInitSlider === true) {
      $list.not('.slick-initialized').slick('unslick');
      isInitSlider = false;
    }
    $thumb.find('.st-gModal_thumbNext').off('click', nextThumb);
    $thumb.find('.st-gModal_thumbPrev').off('click', prevThumb);
    $main.find('.st-gModal_invite').off('mouseenter', openThumb);
  }

  function openThumb() {
    if (isThumbOpen === true || isThumbMotion === true) { return; }
    isThumbOpen = true;
    isThumbMotion = true;
    $modal.find('.st-gModal_invite').addClass('is-open');
    $thumb.on('mouseenter', function() { $thumb.find('.st-gModal_thumbPrev, .st-gModal_thumbNext').addClass('is-show'); })
      .on('mouseleave', function() {
        $thumb.find('.st-gModal_thumbPrev, .st-gModal_thumbNext').removeClass('is-show');
        closeThumb();
      });
    TweenMax.to([$main, $cover], 0.4, {top: thumbHeight * -1, ease: Power2.easeInOut, onComplete: function() {
      isThumbMotion = false;
    }});
  }

  function closeThumb() {
    if (isThumbOpen === false || isThumbMotion === true) { return; }
    isThumbMotion = true;
    $modal.find('.st-gModal_invite').removeClass('is-open');
    $thumb.off('mouseenter').off('mouseleave');
    TweenMax.to([$main, $cover], 0.4, {top: 0, ease: Power2.easeInOut, onComplete: function() {
      isThumbOpen = false;
      isThumbMotion = false;
    }});
  }

  function cover(time) {
    time = time || 0;
    var $dfd = $.Deferred();
    $cover.css({display: 'block', opacity: 0});
    TweenMax.to($cover, time, {opacity: 1, onComplete: function() {
      $dfd.resolve();
    }});
    return $dfd.promise();
  }

  function unCover(time) {
    time = time || 0;
    var $dfd = $.Deferred();
    TweenMax.to($cover, time, {opacity: 0, onComplete: function() {
      $cover.css({display: 'none'});
      $dfd.resolve();
    }});
    return $dfd.promise();
  }

  function onResize() {
    if (media.currentType === 'PC') {
      if (currentType === 'movie') {
        util.putInParentElementMaxSize($viewPort, $youtube);
      } else if (currentType === 'image') {
        util.putInParentElementMaxSize($viewPort, $image);
      }
      checkPager();
    } else if (media.currentType === 'SP') {
      if (currentType === 'movie') {
        util.putInParentElement($viewPort, $youtube);
      } else if (currentType === 'image') {
        util.putInParentElement($viewPort, $image);
      }
    }
  }

  function onStart(e) {
    var orgEvent = e.originalEvent.targetTouches ? e.originalEvent.targetTouches[0] : e.originalEvent;

    startX = orgEvent.pageX;
    startY = orgEvent.pageY;
    endX = orgEvent.pageX;
    endY = orgEvent.pageY;
    startTime = $.now();

    $main.on('touchmove mousemove', onMove);
    $main.on('touchend touchcancel mouseup', onEnd);
  }

  function onMove(e) {
    var orgEvent = e.originalEvent.targetTouches ? e.originalEvent.targetTouches[0] : e.originalEvent;
    endX = orgEvent.pageX;
    endY = orgEvent.pageY;
    event.preventDefault();
  }

  function onEnd() {
    var diffX = endX - startX;
    var diffY = endY - startY;
    var diffTime = $.now() - startTime;

    if (diffTime <= criteriaT && Math.abs(diffY) <= criteriaY) {
      if (diffX <= (-1 * criteriaX)) {
        next();
      } else if (diffX >= criteriaX) {
        prev();
      }
    }

    $main.off('touchmove mousemove', onMove);
    $main.off('touchend touchcancel mouseup', onEnd);
  }

  function onScroll(e) {
    var delta = e.originalEvent.deltaY ? -(e.originalEvent.deltaY) : e.originalEvent.wheelDelta ? e.originalEvent.wheelDelta : -(e.originalEvent.detail);
    if (delta < 0) {
      e.preventDefault();
      openThumb();
    } else if (delta > 0) {
      e.preventDefault();
      closeThumb();
    }
  }

  APP.module.galleryMmodal = {
    init: init,
    open: open,
    close: close
  };

}(window, jQuery, _, APP));
/**
 * Models filter
 */
(function(window, $, _, APP) {
  var unit = APP.module.unit;

  var modelsData = _.cloneDeep(APP.config.modelsData);

  /**
   * Models filter
   * @param {Object} $elm -
   * @param {Object} option - 対象要素、レイアウト数の指定
   * {
   *   list: '.st-megaNav_filterList', // フィルターリスト
   *   contentList: .st-megaNav_filterContent', // 表示コンテンツリスト
   *   layoutNum: number // 一列に表示する数
   *   thumbSize: string('s' | 'm') // 拡大サムネイルのサイズ
   *   cardSize: string('s' | 'm' | 'l') // モデルカードのサイズ
   *   cardTheme: string('white' | 'black') // [optional] テーマカラーに関係なく色指定する際に設定
   * }
   */
  function ModelsFilter($elm, option) {
    this.$filterList = $elm.find(option.list);
    this.$filterItems = this.$filterList.find('li');
    this.$contentList = $elm.find(option.contentList);
    this.contentAry = [];
    this.models = 'all'; // 既存のfilter対象のモデル
    this.sortName = 'type'; // 既存のfilter対象のスペック
    this.layoutNum = option.layoutNum; // 一列に並べる個数
    this.thumbSize = option.thumbSize; // 拡大サムネイルのサイズ
    this.cardSize = option.cardSize; // モデルカードのサイズ
    this.cardTheme = option.cardTheme || null; // カードカラー
    this.splitNum = null; // 一列の左右分割数

    this._onClickFilter = this._onClickFilter.bind(this);
    this.$filterList.on('click.filter', 'li', this._onClickFilter);
    this.init();
  }

  ModelsFilter.prototype.init = function(filterInfo) {
    this.filter(filterInfo);
  };

  /**
   * サムネイルの表示位置を指定する配列を返す
   * @param {String} counter - 表示項目数
   * @return {Array} posAry - 表示位置情報の配列
   */
  ModelsFilter.prototype._getPositionArray = function(counter) {
    var posAry = [];
    var posLeftFlag = true;
    var posCounter = 0;
    var bottomIndex = counter; // 下位置の指定開始インデックス

    if (counter > this.layoutNum) {
      var mod = counter % this.layoutNum;
      bottomIndex = mod === 0 ? counter - this.layoutNum : counter - mod;
      bottomIndex = bottomIndex - 1;
    }

    for (var j = 0; j < counter; j++) {
      if (posCounter === this.splitNum) {
        posLeftFlag = posLeftFlag === false ? true : false;
        posCounter = 0;
      }

      if (posLeftFlag === true) {
        posAry[j] = j <= bottomIndex ? 'topleft' : 'bottomleft';
      } else {
        posAry[j] = j <= bottomIndex ? 'topright' : 'bottomright';
      }
      posCounter++;
    }

    return posAry;
  };

  /**
   *  listを生成しDOMに挿入
   * @param {Array} ary -
   * @param {Array} posAry - 表示位置情報の配列
   */
  ModelsFilter.prototype._createList = function(ary, posAry) {
    var isHTTPS = location.protocol === 'https:';
    var isCrossDomain = location.host !== APP.config.host;
    var html = [];
    var cardSize = this.cardSize;
    var thumbDirectory = this.thumbSize;
    var cardTheme = (this.cardTheme !== null) ? ' sw-models_card-' + this.cardTheme : '';
    var specHTML = '';
    var specClass = '';

    // HYBRIDかF SPORTならテキストを挿入
    switch (this.models) {
      case 'hybrid':
        specHTML = '<strong class="sw-models_cardSpec">' + 'HYBRID' + '</strong>';
        specClass = 'is-spec';
        thumbDirectory += '/hybrid';
        break;
      case 'fsport':
        specHTML = '<strong class="sw-models_cardSpec">' + 'F SPORT' + '</strong>';
        specClass = 'is-spec';
        thumbDirectory += '/fsport';
        break;
      default :
        thumbDirectory += '/main';
        break;
    }

    for (var i = 0, len = ary.length; i < len; i++) {
      var innerHTML;
      innerHTML = '<li class="sw-models_card' + cardTheme + ' sw-models_card-' + cardSize + ' sw-modelsGroup_item ' + specClass + '">'
        + '<a href="/models/' + ary[i]['id'] + '/" class="sw-models_cardLink">'
        + '<div class="sw-models_cardWrap">'
        + '<img class="sw-models_cardImage" src="/common/v3/images/models/s/' + ary[i]['id'] + '.png" alt="">'
        + '<div class="sw-models_cardDetail u-clearfix">'
        + '<p class="sw-models_cardTitle">' + ary[i]['name'] + '</p>'
        + '<p class="sw-models_cardText">';

      if (ary[i]['typeF'] !== true) {
        innerHTML += specHTML;
      }

      innerHTML +=  ary[i]['grade'][this.models] + '</p></div>';

      switch (this.sortName) {
        case 'size':
          innerHTML += '<p class="sw-models_sortInfo sw-models_sortInfo-size">' + ary[i][this.sortName][this.models] + '</p>';
          break;
        case 'price':
        case 'fuel':
          innerHTML += '<p class="sw-models_sortInfo">' + ary[i][this.sortName][this.models] + '</p>';
          break;
        case 'type':
          innerHTML += '';
          break;
      }

      innerHTML += '</div></a>'
      + '<figure class="sw-models_cardThumb' + ' is-' + posAry[i] + '"><img src="/common/v3/images/models/thumb/' + thumbDirectory + '/' + ary[i]['id'] + '.jpg" alt=""></figure>'
      + '</li>';

      html[i] = innerHTML;
    }

    var $tmp = $(html.join(''));
    if (isCrossDomain === true) {
      APP.util.replaceAbsolutePathOfImage($tmp.find('img'));
    }

    if (isHTTPS === true || isCrossDomain === true) {
      APP.util.replaceAbsolutePathOfAnchor($tmp.find('a'));
    }

    this.$contentList.html($tmp);

    unit.init(this.$contentList.find('.' + 'sw-models_cardDetail'), this.layoutNum, {alignHeight: true});
  };

  /**
   * @param {String} sortName - sortName
   * @param {String} models - models
   */
  ModelsFilter.prototype._sort = function(sortName, models) {
    sortName = sortName + 'Index';
    return _.sortBy(this.contentAry, [function(o) { return o[sortName][models]; }]);
  };


  /**
   * @param {Number} time - delay
   */
  ModelsFilter.prototype._hide = function(time) {
    var $dfd = $.Deferred();
    TweenMax.to(this.$contentList, time, {
      visibility: 'hidden',
      opacity: 0,
      ease: Power2.easeIn,
      onComplete: function() {
        $dfd.resolve();
      }
    });
    return $dfd.promise();
  };

  /**
   * @param {Number} time - delay
   */
  ModelsFilter.prototype._show = function(time) {
    var $dfd = $.Deferred();
    TweenMax.to(this.$contentList, time, {
      visibility: 'visible',
      opacity: 1,
      ease: Power2.easeIn,
      onComplete: function() {
        $dfd.resolve();
      }
    });
    return $dfd.promise();
  };

  /**
   * @param {Object} filterInfo -
   */
  ModelsFilter.prototype.filter = function(filterInfo) {
    filterInfo = filterInfo || {};
    var self = this;
    this.contentAry = [];
    this.sortName = ('sortName' in filterInfo === true) ? filterInfo.sortName : this.sortName;
    this.models = ('models' in filterInfo === true) ? filterInfo.models : this.models;
    this.layoutNum = ('layoutNum' in filterInfo === true) ? filterInfo.layoutNum : this.layoutNum;
    this.cardSize = ('cardSize' in filterInfo === true) ? filterInfo.cardSize : this.cardSize;
    this.splitNum = this.layoutNum / 2;

    this._hide(0.3)
      .then(function() {
        _.filter(modelsData, function(o) {
          var t = _.indexOf(o.type, self.models);
          if (t >= 0) {
            self.contentAry.push(o);
          }
        });
        var sortAry = self._sort(self.sortName, self.models);
        var posAry = self._getPositionArray(sortAry.length);
        self._createList(sortAry, posAry);
        self._show(0.3);
      });
  };

  ModelsFilter.prototype._onClickFilter = function(e) {
    var $elm = $(e.currentTarget);

    this.$filterItems.removeClass('is-selected');
    $elm.addClass('is-selected');

    // 既にfilterがかかってるかチェック
    if(this.models === $elm.attr('data-type')) { return; }
    this.filter({models: $elm.attr('data-type')});
  };

  APP.class.ModelsFilter = ModelsFilter;

})(window, jQuery, _, APP);
/**
 * Mega menu
 */
(function(window, $, _, APP) {
  var mq = APP.module.media;
  var unit = APP.module.unit;
  var ModelsFilter = APP.class.ModelsFilter;

  function MegaMenu() {
    this.$win = $(window);
    this.$menu = $('.st-header_megaNav');
    this.$triger = $('.js-megaNav');
    this.$content = $('.st-megaNav_Item');

    this.currentIndex = null;
    this.navFlag = false;
    this.menuFlag = false;
    this.navTimer = null;
    this.menuTimer = null;
    this.openTimer = null;
    this.navDelay = 100;
    this.menuDelay = 100;
    this.modelsFilter = undefined;

    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.onNav = this.onNav.bind(this);
    this.offNav = this.offNav.bind(this);
    this.onMenu = this.onMenu.bind(this);
    this.offMenu = this.offMenu.bind(this);
    this.init4PC = this.init4PC.bind(this);
    this.init4SP = this.init4SP.bind(this);
    this.initMagazine4PC = this.initMagazine4PC.bind(this);
    this.initMagazine4SP = this.initMagazine4SP.bind(this);
    this.initBrand4PC = this.initBrand4PC.bind(this);
    this.initBrand4SP = this.initBrand4SP.bind(this);
    this.initModels4PC = this.initModels4PC.bind(this);
    this.initModels4SP = this.initModels4SP.bind(this);
    this.onResizeModelsOfSP = this.onResizeModelsOfSP.bind(this);

    if (mq.currentType === 'PC') { this.init4PC(); }
    if (mq.currentType === 'SP') { this.init4SP(); }
    mq.on('PC', this.init4PC);
    mq.on('SP', this.init4SP);

    this.initModels();
    this.initMagazine();
    this.initBrand();
  }

  MegaMenu.prototype.init4PC = function() {
    this.$triger.on('mouseenter', this.onNav);
    this.$triger.on('mouseleave', this.offNav);
    this.$menu.on('mouseenter', this.onMenu);
    this.$menu.on('mouseleave', this.offMenu);

    this.$content.css({opacity: 0, height: 0});
    $('.js-iScroll').each(function() {
      var $target = $(this);
      if ($target.data('iscroll') !== undefined) {
        $target.data('iscroll').destroy();
        $target.find('.js-iScroll_inner').width('');
      }
    });
  };

  MegaMenu.prototype.init4SP = function() {
    clearTimeout(this.openTimer);
    this.$triger.off('mouseenter');
    this.$triger.off('mouseleave');
    this.$menu.off('mouseenter');
    this.$menu.off('mouseleave');

    this.$content.css({height: '', opacity: 1});
    $('.js-iScroll').each(function() {
      var $target = $(this);
      var w = 0;
      $target.find('li').each(function() {
        w += $(this).outerWidth(true);
      });
      $target.find('.js-iScroll_inner').width(w + 32); // 左右のpaddingを加算
      $target.data('iscroll', new IScroll($target[0], {click: true, scrollX: true, scrollY: false }));
    });
  };

  MegaMenu.prototype.open = function(index) {
    // 同じメニューならなにもしない
    if (index === this.currentIndex) { return; }
    var self = this;
    var time = 0;
    var prevIndex = this.currentIndex;

    this.$triger
      .removeClass('is-active')
      .eq(index)
      .addClass('is-active');

    if (this.currentIndex !== null) {
      time = 0.2;
      TweenMax.to(this.$content.eq(this.currentIndex), time, {
        opacity: 0,
        ease : Power2.easeOut,
        onComplete: function() {
          self.$content.eq(prevIndex).css({height: 0});
          self.$content.eq(index).css({height: ''});
        }
      });
    } else {
      self.$content.css({height: 0, opacity: 0});
      self.$content.eq(index).css({height: ''});
    }

    this.currentIndex = index;
    TweenMax.to(this.$content.eq(index), 0.3, {
      opacity: 1,
      delay: time,
      ease : Power2.easeIn
    });

    if ($('body').hasClass('no-alphHeader') === true) {
      $('.st-header_inner').addClass('is-alph');
    }

    TweenMax.to(this.$menu, 0.4, {
      visibility: 'visible',
      opacity: 1,
      ease : Power0.easeNone
    });
  };

  MegaMenu.prototype.close = function() {
    clearTimeout(this.menuTimer);
    var self = this;

    this.currentIndex = null;
    this.$triger.removeClass('is-active');
    this.$content.css({ opacity: 0});

    if ($('body').hasClass('no-alphHeader') === true) {
      $('.st-header_inner').removeClass('is-alph');
    }

    TweenMax.to(this.$menu, 0.3, {
      visibility: 'hidden',
      opacity: 0,
      ease : Power4.easeOut,
      onComplete: function() {
        self.$content.css({ height: 0});
      }
     });

  };

  MegaMenu.prototype.onNav = function(e) {
    clearTimeout(this.openTimer);
    this.navFlag = true;

    var $target = $(e.currentTarget);
    var index = parseInt($target.attr('data-nav-index'), 10);
    var self = this;
    clearTimeout(this.navTimer);
    this.openTimer = setTimeout(function() {
      self.open(index);
    }, 500);

  };

  MegaMenu.prototype.offNav = function(e) {
    clearTimeout(this.openTimer);
    this.navFlag = false;

    var $target = $(e.currentTarget);
    var index = parseInt($target.attr('data-nav-index'), 10);
    var self = this;

    self.navTimer = setTimeout(function() {
      if (self.menuFlag === true) { return; }
      self.close();
    }, self.navDelay);
  };

  MegaMenu.prototype.onMenu = function() {
    this.menuFlag = true;
  };

  MegaMenu.prototype.offMenu = function() {
    var self = this;
    this.menuFlag = false;

    self.menuTimer = setTimeout(function() {
      // navにのっていればなにもしない
      if (self.navFlag === true) { return; }
      self.close();
    }, self.menuDelay);
  };

  MegaMenu.prototype.initModels = function() {
    this.settingModels();
    if (mq.currentType === 'PC') { this.initModels4PC(); }
    if (mq.currentType === 'SP') { this.initModels4SP(); }
    mq.on('PC', this.initModels4PC);
    mq.on('SP', this.initModels4SP);
  };

  MegaMenu.prototype.settingModels = function() {
    var $lgListPC = $('.st-modelsGroup');
    var $lgListCloneSP = $lgListPC.clone().addClass('u-sp');
    $lgListPC.addClass('u-pc');
    $lgListPC.after($lgListCloneSP);
  }

  MegaMenu.prototype.initModels4PC = function() {
    if (this.modelsFilter !== undefined) {
      this.modelsFilter.init({layoutNum: 6});
    } else {
      this.modelsFilter = new ModelsFilter($('.st-megaNav_Item-models'), {
        list: '.st-models_list',
        contentList: '.st-models_content',
        layoutNum: 6,
        thumbSize: 's',
        cardSize: 's'
      });
    }
    this.$win.off('resize.models');

    // category
    var $modelsList = $('.st-modelsGroup.u-pc');
    var $win = $(window);
    var winWidth = $win.outerWidth();
    var offsetXOfTooTIp = -5;
    var offsetYOfTooTIp = 25;

    // detail
    var $lg = $('.st-models_inner');
    var $lgList = $('.st-modelsGroup_item');
    var $lgNext = $('.st-modelsGroup_next');
    var $lgPrev = $('.st-modelsGroup_prev');
    var lgSlideLength = $modelsList.find('.st-modelsGroup_item').length;
    var lgSlidesToLength = 2;
    var lgSlidesToScroll = 1;
    var lgSlidesRow = 1;

    /*$modelsList.slick({
      arrows: false,
      infinite: false,
      slidesToShow: lgSlidesToLength,
      slidesToScroll: lgSlidesToScroll,
      draggable: false,
      slidesPerRow: 1,
      rows: lgSlidesRow,
      dots: false,
      appendArrows: $('.st-modelsGroup_pager')
    });*/
    checkPager4Lg();

    $lgNext.off('click').on('click', function() {
      //$modelsList.slick('slickNext');
      movePosition('right');
      checkPager4Lg();
    });

    $lgPrev.off('click').on('click', function() {
      //$modelsList.slick('slickPrev');
      movePosition('left');
      checkPager4Lg();
    });

    $lg.off('mouseenter').on('mouseenter', function() {
      $lgNext.addClass('is-show');
      $lgPrev.addClass('is-show');
    });

    $lg.off('mouseleave').on('mouseleave', function() {
      $lgNext.removeClass('is-show');
      $lgPrev.removeClass('is-show');
    });

    function checkPager4Lg() {
      var current = $modelsList.hasClass('is-right');
      if (!current) {
        $lgPrev.addClass('is-hide');
      } else {
        $lgPrev.removeClass('is-hide');
      }
      if (current) {
        $lgNext.addClass('is-hide');
      } else {
        $lgNext.removeClass('is-hide');
      }
    }

    function movePosition(direction) {
      var dir = direction === 'left' ? 'is-left' : 'is-right';
      if ($modelsList.hasClass('is-left') && direction === 'right') {
        $modelsList.removeClass('is-left');
      } else if ($modelsList.hasClass('is-right') && direction === 'left') {
        $modelsList.removeClass('is-right');
      }
      $modelsList.addClass(dir);
    }

    function initModelsLineup() {
      var min = $modelsList.width();
      var w = $win.width();

      if (w >= min) {
        $modelsList.removeClass('is-left');
        $modelsList.removeClass('is-right');
        $lgNext.addClass('is-hide');
        $lgPrev.addClass('is-hide');
      } else {
        checkPager4Lg();
      }
    }

    initModelsLineup();
    $win.on('resize',initModelsLineup);
  };

  MegaMenu.prototype.initModels4SP = function() {
    if (this.modelsFilter !== undefined) {
      this.modelsFilter.init({layoutNum: 2});
    } else {
      this.modelsFilter = new ModelsFilter($('.st-megaNav_Item-models'), {
        list: '.st-models_list',
        contentList: '.st-models_content',
        layoutNum: 2,
        thumbSize: 's',
        cardSize: 's'
      });
    }
    this.$win.on('resize.models', _.debounce(this.onResizeModelsOfSP, 100));
  };

  MegaMenu.prototype.onResizeModelsOfSP = function() {
    if (mq.currentType === 'PC') { return; }
    this.init4SPUnit();
  };

  MegaMenu.prototype.init4SPUnit = function() {
    unit.destroy(this.modelsFilter.$contentList.find('.sw-models_cardDetail'), {alignHeight: true});
    unit.init(this.modelsFilter.$contentList.find('.sw-models_cardDetail'), 2, {alignHeight: true});
  }


  MegaMenu.prototype.initMagazine = function() {
    var current = 0;
    var isDesabled = false;
    var $magazine = $('.st-magazine');
    var $btns = $magazine.find('.st-magazine_headItem');
    var $magazineItem = $magazine.find('.st-magazine_item');

    $btns.on('click', function() {
      var $t = $(this);
      var idx = $btns.index($t);

      if (isDesabled === true || idx === current) { return; }
      isDesabled = true;

      var $currentMagazineItem = $magazineItem.eq(current);
      var $nextMagazineItem = $magazineItem.eq(idx);

      $btns.eq(current).removeClass('is-selected');
      $t.addClass('is-selected');
      TweenMax.set($nextMagazineItem, {opacity: 0});
      TweenMax.to($currentMagazineItem, 0.1, {opacity: 0, onComplete: function() {
        $currentMagazineItem.addClass('is-hide');
        $nextMagazineItem.removeClass('is-hide');
        TweenMax.to($nextMagazineItem, 0.25, {opacity: 1, delay: 0.2, ease: Power2.easeIn, onComplete: function() {
          current = idx;
          isDesabled = false;
        }});
      }});
    });

    $btns.removeClass('is-selected');
    $magazineItem.addClass('is-hide');
    $btns.eq(current).addClass('is-selected');
    $magazineItem.eq(current).removeClass('is-hide');

    if (mq.currentType === 'PC') { this.initMagazine4PC(); }
    if (mq.currentType === 'SP') { this.initMagazine4SP(); }
    mq.on('PC', this.initMagazine4PC);
    mq.on('SP', this.initMagazine4SP);
  };

  MegaMenu.prototype.initMagazine4PC = function() {
    $('.st-magazineCardGroup').each(function() {
      var $target = $(this);
      var $mcgList = $target.find('.st-magazineCardGroup_list');
      var $mcgNext = $target.find('.st-magazineCardGroup_next');
      var $mcgPrev = $target.find('.st-magazineCardGroup_prev');
      var slideLength = $target.find('.st-magazineCardGroup_item').length;
      var slidesToScroll = 6;

      if ($mcgList.data('initSlick') === true) {
        $mcgList.not('.slick-initialized').slick('unslick');
      } else {
        $mcgList.data('initSlick', true);
      }

      $mcgList.not('.slick-initialized').slick({
        arrows: false,
        infinite: false,
        slidesToShow: slidesToScroll,
        slidesToScroll: slidesToScroll,
        draggable: false,
        dots: false
      });
      checkPager();

      $mcgNext.off('click').on('click', function() {
        $mcgList.not('.slick-initialized').slick('slickNext');
        checkPager();
      });

      $mcgPrev.off('click').on('click', function() {
        $mcgList.not('.slick-initialized').slick('slickPrev');
        checkPager();
      });

      $target.off('mouseenter').on('mouseenter', function() {
        $mcgNext.addClass('is-show');
        $mcgPrev.addClass('is-show');
      });

      $target.off('mouseleave').on('mouseleave', function() {
        $mcgNext.removeClass('is-show');
        $mcgPrev.removeClass('is-show');
      });

      function checkPager() {
        var current = $mcgList.not('.slick-initialized').slick('slickCurrentSlide');
        if (current === 0) {
          $mcgPrev.addClass('is-hide');
        } else {
          $mcgPrev.removeClass('is-hide');
        }
        if (current + 1 >= slideLength - slidesToScroll + 1) {
          $mcgNext.addClass('is-hide');
        } else {
          $mcgNext.removeClass('is-hide');
        }
      }
    });
  };

  MegaMenu.prototype.initMagazine4SP = function() {
    $('.st-magazineCardGroup').each(function() {
      var $target = $(this);
      var $mcgList = $target.find('.st-magazineCardGroup_list');

      if ($mcgList.data('initSlick') === true) {
        $mcgList.not('.slick-initialized').slick('unslick');
      } else {
        $mcgList.data('initSlick', true);
      }

      $mcgList.not('.slick-initialized').slick({
        arrows: false,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: true,
        slidesPerRow: 2,
        rows: 2,
        dots: true,
        appendDots: $target.find('.st-magazineCardGroup_dots')
      });
    });
  };

  MegaMenu.prototype.initBrand = function() {
    this.settingBrand();
    if (mq.currentType === 'PC') { this.initBrand4PC(); }
    if (mq.currentType === 'SP') { this.initBrand4SP(); }
    mq.on('PC', this.initBrand4PC);
    mq.on('SP', this.initBrand4SP);
  };

  MegaMenu.prototype.settingBrand = function() {
    var $bccgListPC = $('.st-brandCategoryCardGroup_list');
    var $bccgListCloneSP = $('.st-brandCategoryCardGroup_list').clone().addClass('u-sp');
    $bccgListPC.addClass('u-pc');
    $bccgListPC.after($bccgListCloneSP);

    var $bcgList = $('.st-brandCardGroup_list');
    var $bcgListCloneSP = $('.st-brandCardGroup_list').clone().addClass('u-sp');
    $bcgList.addClass('u-pc');
    $bcgList.after($bcgListCloneSP);
  };

  MegaMenu.prototype.initBrand4PC = function() {
    // category
    var $bccgList = $('.st-brandCategoryCardGroup_list');
    var $win = $(window);
    var winWidth = $win.outerWidth();
    var offsetXOfTooTIp = -5;
    var offsetYOfTooTIp = 25;

    if ($bccgList.data('initSlick') === true) {
      $bccgList.not('.slick-initialized').slick('unslick');
    }

    // detail
    var $bcg = $('.st-brandCardGroup');
    var $bcgList = $('.st-brandCardGroup_list.u-pc');
    var $bcgNext = $bcg.find('.st-brandCardGroup_next');
    var $bcgPrev = $bcg.find('.st-brandCardGroup_prev');
    var bcgSlideLength = $bcgList.find('.st-brandCardGroup_item').length;
    var bcgSlidesToScroll = 6;
    var bcgSlidesRow = 2;

    if ($bcgList.data('initSlick') === true) {
      $bcgList.not('.slick-initialized').slick('unslick');
    } else {
      $bcgList.data('initSlick', true);
    }

    $bcgList.not('.slick-initialized').slick({
      arrows: false,
      infinite: false,
      slidesToShow: bcgSlidesToScroll,
      slidesToScroll: bcgSlidesToScroll,
      draggable: false,
      slidesPerRow: 1,
      rows: bcgSlidesRow,
      dots: false
    });
    checkPager4Bcg();

    $bcgNext.off('click').on('click', function() {
      $bcgList.slick('slickNext');
      checkPager4Bcg();
    });

    $bcgPrev.off('click').on('click', function() {
      $bcgList.slick('slickPrev');
      checkPager4Bcg();
    });

    $bcg.off('mouseenter').on('mouseenter', function() {
      $bcgNext.addClass('is-show');
      $bcgPrev.addClass('is-show');
    });

    $bcg.off('mouseleave').on('mouseleave', function() {
      $bcgNext.removeClass('is-show');
      $bcgPrev.removeClass('is-show');
    });

    function checkPager4Bcg() {
      var current = $bcgList.slick('slickCurrentSlide');
      if (current === 0) {
        $bcgPrev.addClass('is-hide');
      } else {
        $bcgPrev.removeClass('is-hide');
      }
      if (current + 1 >= bcgSlideLength - bcgSlidesToScroll * bcgSlidesRow + 1) {
        $bcgNext.addClass('is-hide');
      } else {
        $bcgNext.removeClass('is-hide');
      }
    }

    function onMouseEnterToolTip(e) {
      winWidth = $win.outerWidth();

      var $target = $(this);
      var text = $target.attr("data-copy-text");
      if (text === undefined) { return; }
      var $tooltip = $('<div class="st-brandTooltip">' + text + '</div>');
      var x = e.pageX;
      var y = e.pageY;

      $tooltip.css({ left: x + offsetXOfTooTIp, top: y + offsetYOfTooTIp });
      $target.on('mousemove.tooltip', omMouseMoveToolTip);
      $target.data('tooltip', $tooltip);
      $('body').append($tooltip);
    }

    function omMouseMoveToolTip(e) {
      var $target = $(this);
      var $tooltip = $target.data('tooltip');
      var x = e.pageX;
      var y = e.pageY;
      x = calcToopTipInWindow($tooltip, x);
      $tooltip.css({ left: x + offsetXOfTooTIp, top: y + offsetYOfTooTIp });
    }

    function onMouseLeaveToolTip(e) {
      var $target = $(this);
      var $tooltip = $target.data('tooltip');
      $target.off('mousemove.tooltip');
      $($tooltip).remove();
    }

    function calcToopTipInWindow($tooltip , x) {
      var width = $tooltip.outerWidth();
      var winLeft = $win.scrollLeft();
      if (width + x + offsetXOfTooTIp + 20 >= winWidth + winLeft) {
        x = winWidth + winLeft - width - 20;
      }
      return x;
    }
  };

  MegaMenu.prototype.initBrand4SP = function() {
    // category
    var $bccg = $('.st-brandCategoryCardGroup');
    var $bccgList = $('.st-brandCategoryCardGroup_list.u-sp');

    if ($bccgList.data('initSlick') === true) {
      $bccgList.not('.slick-initialized').slick('unslick');
    } else {
      $bccgList.data('initSlick', true);
    }

    $bccgList.not('.slick-initialized').slick({
      arrows: false,
      infinite: false,
      draggable: true,
      dots: true,
      appendDots: $bccg.find('.st-brandCategoryCardGroup_dots')
    });

    // detail
    var $bcg = $('.st-brandCardGroup');
    var $bcgList = $('.st-brandCardGroup_list.u-sp');

    if ($bcgList.data('initSlick') === true) {
      $bcgList.not('.slick-initialized').slick('unslick');
    } else {
      $bcgList.data('initSlick', true);
    }

    $bcgList.not('.slick-initialized').slick({
      arrows: false,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      draggable: true,
      slidesPerRow: 2,
      rows: 2,
      dots: true,
      appendDots: $bcg.find('.st-brandCardGroup_dots')
    });
  };

  APP.class.MegaMenu = MegaMenu;
})(window, jQuery, _, APP);
/**
 * Footer
 */
(function(window, $, _, APP) {
  var mq = APP.module.media;

  function Footer() {
    this.init = this.init.bind(this);
    this.init4PC = this.init4PC.bind(this);
    this.init4SP = this.init4SP.bind(this);
    this.init();
  }

  Footer.prototype.init = function() {
    var isHTTPS = location.protocol === 'https:';
    var isCrossDomain = location.host !== APP.config.host;
    var $footer;

    $footer = $(APP.config.footerHTML);

    if (isCrossDomain === true) {
      APP.util.replaceAbsolutePathOfImage($footer.find('img'));
    }

    if (isHTTPS === true || isCrossDomain === true) {
      APP.util.replaceAbsolutePathOfAnchor($footer.find('a'));
    }

    $('.st-footer').append($footer);
    if (mq.currentType === 'PC') { this.init4PC(); }
    if (mq.currentType === 'SP') { this.init4SP(); }
    mq.on('PC', this.init4PC);
    mq.on('SP', this.init4SP);

    var $topBtn = $('.st-topAnchor_button');
    var scrollController = new ScrollMagic.Controller();
    var tween = TweenMax.to($topBtn, .3, {opacity: 1});
    new ScrollMagic.Scene({
      triggerElement: '.st-main',
      triggerHook: 'onLeave'
    })
      .setTween(tween)
      .addTo(scrollController);

    new ScrollMagic.Scene({
      triggerElement: '.st-topAnchor',
      triggerHook: 'onEnter'
    })
      .setClassToggle('.st-topAnchor', 'is-relative')
      .addTo(scrollController);
  };

  Footer.prototype.init4PC = function() {
    $('.st-footer').off('click.footerMenu');
  };

  Footer.prototype.init4SP = function() {
    var self = this;
    $('.st-footer').on('click.footerMenu', '.st-footer_groupTitleLink-dropdown', function() {
      self.onClickMenuHead(this, {parent: '.st-footer_groupItem', body: '.st-footer_groupContent'});
    });
  };

  // SP用 アコーディオンメニューの開閉
  Footer.prototype.onClickMenuHead = function(self, obj) {
    var $target = $(self);
    var $parent = $target.closest(obj.parent);
    var $body = $parent.find(obj.body);
    if ($target.hasClass('is-open') === true) {
      $target.removeClass('is-open');
      $body.removeClass('is-open');
    } else {
      $target.addClass('is-open');
      $body.addClass('is-open');
    }
  };

  APP.class.Footer = Footer;
})(window, jQuery, _, APP);
/**
 * Header
 */
(function(window, $, _, APP) {
  var mq = APP.module.media;
  var util = APP.util;
  var share = APP.module.share;

  var newsTemplate = ''
      + '<div class="st-headNews">'
      + '  <div class="st-headNews_inner">'
      + '    <ul class="st-headNews_list">'
      + '<% _.forEach(news, function(n) { %>'
      + '      <li class="st-headNews_item">'
      + '        <span class="st-headNews_date"><%= n.date %></span>'
      + '        <span class="st-headNews_detail"><%= n.detail %></span>'
      + '      </li>'
      + '<% }); %>'
      + '    </ul>'
      + '    <button class="st-headNews_close"></button>'
      + '  </div>'
      + '</div>';

  function Header() {
    this.init = this.init.bind(this);
    this.init4PC = this.init4PC.bind(this);
    this.init4SP = this.init4SP.bind(this);
    this.init();
  }

  Header.prototype.init = function() {
    var isHTTPS = location.protocol === 'https:';
    var isCrossDomain = location.host !== APP.config.host;
    var newsHTML = '';
    var $header;

    if (APP.config.news.length > 0) {
      newsHTML += this.createNewsHTML();
    }

    $header = $(newsHTML + APP.config.headerHTML);

    if (isCrossDomain === true) {
      APP.util.replaceAbsolutePathOfImage($header.find('img'));
    }

    if (isHTTPS === true || isCrossDomain === true) {
      APP.util.replaceAbsolutePathOfAnchor($header.find('a'));
    }

    $('.st-header').append($header);
    this.megaMenu = new APP.class.MegaMenu();
    if (mq.currentType === 'PC') { this.init4PC(); }
    if (mq.currentType === 'SP') { this.init4SP(); }
    mq.on('PC', this.init4PC);
    mq.on('SP', this.init4SP);

    // 緊急メニューの非表示
    $('.st-headNews_close').on('click', function() {
      $('.st-headNews').css({display: 'none'});
    });
    this.initShare();
    this.addSearchScript();
  };

  Header.prototype.addSearchScript = function() {
    $('body').append('<script id="m_sug" src="//' + APP.config.host + '/common/mf/js/m_sug.js"></script>')
  };

  Header.prototype.initShare = function() {
    $('.st-header_shareText-fb').on('click', function() {
      var url = document.location.href;
      share.facebook(url);
      return false;
    });
    $('.st-header_shareText-tw').on('click', function() {
      var url = document.location.href;
      var text = $('title').text();
      share.twitter(url, text);
      return false;
    });
    $('.st-header_shareText-google').on('click', function() {
      var url = document.location.href;
      share.google(url);
      return false;
    });
  };

  Header.prototype.init4PC = function() {
    var $subNav = $('.st-header_subNav');
    var $tooltipNav = $subNav.find('.st-header_subNavLink-tooltip');
    var $controlShare = $('.st-header_controlItem-share');

    util.alignTextWidth('.st-header_subNavTooltip', '.sw-tooltip_text');
    $('.st-header_spMenu').off('click.menuModal');
    $('.st-header_controlIcon-share').off('click.shareModal');
    $('.st-header_megaNav').off('click.headerMegaMenu');
    $subNav.off('click.headerSuvMenu');
    $('.js-search').on('click.searchIcon', this.onClickSearchIcon);

    // タブレットなどのタッチデバイス対策
    // hover処理無効化、clickイベントに切り替え
    $tooltipNav.removeClass('is-mobile');
    if ($.browser.mobile === true) {
      $tooltipNav.addClass('is-mobile');
      $controlShare.addClass('is-mobile');
      $controlShare.off('click.navTooltip').on('click.navTooltip', this.onClickNavTooltip);
    } else {
      $controlShare.off('click.navTooltip');
    }
  };

  Header.prototype.init4SP = function() {
    var self = this;
    var $subNav = $('.st-header_subNav');
    var $tooltipNav = $subNav.find('.st-header_subNavLink-tooltip');
    var $controlShare = $('.st-header_controlItem-share');

    $('.st-header_spMenu').on('click.menuModal', this.onClickMenuIcon);
    $('.st-header_controlIcon-share').on('click.shareModal', this.onClickShareIcon);
    $('.st-header_megaNav').on('click.headerMegaMenu', '.st-header_navText-dropdown', function() {
      self.onClickMenuHead(this, {parent: '.st-megaNav_Item', body: '.st-header_navContent'});
    });
    $subNav.on('click.headerSuvMenu', '.st-header_subNavText-dropdown', function() {
      self.onClickMenuHead(this, {parent: '.st-header_subNavLink-tooltip', body: '.st-header_subNavTooltip'});
    });
    $('.js-search').off('click.searchIcon');
    $tooltipNav.removeClass('is-mobile');
    $controlShare.removeClass('is-mobile');
    $controlShare.off('click.navTooltip');
  };

  Header.prototype.onClickNavTooltip = function(e) {
    var $target = $(this);

    if ($target.hasClass('is-open') === true) {
      $target.removeClass('is-open');
    } else {
      $target.addClass('is-open');
    }
  };

  Header.prototype.createNewsHTML = function() {
    var compiled = _.template(newsTemplate);
    return compiled({ news: APP.config.news });
  };

  // PC用 header検索ボックスの表示切り替え
  Header.prototype.onClickSearchIcon = function() {
    var $target = $(this);
    var $content = $('.st-header_search');
    var $input = $('.st-header_searchInput');
    var $navList = $('.st-header_mainNavList');
    if ($target.hasClass('is-open') === true) {
      $target.removeClass('is-open');
      $content.removeClass('is-open');
      $navList.removeClass('is-hidden');
    } else {
      $target.addClass('is-open');
      $content.addClass('is-open');
      $navList.addClass('is-hidden');
      setTimeout(function() { // 表示のイージング分ずらす
        $input.focus();
      }, 300);
    }
  };

  // SP用 menuモーダルの表示切り替え
  Header.prototype.onClickMenuIcon = function() {
    var $target = $(this);
    var $content = $('.st-header_nav');
    var $navContent = $('.st-header_navContent');

    if ($target.hasClass('is-open') === true) {
      $target.removeClass('is-open');
      $content.removeClass('is-open');
      $navContent.addClass('is-close');
    } else {
      $target.addClass('is-open');
      $content.addClass('is-open');
      $navContent.removeClass('is-close');
    }
  };

  // SP用 shareモーダルの表示切り替え
  Header.prototype.onClickShareIcon = function() {
    var $target = $(this);
    var $content = $('.st-header_share');
    var $menuIcon = $('.st-header_spMenu');
    if ($target.hasClass('is-open') === true) {
      $target.removeClass('is-open');
      $content.removeClass('is-open');
      $menuIcon.removeClass('is-hidden');
    } else {
      $target.addClass('is-open');
      $content.addClass('is-open');
      $menuIcon.addClass('is-hidden');
    }
  };

  // SP用 アコーディオンメニューの開閉
  Header.prototype.onClickMenuHead = function(self, obj) {
    var $target = $(self);
    var $parent = $target.closest(obj.parent);
    var $body = $parent.find(obj.body);
    if ($target.hasClass('is-open') === true) {
      $target.removeClass('is-open');
      $body.removeClass('is-open');
    } else {
      $target.addClass('is-open');
      $body.addClass('is-open');
    }
  };

  APP.class.Header = Header;
})(window, jQuery, _, APP);
/**
 * conversion
 */
(function(window, $, _, APP) {
  var mq = APP.module.media;

  var notConversionList = [
      new RegExp('^/mailnews/'),
      new RegExp('^/request/catalog/service/'),
      new RegExp('^/dealership/'),
      new RegExp('^/request/simulation/'),
      new RegExp('^/request/trial/'),
      new RegExp('^/request/estimate/'),
      new RegExp('^/request/consult/'),
      new RegExp('^/folw/simulation/')
  ];

  var conversionHTML = ''
      + '<div class="st-conversion">'
      + '  <div class="st-conversion_btn"><span class="st-conversion_icon st-conversion_icon-info"></span></div>'
      + '  <div class="st-conversion_list">'
      + '    <div class="st-conversion_mask"></div>'
      + '    <a class="st-conversion_item" href="/mailnews/"><span class="st-conversion_icon st-conversion_icon-mail"></span><span class="st-conversion_text">メールニュース</span></a>'
      + '    <a class="st-conversion_item" href="/request/catalog/service/catalogselect"><span class="st-conversion_icon st-conversion_icon-catalog"></span><span class="st-conversion_text">カタログ請求</span></a>'
      + '    <a class="st-conversion_item" href="/dealership/"><span class="st-conversion_icon st-conversion_icon-shop"></span><span class="st-conversion_text">販売店検索</span></a>'
      + '    <a class="st-conversion_item" href="/request/trial/"><span class="st-conversion_icon st-conversion_icon-calender"></span><span class="st-conversion_text">試乗予約</span></a>'
      + '    <a class="st-conversion_item" href="/request/estimate/"><span class="st-conversion_icon st-conversion_icon-document"></span><span class="st-conversion_text">見積り依頼</span></a>'
      + '    <a class="st-conversion_item" href="/request/consult/"><span class="st-conversion_icon st-conversion_icon-operation"></span><span class="st-conversion_text">ご購入相談</span></a>'
      + '  </div>'
      + '</div>';

  var $conversion, $btn, $list, $mask;
  var timer;
  init();

  function init() {
    var $body = $('body');
    var i = 0, len = notConversionList.length;
    var pathName = window.location.pathname;

    for (; i < len; i++) {
      if (notConversionList[i].test(pathName) === true) {
        return;
      }
    }

    if ($body.hasClass('not-conversion') === true) {
      return;
    }

    var isHTTPS = location.protocol === 'https:';
    var isCrossDomain = location.host !== APP.config.host;

    $conversion = $(conversionHTML);

    var coolieValue = $.cookie('stConversion');
    if (coolieValue !== 'close') {
      $conversion.addClass('is-open');
    }

    var d = new Date();
    d.setTime( d.getTime() + (5 * 60 * 60 * 1000));
    $.cookie('stConversion', 'close', { path: '/', expires: d });

    if (isCrossDomain === true) {
      APP.util.replaceAbsolutePathOfImage($conversion.find('img'));
    }

    if (isHTTPS === true || isCrossDomain === true) {
      APP.util.replaceAbsolutePathOfAnchor($conversion.find('a'));
    }

    $body.append($conversion);
    $conversion = $('.st-conversion');
    $btn = $conversion.find('.st-conversion_btn');
    $list = $conversion.find('.st-conversion_list');
    $mask = $conversion.find('.st-conversion_mask');

    if (mq.currentType === 'PC') { init4PC(); }
    if (mq.currentType === 'SP') { init4SP(); }
    mq.on('PC', init4PC);
    mq.on('SP', init4SP);
    $btn.on('click', open);

    timer = setTimeout(function() {
      close();
    }, 2000);

  }

  function init4PC() {
    if ($.isTablet !== true) {
      $mask.on('click', open);
      $list.on('mouseenter', open);
      $list.on('mouseleave', close);
    }
  }

  function init4SP() {
    $mask.off('click', open);
    $list.off('mouseenter', open);
    $list.off('mouseleave', close);
  }

  function open() {
    $conversion.addClass('is-open');
    $(window).on('scroll', close);
  }
  function close() {
    clearTimeout(timer);
    $conversion.removeClass('is-open');
    $(window).off('scroll', close);
  }


}(window, jQuery, _, APP));
/**
 * common
 */
(function(window, $, _, APP) {
  var mq = APP.module.media;
  var gModal = APP.module.galleryMmodal;
  var util = APP.util;

  // tablet compatible
  if ($.isTablet === true) {
    $('body').addClass('is-tablet');
    $('meta[name="viewport"]').attr('content', 'width=1024');
  }

  var header = new APP.class.Header();
  var footer = new APP.class.Footer();

  if (mq.currentType === 'PC') { init4PC(); }
  if (mq.currentType === 'SP') { init4SP(); }
  mq.on('PC', init4PC);
  mq.on('SP', init4SP);
  init();

  function init4PC() {
      util.alignTextWidth('.sw-tooltip', '.sw-tooltip_text');
  }

  function init4SP() {
      util.alignTextWidth('.sw-tooltip', '.sw-tooltip_text');
  }

  function init() {
    $('.sw-accordion').on('click.accordion', '.sw-accordion_head', onClickAccordionHead);
    $('.js-stMovieModal').on('click', function(e) {
      e.preventDefault();
      var info = {};
      info.type = 'movie';
      info.id = $(this).attr('data-movie-id');
      if (info.id === undefined) { return; }
      gModal.open(info);
    });

    $('.js-stImageModal').on('click', function(e) {
      e.preventDefault();
      var info = {};
      info.type = 'image';
      info.imagePath = $(this).attr('data-image-path');
      if (info.imagePath === undefined) { return; }
      gModal.open(info);
    });
  }

  // accordion
  function onClickAccordionHead() {
    var $target = $(this);
    var $parent = $target.closest('.sw-accordion_item');
    if ($parent.hasClass('is-open') === true) {
      $parent.removeClass('is-open');
    } else {
      $parent.addClass('is-open');
    }
  }

  // smooth scroll
  $('body').on('click.scrollTop', '.js-scrollTo', function() {
    var $current = $(this);
    var href = $current.attr('href') || $current.attr('data-anchor');
    var offset1 = $($current.attr('data-offset-target')).height() || 0;
    var offset2 = $current.attr('data-offset') ? Number($current.attr('data-offset')) : 0;
    var $target = $(href === '#' || href === '' ? 'body' : href);
    var topPosition = $target.offset().top - offset1 - offset2;
    var time = Math.abs(topPosition - $current.offset().top) * 0.7;
    if (time <= 600) { time = 600; }
    if (time >= 1300) { time = 1300; }
    time = time / 1000;

    TweenMax.to($('body, html'), time, {
      ease : Power4.easeOut,
      scrollTop: topPosition
    });
    return false;
  });

}(window, jQuery, _, APP));

/**
 * addClickCount
 */
(function(window, $, _, APP) {

  var $win = $(window);

  $win.on('load',function(){
    addClickCountLink('.st-models_content','header:lineup-');
    addClickCountLink('.models_content','top:lineup-');
  });

  function addClickCountLink(elem, setName) {
    var self = $(elem).find('a');
    self.each(function(){
      var $this = $(this);
      var href = $this.attr('href').split('/');
      var value = href.pop().match('html') ? href.pop() : href[href.length - 1];
      $this.attr('onclick','sc(\'lexus:' + setName + value + '\');');
    });
  }

}(window, jQuery, _, APP));

/**
 * No download Image
 */
(function(window, $, _, APP) {
  $('.nodl-img').each(function() {
    $(this).attr('onmousedown', 'return false');
    $(this).attr('onselectstart', 'return false');
    $(this).attr('oncontextmenu', 'return false');
  });
}(window, jQuery, _, APP));

/* appEOF > 最後まで読み込んだかどうか */
var appEOF = true;
