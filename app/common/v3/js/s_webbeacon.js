/* ------------------------------------------------------
   更新履歴
------------------------------------------------------ */
/* 2017/3/17
   ・新規作成
------------------------------------------------------ */

if (typeof (_SCoutputBc) === 'undefined') {
	var _SCoutputBc = false;
}

//ビーコン出力
function SCoutput_bc() {
	if (typeof (v2OTbodyLoaded) === 'undefined') {
		if (_SCoutputBc === false) {
			_SCoutputBc = true;
			// Google Tag Manager
			document.write('<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T9NWLH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>');
			(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-T9NWLH');

			document.write('<script type="text/javascript" src="//onetag.tws.toyota.jp/onetag/body"></script>');
		}
	}
}

if (typeof (_OTheadLoaded) === 'undefined') {
	var _OTheadLoaded = false;
}
if (typeof (v2OTheadLoaded) === 'undefined') {
	if (_OTheadLoaded === false) {
		_OTheadLoaded = true;
		document.write('<script type="text/javascript" src="//onetag.tws.toyota.jp/onetag/head"></script>');
	}
}
