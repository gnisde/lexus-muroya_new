// -- HTML上のリンクのクリック数の計測 2008/05/19 --
// -- サイトカタリストコードの追加 2009/11/05 --
// -- 指定された文字の先頭にex-を追加 2009/11/09 --
// -- サイトセンサス削除 2009/12/02 --

var clickCount = true;

function sc(contentsID){
	if(contentsID.indexOf("lexus:header:") > -1 || contentsID.indexOf("lexus:footer:") > -1 || contentsID.indexOf("lexus.smp:globalmenu:") > -1 || contentsID.indexOf("lexus.smp:footerlink:") > -1)
	{
		var s=s_gi('toyotamotorlexusprd');
		s.linkTrackVars='prop48,eVar14';
		s.eVar14= contentsID;
		s.prop48= contentsID;
		s_sc_cd.tl(this,'o','ex-' + contentsID);
	}else{
		s_sc_cd.tl(this,'o','ex-' + contentsID);
	}
}

/*(function(){
	var path = location.pathname;
	var w = window.innerWidth;

	if(path === '/dealer-search/search-result') {
		var breakPoint = 599;
		var map,mapWrap,mapControl;

		function searchPrint() {
			map = document.getElementById('map');
			mapWrap = document.getElementById('mapMapContainer');
			mapControl = document.getElementById('mapTMILayerControlContainer');
			//print style
			var innerStyle  = '\nbody { ' +
								'display: block;' +
								'width: ' + w + 'px; ' + 
								'margin: 0 auto;' +
							  ' }';

				innerStyle += '\n.container {' +
								'min-width: ' + w + 'px;' +
							  ' }';

				innerStyle += '\n#map {' +
								'height: ' + map.clientHeight + 'px;' +
							  ' }';

				innerStyle += '\n#mapMapContainer { ' +
								'width: ' + mapWrap.clientWidth + 'px;' +
								'height: ' + mapWrap.clientHeight + 'px;' +
							  ' }';

				innerStyle += '\n#mapTMILayerControlContainer { ' +
								'top: ' + mapControl.style.top + ';' +
								'left: ' + mapControl.style.left + ';' +
							  ' }\n';
			cleateStyle('printStyle', 'print', innerStyle);
		}

	    function beforePrint() {
	    	if (w > 1280) { 
	    		var smallSize = 1280 / w;
	    		var positionX = (1 - smallSize) / 4 * 100;

	    		var ratioX = Math.floor(mapControl.style.top.split('p')[0] * smallSize);
	    		var ratioY = Math.floor(mapControl.style.left.split('p')[0] * smallSize);
	    		var mapPositionX = Number(mapControl.style.top.split('p')[0]) + (ratioX / 2);
	    		var mapPositionY = Number(mapControl.style.left.split('p')[0]) + (ratioY / 2);

				var setScaleStyle  = '\n.container { ' +
										'min-width: ' + w * smallSize + 'px;' +
										'transform: scale(' + smallSize + ');' +
										'position: absolute;' +
										'left: -' + positionX + '%;' +
								 	 ' }\n';

					setScaleStyle += '\n#mapTMILayerControlContainer { ' +
										'top: ' + mapPositionX + 'px !important;' +
										'left: ' + mapPositionY + 'px !important;' +
								 	 ' }\n';
	        	cleateStyle('setScale', 'print', setScaleStyle);
	        } else if (w <= breakPoint) {
				var setScaleStyle  = '\n.result-items * {' +
									    'display: inline-block;' +
									 ' }';

					setScaleStyle += '\n.result-items {' +
									    'border: none;' +
									 ' }';

					setScaleStyle += '\n.sheet-header {' +
									 	'display: none;' +
									 ' }';

					setScaleStyle += '\n.dealer_title {' +
										'border: none;' +
										'display: block;' +
									 ' }';

					setScaleStyle += '\n.dealer_content {' +
										'display: table;' +
									 ' }';

					setScaleStyle += '\n.dealer_content p {' +
										'display: table-cell;' +
									 ' }\n';
	        	cleateStyle('setScale', 'print', setScaleStyle);	
	        }
	    }

	    function afterPrint() {
	    	if (w > 1280) { 
	    		var target = document.getElementById('setScale');
	        	target.parentNode.removeChild(target);
	        }
	    }

	    if (window.matchMedia) {
	        var mediaQueryList = window.matchMedia('print');
	        mediaQueryList.addListener(function(mql) {
	            if (mql.matches) {
	                beforePrint();
	            } else {
	                afterPrint();
	            }
	        });
	    }

		window.onload = function(){ 
			searchPrint();
		    window.onbeforeprint = beforePrint;
		    window.onafterprint = afterPrint;
		}
		window.onresize = function(){ 
			w = window.innerWidth;
			searchPrint();
		}
	}

	function cleateStyle(id, media, style) {
		var getID = document.getElementById(id);
		if (getID) {
			Modified(getID, id, media, style);
		} else {
			cleate(id, media, style);
		}

		function cleate(id, media, style) {
			s = document.createElement('style');
			s.type = 'text/css';
			s.id = id;
			s.media = media;
			s.innerHTML = style;
			document.getElementsByTagName("head")[0].appendChild(s);
		}

		function Modified(target, id, media, style) {
			target.parentNode.removeChild(target);
			cleate(id, media, style);
		}
	}
})();*/