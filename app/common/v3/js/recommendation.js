/**
 * recommendation.js
 */

//app.jsが読み込まれていない場合の対策
var APP = APP || {};

//visionary.jsが読み込まれていない場合の対策
var VISIONARY = VISIONARY || {};

//Rtoaster送信イベント
(function($, Rtoaster, _, APP, VISIONARY) {

  $('body').append('<div id="day_review_change" style="height: 0; text-indent: -9999px; overflow: hidden;"></div>');

  var branchPath;

  var recommendTarget;
  var domein = location.hostname;
  var pathname = location.pathname;

  var callbackFlag = false;

  if (pathname.search(/^\/magazine\/(\w+)\/(.*.|)$/) > -1 || pathname === '/magazine/' || pathname === '/magazine/index.html') {
    recommendTarget = [ 'day_review_change', 'visionary_recommendation_day_visionary', 'visionary_recommendation_day_brand', 'visionary_recommendation_review_car', 'visionary_recommendation_review_request', 'visionary_ranking' ];
    branchPath = 'visionary';
  } else if (pathname === '/' || pathname === '/index.html' || pathname === '/index_spindlerink.html') {
    recommendTarget = [ 'day_review_change', 'top_recommendation_day_visionary', 'top_recommendation_day_brand', 'top_recommendation_day_car', 'top_recommendation_review_car', 'top_recommendation_review_brand', 'top_recommendation_review_visionary', 'top_model_history_review', 'top_model_recommend_review', 'top_simulation_history_review', 'top_find_dealer_review' ,'top_tokyo_dealer'];
    branchPath = 'top';
  } else if (pathname.search(/^\/models\/(\w+)\/(.*.|)$/) > -1) {
    recommendTarget = [ 'day_review_change', 'models_recommendation_day_car', 'models_recommendation_review_car', 'models_recommendation_review_request' ];
    branchPath = 'models';
  } else if (pathname.search(/^\/brand\/(\w+)\/(.*.|)$/) > -1) {
    recommendTarget = [ 'day_review_change', 'brand_recommendation' ];
    branchPath = 'brand';
  }

  //////////////////// TOP ////////////////////
  var initTop = function () {
    var requestRecommend = {};
    var FeaturesRecommend = {};

    var requestRecommendWrap =  '<div class="conversion_main">' +
                                '<div class="conversion_mainInner">' +
                                '{recomendation_browsingHistory}' +
                                '{recomendation_recommendModel}' +
                                '{recomendation_simulationHistory}' +
                                '</div>' +
                                '<div class="conversion_dots"></div>';

    var findDealerWrap =        '<div class="conversion_aside">' +
                                '{recomendation}' +
                                '</div>';

    var FeaturesRecommendWrap = '<div id="features" class="sw-section">' +
                                '<div class="sw-section_head">' +
                                '<span class="sw-section_headInner">' +
                                '<h2 class="sw-section_title">FEATURES</h2>' +
                                '<p class="sw-section_lead">特集／ピックアップ</p>' +
                                '</span>' +
                                '</div>' +
                                '{reccomendation_day}' +
                                '{reccomendation_review}' +
                                '</div>';

    //検討期 最近閲覧したモデル
    requestRecommend.browsingHistory =  　'<div id="top_model_history_review" class="conversion_item conversion_item-models"></div>';

    //検討期 おすすめモデル
    requestRecommend.recommendModel =    '<div id="top_model_recommend_review" class="conversion_item conversion_item-models"></div>';

    //検討期 シミュレーション履歴
    requestRecommend.simulationHistory = '<div id="top_simulation_history_review" class="conversion_item conversion_item-models"></div>';

    //検討期 お近くの販売店を探す
    var findDealer =  '<div id="top_find_dealer_review" class="conversion_item conversion_item-dealer"></div>';

    //日常期 Features
    FeaturesRecommend.dayFeatures =     '<div class="day sw-section_body">' +
                                        '<div id="top_recommendation_day_visionary" class="fCardGroup moreCard">' +
                                        '<div id="top_recommendation_day_visionary_1" class="fCardGroup_item fCard firstItem"></div>' +
                                        '<div id="top_recommendation_day_visionary_2" class="fCardGroup_item fCard"></div>' +
                                        '<div id="top_recommendation_day_visionary_3" class="fCardGroup_item fCard"></div>' +
                                        '</div>' +
                                        '<div id="top_recommendation_day_car" class="fCardGroup">' +
                                        '<div id="top_recommendation_day_car_1" class="fCardGroup_item fCard firstItem"></div>' +
                                        '<div id="top_recommendation_day_car_2" class="fCardGroup_item fCard"></div>' +
                                        '</div>' +
                                        '<div id="top_recommendation_day_brand-wrap" class="fCardGroup">' +
                                        '<div id="top_recommendation_day_brand" class="fCardGroup_item fCard"></div>' +
                                        '</div>' +
                                        '<div class="moreIcon u-sp" onClick="sc(\'lexus:top:features:more\');"></div>' +
                                        '</div>';

    //検討期 Features
    FeaturesRecommend.reviewFeatures =  '<div class="sw-section_body review">' +
                                        '<div id="top_recommendation_review_car" class="fCardGroup moreCard">' +
                                        '<div id="top_recommendation_review_car_1" class="fCardGroup_item fCard firstItem"></div>' +
                                        '<div id="top_recommendation_review_car_2" class="fCardGroup_item fCard"></div>' +
                                        '<div id="top_recommendation_review_car_3" class="fCardGroup_item fCard"></div>' +
                                        '</div>' +
                                        '<div id="top_recommendation_review_visionary" class="fCardGroup">' +
                                        '<div id="top_recommendation_review_visionary_1" class="fCardGroup_item fCard firstItem"></div>' +
                                        '<div id="top_recommendation_review_visionary_2" class="fCardGroup_item fCard"></div>' +
                                        '</div>' +
                                        '<div id="top_recommendation_review_brand-wrap" class="fCardGroup">' +
                                        '<div id="top_recommendation_review_brand" class="fCardGroup_item fCard"></div>' +
                                        '</div>' +
                                        '<div class="moreIcon u-sp" onClick="sc(\'lexus:top:features:more\');"></div>' +
                                        '</div>';

    var appendTargetRequest = requestRecommendWrap.replace('{recomendation_browsingHistory}',requestRecommend.browsingHistory).replace('{recomendation_recommendModel}',requestRecommend.recommendModel).replace('{recomendation_simulationHistory}',requestRecommend.simulationHistory)
    var appendTargetFindDealer = findDealerWrap.replace('{recomendation}',findDealer);
    var appendTargetFeatures = FeaturesRecommendWrap.replace('{reccomendation_day}',FeaturesRecommend.dayFeatures).replace('{reccomendation_review}',FeaturesRecommend.reviewFeatures);

    $('.conversion').append(appendTargetRequest);
    $('.conversion').append(appendTargetFindDealer);
    $('#recommendation').append(appendTargetFeatures);

    //debug
    //console.log(appendTargetRequest);
    //console.log(appendTargetFindDealer);
    //console.log(appendTargetFeatures);
  };

  //////////////////// MODELS ////////////////////
  var initModels = function () {
    var recommend = {};

    var recommendWrap = '<div class="sw-container">' +
                        '<div class="sw-section_head">' +
                        '<span class="sw-section_headInner">' +
                        '<h2 class="sw-section_title">RECOMMENDED</h2>' +
                        '<p class="sw-section_lead">おすすめコンテンツ</p>' +
                        '</span>' +
                        '</div>' +
                        '<div class="sw-section_body">' +
                        '{recommendasion_day}' +
                        '{recommendasion_review}' +
                        '</div>' +
                        '</div>';

    //日常期
    recommend.dayRecommend =    '<div id="models_recommendation_day_car" class="fCardGroup day">' +
                                '<div id="models_recommendation_day_car_1" class="fCardGroup_item fCard"></div>' +
                                '<div id="models_recommendation_day_car_2" class="fCardGroup_item fCard"></div>' +
                                '<div id="models_recommendation_day_car_3" class="fCardGroup_item fCard lastItem"></div>' +
                                '</div>';
    //検討期
    recommend.reviewRecommend = '<div id="models_recommendation_review_car" class="fCardGroup review">' +
                                '<div id="models_recommendation_review_car_1" class="fCardGroup_item fCard"></div>' +
                                '<div id="models_recommendation_review_car_2" class="fCardGroup_item fCard"></div>' +
                                '</div>' +
                                '<div id="models_request" class="fCardGroup review">' +
                                '<div id="models_recommendation_review_request" class="fCardGroup_item fCard lastItem"></div>' +
                                '</div>';

    var appendTargetRecommend = recommendWrap.replace('{recommendasion_day}',recommend.dayRecommend).replace('{recommendasion_review}',recommend.reviewRecommend);
    $('#recommendation').append(appendTargetRecommend);

    //debug
    //console.log(appendTargetRecommend);
  };

  //////////////////// BRAND ////////////////////
  var initBrand = function () {
    var recommend = {};

    var recommendWrap = '<div class="sw-container">' +
                        '<div class="sw-section_head">' +
                        '<span class="sw-section_headInner">' +
                        '<h2 class="sw-section_title">RECOMMENDED</h2>' +
                        '<p class="sw-section_lead">おすすめコンテンツ</p>' +
                        '</span>' +
                        '</div>' +
                        '<div class="sw-section_body">' +
                        '{recommendasion}' +
                        '</div>' +
                        '</div>';

    //日常期
    recommend = '<div id="brand_recommendation" class="fCardGroup">' +
                '<div id="brand_recommendation_1" class="fCardGroup_item fCard"></div>' +
                '<div id="brand_recommendation_2" class="fCardGroup_item fCard"></div>' +
                '<div id="brand_recommendation_3" class="fCardGroup_item fCard"></div>' +
                '</div>';

    var appendTargetRecommend = recommendWrap.replace('{recommendasion}',recommend);
    $('#recommendation').append(appendTargetRecommend);

    //debug
    //console.log(appendTargetRecommend);
  };

  //////////////////// Visionary ////////////////////
  var initVisionary = function () {
    var recommend = {};

    var recommendWrap = '<div class="vsw-articleCardGroup vsw-articleCardGroup-slider">' +
                        '{recommendation_day}' +
                        '{recommendation_review}' +
                        '</div>';

    recommend.day = '<ul id="visionary_recommendation_day_visionary" class="vsw-articleCardGroup_list day">' +
                    '<li id="visionary_recommendation_day_visionary_1" class="vsw-articleCardGroup_item"></li>' +
                    '<li id="visionary_recommendation_day_visionary_2" class="vsw-articleCardGroup_item"></li>' +
                    '</ul>' +
                    '<ul id="visionary_recommendation_day_brand-wrap" class="sw-articleCardGroup_list day">' +
                    '<li id="visionary_recommendation_day_brand" class="vsw-articleCardGroup_item"></li>' +
                    '</ul>';

    //検討期
    recommend.review = '<ul id="visionary_recommendation_review_car" class="vsw-articleCardGroup_list review">' +
                       '<li id="visionary_recommendation_review_car_1" class="vsw-articleCardGroup_item"></li>' +
                       '<li id="visionary_recommendation_review_car_2" class="vsw-articleCardGroup_item"></li>' +
                       '</ul>' +
                       '<ul id="visionary_recommendation_review_request-wrap" class="sw-articleCardGroup_list review">' +
                       '<li id="visionary_recommendation_review_request" class="vsw-articleCardGroup_item"></li>' +
                       '</ul>';

    var recommendRankingWrap = '<div class="vsw-rankingCardGroup">' +
                               '{recommendation}' +
                               '</div>';

    var recommendRanking =  '<ul id="visionary_ranking" class="vsw-rankingCardGroup_list">' +
                            '<li id="visionary_ranking_1" class="vsw-rankingCardGroup_item"></li>' +
                            '<li id="visionary_ranking_2" class="vsw-rankingCardGroup_item"></li>' +
                            '<li id="visionary_ranking_3" class="vsw-rankingCardGroup_item"></li>' +
                            '</ul>';


    var appendTargetRecommend = recommendWrap.replace('{recommendation_day}',recommend.day).replace('{recommendation_review}',recommend.review);
    $('#recommendation').append(appendTargetRecommend);

    var appendTargetRecommendRanking = recommendRankingWrap.replace('{recommendation}',recommendRanking);
    $('#recommendation-ranking').append(appendTargetRecommendRanking);

    //debug
    //console.log(appendTargetRecommend);
    //console.log(appendTargetRecommendRanking);
  };

  switch (branchPath) {
    case 'top' : initTop(); break;
    case 'models' : initModels(); break;
    case 'brand' : initBrand(); break;
    case 'visionary' : initVisionary(); break;
    default : break;
  }

  //Rtoaster Callback Start
  var rcmCallback = function () {
    if (callbackFlag === true) { return; }

    /////////////////////// 共通処理 //////////////////////////
    var userStatus = ($('#day_or_review').data('day') === 0) ? 'day' : 'review' ;
    var review = $('.review');
    var day = $('.day');

    if (userStatus === 'review') { /*emptyBanner('review','fCard'); console.log('検討期');*/ day.remove(); addClickCount('.review','.fCard_image'); }
    if (userStatus === 'day') { /*emptyBanner('day','fCard'); console.log('日常期');*/ review.remove(); addClickCount('.day','.fCard_image'); }

    if ($('.review').length < 1 || $('.day').length < 1) { addClickCount('#recommendation','.fCard_image'); }

    //click count
    function addClickCount (self, target, category) {
      //var targetCb = arguments[0];
      //if (targetCb != recommendTarget) return;
      var $targetCb = $(self);
      //console.log(arguments);

      // sc
      // http://tools.ietf.org/html/rfc2396#appendix-B
      var reg = /^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/;

      var baseUri = location.pathname.replace(/\\/g,'/').replace(/\/[^/]*$/,'');
      if (baseUri.match(reg)) {
        if (branchPath === 'top') {
          var bid = ':top'
        } else {
          var bid = RegExp.$5.replace(/[\/]/g, ":");
        }
      }

      if ($targetCb.find('a').length < 1) { setTimeout(function () { setClickCount(); },500); } else { setClickCount(); }

      function setClickCount () {
        $targetCb.find('a').each(function(e) {
          var uri = $(this).attr('href');
          if (uri.match(reg)) {
              var cid = RegExp.$4 + RegExp.$5.replace(/[\/]/g, "-");
          }

          var pagePosition = (typeof category !== 'undefined') ? category : 'recommend';

          var imgUri = $(this).find(target).find('img').attr('src');

          if (typeof imgUri === 'undefined') { return; }
          if (imgUri.match('_')) {
           var iid = imgUri.split('/').pop().split('_')[1].split('.')[0];
          } else {
            var iid = imgUri.split('/').pop().split('.')[0];
          }

          var sid = 'lexus' + bid + ':' + pagePosition + ':' + iid + ':' + cid;
          sid = sid.replace(/(-:)/g, ":");
          //console.log('sid: ' + sid);

          //$(this).attr('onclick', 'sc(\'' + sid + '\')\; alert("onclick");');
          $(this).attr('onclick', 'sc(\'' + sid + '\')\;');
        });
      }
    }

    function setTabChange (target) {
      var targetLink = $(target).find('a');
      targetLink.each(function () {
        $(this).attr('target', '_blank');
      });
    }

    /////////////////////// TOP //////////////////////////
    var topEvent = function () {

      $('.fCardGroup').find('.fCardGroup_item').eq(0).find('.fCard_body').addClass('is-unit-left');

      var dayEvent = function () {
        var appendCar = $('#top_recommendation_day_car').html();
        var appendBrand = $('#top_recommendation_day_brand-wrap').html();
        $('#top_recommendation_day_car').remove();
        $('#top_recommendation_day_brand-wrap').remove();
        $('#top_recommendation_day_visionary').append(appendCar);
        $('#top_recommendation_day_visionary').append(appendBrand);
      };

      var reviewEvent = function () {
        var appendVisionary = $('#top_recommendation_review_visionary').html();
        var appendBrand = $('#top_recommendation_review_brand-wrap').html();
        $('#top_recommendation_review_visionary').remove();
        $('#top_recommendation_review_brand-wrap').remove();
        $('#top_recommendation_review_car').append(appendVisionary);
        $('#top_recommendation_review_car').append(appendBrand);
      };

      $.when(
        setTabChange('#top_recommendation_day_visionary'),
        setTabChange('#top_recommendation_review_visionary')
      ).done(function(){
        //検討期の場合のみ
        if (userStatus === 'day') { dayEvent(); return; }
        if (userStatus === 'review') { reviewEvent(); }
      });

      //------ おすすめモデル ------//
      var recommendModels = [];
      var modelData = {
        LS : {
          carName : 'LS',
          linkCarName : 'ls',
          modelName : 'LS500h/LS500',
          price : '<strong class="cCard_priceValue">9,800,000</strong>円～',
          modelCode: 'GVF50-AEVQH(E)',
          imagePath: '/common/resource/models/tcv/LS/exterior/GVF50-AEVQH(E)/1J2_90.png'
        },
        GS : {
          carName : 'GS',
          linkCarName : 'gs',
          modelName : 'GS450h/GS350 /<br>GS300h/GS300',
          price : '<strong class="cCard_priceValue">5,770,000</strong>円～',
          ModelCode: 'GWL10-BEXQB(L)',
          imagePath: '/common/resource/models/tcv/GS/exterior/GWL10-BEXQB(L)/1J2_90.png'
        },
        GSF : {
          carName : 'GS F',
          linkCarName : 'gsf',
          modelName : 'GS F',
          price : '<strong class="cCard_priceValue">11,120,000</strong>円～',
          ModelCode: 'URL10-FEZRH',
          imagePath: '/common/resource/models/tcv/GSF/exterior/URL10-FEZRH/1J2_90.png'
        },
        IS : {
          carName : 'IS',
          linkCarName : 'is',
          modelName : 'IS350/IS300h /<br>IS300',
          price : '<strong class="cCard_priceValue">4,700,000</strong>円～',
          ModelCode: 'GSE31-AEZLH(L)',
          imagePath: '/common/resource/models/tcv/IS/exterior/GSE31-AEZLH(L)/1J4_90.png'
        },
        HS : {
          carName : 'HS',
          linkCarName : 'hs',
          modelName : 'HS250h',
          price : '<strong class="cCard_priceValue">4,347,000</strong>円～',
          ModelCode: 'ANF10-AEXVB(L)',
          imagePath: '/common/resource/models/tcv/HS/exterior/ANF10-AEXVB(L)/1J4_90.png'
        },
        LC : {
          carName : 'LC',
          linkCarName : 'lc',
          modelName : 'LC500/LC500h',
          price : '<strong class="cCard_priceValue">13,000,000</strong>円～',
          ModelCode: 'GWZ100-ACVBB(L)',
          imagePath: '/common/resource/models/tcv/LC/exterior/GWZ100-ACVBB(L)/1J2_90.png'
        },
        RC : {
          carName : 'RC',
          linkCarName : 'rc',
          modelName : 'RC350/RC300h/<br>RC300',
          price : '<strong class="cCard_priceValue">5,300,000</strong>円～',
          ModelCode: 'GSC10-RCZLH(L)',
          imagePath: '/common/resource/models/tcv/RC/exterior/GSC10-RCZLH(L)/1J4_90.png'
        },
        RCF : {
          carName : 'RC F',
          linkCarName : 'rcf',
          modelName : 'RC F',
          price : '<strong class="cCard_priceValue">9,824,000</strong>円～',
          ModelCode: 'USC10-FCZRH',
          imagePath: '/common/resource/models/tcv/RCF/exterior/USC10-FCZRH/1J2_90.png'
        },
        CT : {
          carName : 'CT',
          linkCarName : 'ct',
          modelName : 'CT200h',
          price : '<strong class="cCard_priceValue">3,770,000</strong>円～',
          ModelCode: 'ZWA10-AHXBB(L)',
          imagePath: '/common/resource/models/tcv/CT/exterior/ZWA10-AHXBB(L)/1J7_90.png'
        },
        LX : {
          carName : 'LX',
          linkCarName : 'lx',
          modelName : 'LX570',
          price : '<strong class="cCard_priceValue">11,150,000</strong>円～',
          ModelCode: 'URJ201W-GNZGK',
          imagePath: '/common/resource/models/tcv/LX/exterior/URJ201W-GNZGK/1J7_90.png'
        },
        RX : {
          carName : 'RX',
          linkCarName : 'rx',
          modelName : 'RX450hL/RX450h/<br>RX300',
          price : '<strong class="cCard_priceValue">4,972,000</strong>円～',
          ModelCode: 'GYL26W-ARXGB(L)',
          imagePath: '/common/resource/models/tcv/RX/exterior/GYL26W-ARXGB(L)/1J4_90.png'
        },
        NX : {
          carName : 'NX',
          linkCarName : 'nx',
          modelName : 'NX300h/NX300',
          price : '<strong class="cCard_priceValue">4,400,000</strong>円～',
          ModelCode: 'AYZ15-AWXLB(L)',
          imagePath: '/common/resource/models/tcv/NX/exterior/AYZ15-AWXLB(L)/1J4_90.png'
        }
      };

      var recommendModelsEvent = function () {
        var scoreTop = $('#recommendModels').data('model');
        switch (scoreTop) {
          case 'LS' :
            recommendModels = [ 'GS', 'NX', 'RCF' ];
            break;
          case 'GS' :
            recommendModels = [ 'LS', 'NX', 'RCF' ];
            break;
          case 'GSF' :
            recommendModels = [ 'RCF', 'RC', 'LS' ];
            break;
          case 'IS' :
            recommendModels = [ 'GS', 'NX', 'RCF' ];
            break;
          case 'HS' :
            recommendModels = [ 'IS', 'CT', 'NX' ];
            break;
          case 'LC' :
            recommendModels = [ 'RCF', 'GSF', 'LS' ];
            break;
          case 'RC' :
            recommendModels = [ 'RCF', 'NX', 'LS' ];
            break;
          case 'RCF' :
            recommendModels = [ 'GSF', 'RC', 'LS' ];
            break;
          case 'CT' :
            recommendModels = [ 'IS', 'NX', 'RCF' ];
            break;
          case 'LX' :
            recommendModels = [ 'RX', 'NX', 'LS' ];
            break;
          case 'RX' :
            recommendModels = [ 'NX', 'LX', 'GS' ];
            break;
          case 'NX' :
            recommendModels = [ 'RX', 'CT', 'LS' ];
            break;
        }

        var recommendModelsWrap = '<div class="cCard">' +
                                  '<h3 class="cCard_title">RECOMMEND</h3>' +
                                  '<p class="cCard_lead">おすすめモデル</p>' +
                                  '<div class="cCard_slider">' +
                                  '{slider_item}' +
                                  '</div>' +
                                  '</div>';
        var setActionW = $('#top_simulation_history_review').find('.cCard_action').width();

        var modelsList = [];
        for (var i = 0, model = recommendModels.length; i < model; i++) {
          var thisModel = modelData[recommendModels[i]];
          var modelList = '<div class="cCard_sliderItem">' +
                          '<a class="cCard_detail" href="/models/' + thisModel.linkCarName + '/">' +
                          '<img class="cCard_image" src="' + thisModel.imagePath + '" alt="">' +
                          '<div class="cCard_model">' +
                          '<div class="cCard_modelInner u-clearfix">' +
                          '<p class="cCard_modelTitle">' + thisModel.carName + '</p>' +
                          '<p class="cCard_modelText">' + thisModel.modelName + '</p>' +
                          '</div>' +
                          '</div>' +
                          '<p class="cCard_price">' + thisModel.price + '</p>' +
                          '</a>' +
                          '<ul class="cCard_control">' +
                          '<li class="cCard_controlItem">' +
                          '<a class="cCard_controlLink" href="/models/' + thisModel.linkCarName + '/#st-compare">' +
                          '<span class="cCard_controlText">比較</span>' +
                          '</a>' +
                          '</li>' +
                          '<li class="cCard_controlItem cCard_controlItem-action">' +
                          '<a class="cCard_controlLink" href="/request/simulation/#' + thisModel.linkCarName + '">' +
                          '<span class="cCard_controlText">見積りシミュレーション</span>' +
                          '</a>' +
                          '<div class="cCard_action">' +
                          '<button class="cCard_actionButton"></button>' +
                          '<ul class="cCard_actionTooltip sw-tooltip sw-tooltip-white" style="width: 102px;">' +
                          '<li class="sw-tooltip_item">' +
                          '<a class="sw-tooltip_link" href="/request/estimate/?seriesCode=' + thisModel.linkCarName + '"><span class="sw-tooltip_text">見積依頼</span></a>' +
                          '</li>' +
                          '<li class="sw-tooltip_item">' +
                          '<a class="sw-tooltip_link" href="/request/consult/?seriesCode=' + thisModel.linkCarName + '"><span class="sw-tooltip_text">ご購入相談</span></a>' +
                          '</li>' +
                          '<li class="sw-tooltip_item">' +
                          '<a class="sw-tooltip_link" href="/request/catalog/service/senderselect?seriesCode=' + thisModel.linkCarName + '"><span class="sw-tooltip_text">カタログ請求</span></a>' +
                          '</li>' +
                          '<li class="sw-tooltip_item">' +
                          '<a class="sw-tooltip_link" href="/request/trial/service/dealerselect?seriesCode=' + thisModel.linkCarName + '"><span class="sw-tooltip_text">試乗予約</span></a>' +
                          '</li>' +
                          '</ul>' +
                          '</div>' +
                          '</li>' +
                          '</ul>' +
                          '</div>';
          modelsList[i] = modelList;
        }
        modelsList = modelsList.join('');
        recommendModelsWrap = recommendModelsWrap.replace('{slider_item}', modelsList);
        $('#top_model_recommend_review').append(recommendModelsWrap);

        //------ お近くの販売店を探す ------//
        var dealerSearchHtml = '<div class="cCard">' +
                               '<h3 class="cCard_title">FIND A DEALER</h3>' +
                               '<p class="cCard_lead">販売店を探す</p>' +
                               '<div class="cCard_dealer">' +
                               '<figure class="cCard_dealerIcon"></figure>' +
                               '<p class="cCard_dealerText">販売店を、住所や郵便番号、<br>地図などから検索することができます。</p>' +
                               '<span class="cCard_dealerButton">' +
                               '<a class="cCard_dealerButtonText" href="/dealership/" onclick="sc(\'lexus:top:conversion-top_top_find_dealer_review:dealership\');">郵便番号・住所から検索</a>' +
                               '</span>' +
                               '</div>' +
                               '</div>';

        var dealerSearch = $('#top_find_dealer_review').html();

        if (dealerSearch === '') {
          $('#top_find_dealer_review').append(dealerSearchHtml);
        } else {
          var dealerCode = $('#dealerCode').data('dealercode');
          var dealerData = {};

          var selectedDealer = function (self) {
            selfCode = self.dealerCode;
            selfName = self.dealerName;
            imgPath = '/dealership/images/' + selfCode + '/' + selfCode + '-img_main_01.jpg';
            streetAddress = self.StreetAddress;
            phoneNumber = self.phoneNumber;

            dealerSearchHtml = '<div class="cCard">' +
                               '<h3 class="cCard_title">FIND A DEALER</h3>' +
                               '<p class="cCard_lead">販売店を探す</p>' +
                               '<div class="cCard_dealer selected-dealer">' +
                               '<a id="dealerImg" href="/lexus-dealer/dc/info/' + selfCode + '" onclick="sc(\'lexus:top:conversion-top_top_find_dealer_review:lexus-dealer-' + selfCode + '\');">' +
                               '<figure class="cCard_dealerIcon"><img src="' + imgPath + '" alt="" onerror="this.remove();"></figure>' +
                               '<p class="cCard_dealer_Title">' + selfName + '</p>' +
                               '<p class="cCard_dealer_Text">' + streetAddress + '<br>' + phoneNumber + '</p>' +
                               '</a>' +
                               '<span class="cCard_dealerButton">' +
                               '<a class="cCard_dealerButtonText" href="/dealership/" onclick="sc(\'lexus:top:conversion-top_top_find_dealer_review:dealership\');">郵便番号・住所から検索</a>' +
                               '</span>' +
                               '</div>' +
                               '</div>';

            $('#top_find_dealer_review').append(dealerSearchHtml);
          };

          var embedDealer = function (dealerData) {
            for (var i = 0, len = dealerData.dealers.length; i < len; i++) {
              self = dealerData.dealers[i];
              if (self.dealerCode == dealerCode) { selectedDealer(self); break; }
            }
          };

          $.ajax({
            type: "GET",
            url: '/common/resource/dealer/json/dealer.json',
            dataType: 'json',
            cache: false,
            success: function(data) {
              dealerData = data;
              embedDealer(dealerData);
            },
            error:function(){ $('#top_find_dealer_review').append(dealerSearchHtml); }
          });
        }

        //------ シミュレーション履歴 ------//
        var simulationHistory = $('#top_simulation_history_review').html();
        if (simulationHistory === '') {
          var modelHistory = $('#top_model_history_review').html();
          var simulationHistoryHtml = modelHistory.replace('HISTORY','SIMULATION').replace('最近閲覧したモデル','シミュレーション履歴');
          $('#top_simulation_history_review').append(simulationHistoryHtml);
        }

        // top conversion
        var mq = APP.module.media;

        var $main = $('.conversion_mainInner');
        var $item = $('.conversion_item');
        var $cardSlider = $('.cCard_slider');
        var $cardSliderItem = $('.cCard_sliderItem');

        if (mq.currentType === 'PC') { init4PC(); }
        if (mq.currentType === 'SP') { init4SP(); }
        mq.on('PC', init4PC);
        mq.on('SP', init4SP);

        function init4PC() {
          $main.off('click.cardAction');

          if (mq.prevType === 'SP') {
            $main.slick('unslick');
            $main.off('swipe.slider');
          }

          $cardSlider.slick({
            infinite: false
          });
        }

        function init4SP() {
          $main.on('click.cardAction', '.cCard_actionButton', onClickCardAction);

          if (mq.prevType === 'PC') {
            $cardSlider.slick('unslick');
          }

          $main.slick({
            dots: true,
            appendDots: $('.conversion_dots'),
            arrows: false,
            infinite: false
          });
          $main.on('swipe.slider', function(event, slick, direction) {
            $item.removeClass('is-open');
          });
        }

        function onClickCardAction() {
          var $target = $(this);
          var $parent = $target.closest('.conversion_item');

          if ($parent.hasClass('is-open') === true) {
            $parent.removeClass('is-open');
          } else {
            $parent.addClass('is-open');
          }
        }

        addClickCountConversion();

        //conversion click count
        function addClickCountConversion () {
          var $targetWrap = $('.conversion');
          $targetWrap.find('a').each(function () {
            var linkUrl,
                linkModel,
                clickCount,
                pid = $(this).parents('.conversion_item').attr('id'),
                targetHref = $(this).attr('href');

            //href models
            if ($(this).hasClass('cCard_detail')) {
              linkUrl = targetHref.split('/');
              clickCount = 'sc(\'' + 'lexus:top:conversion-' + pid + ':' + linkUrl[linkUrl.length - 2] + '\')\;';
            }

            //href compare
            if (targetHref.match(/st-compare/)) {
              linkModel = targetHref.split('/');
              linkUrl = targetHref.split('/').pop().split('#').pop();
              clickCount = 'sc(\'' + 'lexus:top:conversion-' + pid + ':' + linkModel[1] + '-' + linkUrl + '\')\;';
            }

            //href simulation
            if (targetHref.match(/simulation/) && pid !== 'top_simulation_history_review') {
              linkUrl = targetHref.replace(/[\/]/g, '-').replace('#','-');
              clickCount = 'sc(\'' + 'lexus:top:conversion-' + pid + ':' + linkUrl + '\')\;';
            } else if (targetHref.match(/simulation/) && pid === 'top_simulation_history_review') {
              var linkBox = [];
              linkModel = targetHref.split('/').pop().split('&model=').pop();
              linkUrl = targetHref.split('/');
              for (var i, len = linkUrl.length; i < len; i++) {
                len = linkUrl.length;
                if (len - 1 !== i) { linkBox[i] = $(this); }
              };
              var setClickCountSimulation = linkBox.join('-');
              clickCount = 'sc(\'' + 'lexus:top:conversion-' + pid + ':' + setClickCountSimulation + linkModel + '\')\;';
            }

            //href estimate consult
            if (targetHref.match(/estimate/) || targetHref.match(/consult/)) {
              linkUrl = targetHref.replace(/[\/]/g, '-').replace('?seriesCode=','');
              clickCount = 'sc(\'' + 'lexus:top:conversion-' + pid + ':' + linkUrl + '\')\;';
            }

            //href catalog trial
            if (targetHref.match(/catalog/) || targetHref.match(/trial/)) {
              linkUrl = targetHref.replace(/[\/]/g, '-').replace('?seriesCode=','-');
              clickCount = 'sc(\'' + 'lexus:top:conversion-' + pid + ':' + linkUrl + '\')\;';
            }

            if(typeof clickCount !== 'undefined' && clickCount.indexOf('#') != -1) {
              clickCount = clickCount.replace('#', '');
            }

            $(this).attr('onClick', clickCount);
          });
        }
      };

      //おすすめモデル
      var checkRecommenModels = function() {
          var hasdata = $('#recommendModels').data('model');
          if (typeof hasdata !== 'undefined') {
              recommendModelsEvent();
              return;
          }
          window.setTimeout(checkRecommenModels, 200);
      };
      // setTimeout
      window.setTimeout(checkRecommenModels, 200);

    };

    /////////////////////// MODELS //////////////////////////
    var modelsEvent = function () {

      $('.fCardGroup').find('.fCardGroup_item').eq(2).find('.fCard_body').addClass('is-unit-right');

      var reviewEvent = function () {
        var appendRequest = $('#models_request').html();
        $('#models_request').remove();
        $('#models_recommendation_review_car').append(appendRequest);
      };

      //検討期の場合のみ
      if (userStatus === 'day') { return; }
      if (userStatus === 'review') { reviewEvent(); }
    };

    /////////////////////// CALL EVENT //////////////////////////
    if (branchPath === 'top') { topEvent();}
    if (branchPath === 'models') { modelsEvent(); }
    if (branchPath !== 'visionary') {
      commonEvent();
    } else {
      var checkRecommenVisionary = function() {
          var hasdata = $('#recommendation').find('li').find('a').data('category');
          if (typeof hasdata !== 'undefined') {
              visionaryEvent();
              return;
          }
          window.setTimeout(checkRecommenVisionary, 200);
      };
      // setTimeout
      window.setTimeout(checkRecommenVisionary, 200);
    }

    /////////////////////// Visionary以外共通 //////////////////////////

    function commonEvent () {
      // card
      var mq = APP.module.media;
      var unit = APP.module.unit;

      // jQuery object
      var $win = $(window);
      var $fCardTitle = $('.fCard_title');
      var $fCardBody = $('.fCard_body');
      var $bCardBody = $('.bCard_body');
      var $nCard = $('.nCard');
      var $relationItem = $('.relation_item');

      var scrollController;

      if (mq.currentType === 'PC') { init4PC(); }
      if (mq.currentType === 'SP') { init4SP(); }
      mq.on('PC', init4PC);
      mq.on('SP', init4SP);

      itemHeightSet();

      function init4PC() {
        $win.off('resize.cardGroup');
        //unit.destroy($fCardTitle, {alignHeight: true});
        unit.destroy($fCardBody, {alignHeight: true});
        unit.destroy($bCardBody, {alignHeight: true});
        unit.destroy($nCard, {alignHeight: true});
        unit.destroy($relationItem, {alignHeight: true});
        //unit.init($fCardTitle, 3, {alignHeight: true});
        unit.init($fCardBody, 3, {alignHeight: true});
        unit.init($bCardBody, 4, {alignHeight: true});
        unit.init($nCard, 4, {alignHeight: true});
        unit.init($relationItem, 4, {alignHeight: true});
        initScrollMotion();
      }

      function init4SP() {
        init4SPUnit();

        $win.on('resize.cardGroup', _.debounce(onResizeCardOfSP, 100));

        if (mq.prevType === 'PC') {
          scrollController.destroy();
        }
        TweenMax.set($('.fCardGroup_item'), {opacity: 1, y: 0});
      }

      function init4SPUnit() {
        //unit.destroy($fCardTitle, {alignHeight: true});
        unit.destroy($fCardBody, {alignHeight: true});
        unit.destroy($bCardBody, {alignHeight: true});
        unit.destroy($nCard, {alignHeight: true});
        unit.destroy($relationItem, {alignHeight: true});
        //unit.init($fCardTitle, 2, {alignHeight: true});
        unit.init($fCardBody, 2, {alignHeight: true});
        unit.init($bCardBody, 2, {alignHeight: true});
        unit.init($nCard, 2, {alignHeight: true});
        unit.init($relationItem, 2, {alignHeight: true});
      }

      function onResizeCardOfSP() {
        if (mq.currentType === 'PC') { return; }
        init4SPUnit();
      }

      function initScrollMotion() {
        scrollController = new ScrollMagic.Controller();

        var $fCardItem = $('.fCardGroup_item');
        TweenMax.set($fCardItem, {opacity: 0, y: 16.5});

        $fCardItem.each(function() {
          var idx = $fCardItem.index(this) % 3;
          var delay = 0.11 * idx;
          var tween = TweenMax.to(this, 0.3, {opacity: 1, y: 0, delay: delay});
          new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 'onEnter',
            offset: 200,
            reverse:false
          })
            .setTween(tween)
            .addTo(scrollController);
        });
      }

      function itemHeightSet(target) {
        var itemHeights = [];
        var $fCard = $('.fCardGroup_item');

        function keepHeight ($item) {
          $item.css({height: 'auto'});
          $item.each(function (i) {
            itemHeights[i] = $(this).height();
          });
        }

        function fixHeight ($item) {
          keepHeight($item);
          var maxHeight = 0;
          $item.each(function (i) {
            $(this).height(itemHeights[i]);

            if (i % 2 == 0) {
              maxHeight = $(this).height();
            } else {
              maxHeight = ($(this).height() > maxHeight) ? maxHeight = $(this).height() : maxHeight = maxHeight;
              $item.eq(i).height(maxHeight);
              $item.eq(i - 1).height(maxHeight);
              maxHeight = 0;
            }
          });
        }

        $win.on('load', function() { fixHeight($fCard); });
        $win.on('resize', function () { fixHeight($fCard); });
      };
    }

    /////////////////////// Visionary //////////////////////////
    function visionaryEvent () {

      setTabChange('#visionary_recommendation_day_brand-wrap');
      setTabChange('#visionary_recommendation_review_request-wrap');
      setTabChange('#visionary_recommendation_review_car');

      var recommendContent = $('#recommendation');
      var rankingContent = $('#recommendation-ranking');

      if (userStatus === 'day') { dayEvent(); }
      if (userStatus === 'review') { reviewEvent(); }

      function dayEvent () {
        var appendBrand = $('#visionary_recommendation_day_brand-wrap').html();
        $('#visionary_recommendation_day_brand-wrap').remove();
        $('#visionary_recommendation_day_visionary').append(appendBrand);
        setRecommendSlider();
      }

      function reviewEvent () {
        var appendRequest = $('#visionary_recommendation_review_request-wrap').html();
        $('#visionary_recommendation_review_request-wrap').remove();
        $('#visionary_recommendation_review_car').append(appendRequest);
        setRecommendSlider();
      }

      //ランキング付け
      rankingContent.find('li').each(function (i) {
        $(this).find('.vsw-rankingCard_rankInner').text(i+1);
      });

      ////////////////////////// recommend //////////////////////////
      function setRecommendSlider () {
        var mq = VISIONARY.module.media;
        var unit = VISIONARY.module.unit;
        var util = VISIONARY.util;

        var $win = $(window);
        var $section = $('#recommendedSection');
        var $list = $section.find('.vsw-articleCardGroup_list');
        var $item = $list.find('.vsw-articleCardGroup_item');

        init();

        function init() {
          if (mq.currentType === 'PC') { init4PC(); }
          if (mq.currentType === 'SP') { init4SP(); }
          mq.on('PC', init4PC);
          mq.on('SP', init4SP);
          emptyRanking();
        }

        function init4PC() {
          if (mq.prevType === 'SP') {
            $list.slick('unslick');
          }
          unit.destroy($item, {alignHeight: true});
          unit.init($item, 3, {alignHeight: true});
        }

        function init4SP() {
          init4SPUnit();
          $win.on('resize.cardGroup', _.debounce(onResizeCardOfSP, 100));
          $list.slick({
            dots: false,
            arrows: false,
            infinite: false,
            centerMode: true,
            centerPadding: '32px'
          });
        }

        function init4SPUnit() {
          unit.destroy($item, {alignHeight: true});
          unit.init($item, $item.length, {alignHeight: true});
        }

        function emptyRanking () {
          var $rItem = $('.vsw-rankingCardGroup_item');
          var wrap =  '<a class="vsw-rankingCard" data-category="${アイテムカテゴリ2}" href="${リンク先URL}">' +
                      '<div class="vsw-rankingCard_rank">' +
                      '<div class="vsw-rankingCard_rankInner"></div>' +
                      '</div>' +
                      '<div class="vsw-rankingCard_image">' +
                      '<div class="vsw-rankingCard_imageInner">' +
                      '<img src="${画像パス}" alt="">' +
                      '<div class="vsw-rankingCard_imageCover"></div>' +
                      '</div>' +
                      '</div>' +
                      '<div class="vsw-rankingCard_detail">' +
                      '<div class="vsw-rankingCard_title">${テキスト}</div>' +
                      '<div class="vsw-rankingCard_info">' +
                      '<span class="vsw-rankingCard_category">${アイテムカテゴリ2}</span>' +
                      '<div class="vsw-rankingCard_date">${公開日}</div>' +
                      '</div>' +
                      '</div>' +
                      '</a>';
          var setItem = [
            {
              category : 'CAR',
              link : '/magazine/20170517/30/car_sawa.html',
              imagePath: '/magazine/images/recommendation/2017051730.jpg',
              txt : 'LEXUS INTERNATIONAL President 澤良宏氏が語るLEXUSの未来の形',
              release : '2017.05.17 WED'
            },
            {
              category : 'CULTURE',
              link : '/magazine/20170517/6/cul_portland.html',
              imagePath: '/magazine/images/recommendation/2017051706.jpg',
              txt : 'リバースプロジェクトに見る、ポストポートランド文化の芽生え',
              release : '2017.05.17 WED'
            },
            {
              category : 'CRAFTSMANSHIP',
              link : '/magazine/20170517/24/cra_bicycle.html',
              imagePath: '/magazine/images/recommendation/2017051724.jpg',
              txt : 'テイラーメイドの高級自転車を手掛けるパリの小さなメーカー',
              release : '2017.05.17 WED'
            }
          ];

          $rItem.each(function (i) {
            if ($(this).html() === '') {
              var content = wrap.replace(/\${アイテムカテゴリ2}/g, setItem[i].category).replace('${リンク先URL}', setItem[i].link).replace('${画像パス}', setItem[i].imagePath).replace('${テキスト}', setItem[i].txt).replace('${公開日}', setItem[i].release);
              $(this).append(content);
            }
          });
        }

        function onResizeCardOfSP() {
          if (mq.currentType === 'PC') { return; }
          init4SPUnit();
        }
      }

      ////////////////////////// //recommend //////////////////////////
    }

    callbackFlag = true;
  };//rcmCallback End

  //local debug
  //rcmCallback();

  Rtoaster.callback = rcmCallback;
  Rtoaster.recommendNow.apply(window, recommendTarget);

  //debug
  /*if (   domein !== 'lexus.jp'
      || domein !== 'stg.lexus.jp'
      || domein !== 'lexusjp.webrev.jp')
  {
      var localDebugTop = '<div id="features" class="sw-section"><div class="sw-section_head"><span class="sw-section_headInner"><h2 class="sw-section_title">FEATURES</h2><p class="sw-section_lead">特集／ピックアップ</p></span></div><div class="sw-section_body localCallbacks"><div id="top_recommendation_review_car" class="fCardGroup moreCard" style="display: block;"><div id="top_recommendation_review_car_1" class="fCardGroup_item fCard firstItem" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><a class="fCardGroup_link" href="/models/is/gallery/" target="" onclick="sc(\'lexus:top:recommend:053:-models-is-gallery-\');"><div class="fCard_image"><img src="/common/v3/images/recommendation/rcm_053.jpg" alt="IS"><div class="fCard_cover"></div></div><div class="fCard_body s-unit is-unit-top is-unit-right"><h3 class="fCard_title"><span class="fCard_titleText">ISの写真と動画をご覧いただけるギャラリーページ</span></h3><p class="fCard_meta"><span class="fCard_category">IS</span></p></div></a></div><div id="top_recommendation_review_car_2" class="fCardGroup_item fCard" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><a class="fCardGroup_link" href="/models/is/features/navigation_and_audio/" target="" onclick="sc(\'lexus:top:recommend:063:-models-is-features-navigation_and_audio-\');"><div class="fCard_image"><img src="/common/v3/images/recommendation/rcm_063.jpg" alt="IS"><div class="fCard_cover"></div></div><div class="fCard_body s-unit is-unit-top is-unit-right"><h3 class="fCard_title"><span class="fCard_titleText">走る愉しさをさらに広げるISのサウンド＆ナビゲーションシステム</span></h3><p class="fCard_meta"><span class="fCard_category">IS</span></p></div></a></div><div id="top_recommendation_review_car_3" class="fCardGroup_item fCard" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><a class="fCardGroup_link" href="/models/is/features/the_art_of_interior/" target="" onclick="sc(\'lexus:top:recommend:055:-models-is-features-the_art_of_interior-\');"><div class="fCard_image"><img src="/common/v3/images/recommendation/rcm_055.jpg" alt="IS"><div class="fCard_cover"></div></div><div class="fCard_body s-unit is-unit-top is-unit-right"><h3 class="fCard_title"><span class="fCard_titleText">走りへの想いを掻き立て、非日常へと誘うISのインテリア</span></h3><p class="fCard_meta"><span class="fCard_category">IS</span></p></div></a></div><div id="top_recommendation_review_visionary_1" class="fCardGroup_item fCard firstItem" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><a class="fCardGroup_link" href="/magazine/20170602/35/cul_saito_02.html" target="_blank" onclick="sc(\'lexus:top:recommend:2017060235:-magazine-20170602-35-cul_saito_02.html\');"><div class="fCard_image"><img src="/magazine/images/recommendation/2017060235.jpg" alt="至近距離の現代"><div class="fCard_cover"></div></div><div class="fCard_body s-unit is-unit-top is-unit-right"><h3 class="fCard_title"><span class="fCard_titleText">至近距離の現代</span></h3><p class="fCard_meta"><span class="fCard_category">CULTURE</span></p></div></a></div><div id="top_recommendation_review_visionary_2" class="fCardGroup_item fCard" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><a class="fCardGroup_link" href="/magazine/20170517/15/cul_little_inventors.html" target="_blank" onclick="sc(\'lexus:top:recommend:2017051715:-magazine-20170517-15-cul_little_inventors.html\');"><div class="fCard_image"><img src="/magazine/images/recommendation/2017051715.jpg" alt="イギリス発、子ども発明家を育てる「Little Inventors!」"><div class="fCard_cover"></div></div><div class="fCard_body s-unit is-unit-top is-unit-right"><h3 class="fCard_title"><span class="fCard_titleText">イギリス発、子ども発明家を育てる「Little Inventors!」</span></h3><p class="fCard_meta"><span class="fCard_category">CULTURE</span></p></div></a></div><div id="top_recommendation_review_brand" class="fCardGroup_item fCard" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><a class="fCardGroup_link" href="/brand/lexus-design/design_events/" target="" onclick="sc(\'lexus:top:recommend:216:-brand-lexus-design-design_events-\');"><div class="fCard_image"><img src="/common/v3/images/recommendation/rcm_216.jpg" alt="LEXUS DESIGN EVENT"><div class="fCard_cover"></div></div><div class="fCard_body s-unit is-unit-top is-unit-right"><h3 class="fCard_title"><span class="fCard_titleText">“DESIGN”の可能性を発信するデザインイベント</span></h3><p class="fCard_meta"><span class="fCard_category">LEXUS DESIGN EVENT</span></p></div></a></div></div><div class="moreIcon u-sp"></div></div></div>';
      localSet('#recommendation',localDebugTop);

      var localDebugTopConversion = '<div class="conversion_main"><div class="conversion_mainInner"><div id="top_model_history_review" class="conversion_item conversion_item-models"><div class="cCard"> <h3 class="cCard_title">HISTORY</h3> <p class="cCard_lead">最近閲覧したモデル</p> <a class="cCard_detail" href="/models/lc/" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_model_history_review:lc\');"> <img class="cCard_image" src="/common/resource/models/tcv/LC/exterior/GWZ100-ACVBB(L)/1J2_90.png" alt="LC"> <div class="cCard_model"> <div class="cCard_modelInner u-clearfix"> <p class="cCard_modelTitle">LC</p> <p class="cCard_modelText">LC500 / LC500h</p> </div> </div> <p class="cCard_price"><strong class="cCard_priceValue">13,000,000</strong>円～</p> </a> <ul class="cCard_control"> <li class="cCard_controlItem"> <a class="cCard_controlLink" href="/models/lc/#st-compare" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_model_history_review:models-st-compare\');"> <span class="cCard_controlText">比較</span> </a> </li> <li class="cCard_controlItem cCard_controlItem-action"> <a class="cCard_controlLink" href="/request/simulation/#LC" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_model_history_review:-request-simulation--LC\');"> <span class="cCard_controlText">見積りシミュレーション</span> </a> <div class="cCard_action"> <button class="cCard_actionButton" tabindex="-1"></button> <ul class="cCard_actionTooltip sw-tooltip sw-tooltip-white" style="width: 102px;"> <li class="sw-tooltip_item"> <a class="sw-tooltip_link" href="/request/estimate/?seriesCode=LC" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_model_history_review:-request-estimate-LC\');"><span class="sw-tooltip_text">見積依頼</span></a> </li> <li class="sw-tooltip_item"> <a class="sw-tooltip_link" href="/request/consult/?seriesCode=LC" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_model_history_review:-request-consult-LC\');"><span class="sw-tooltip_text">ご購入相談</span></a> </li> <li class="sw-tooltip_item"> <a class="sw-tooltip_link" href="/request/catalog/service/senderselect?seriesCode=LC" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_model_history_review:-request-catalog-service-senderselect-LC\');"><span class="sw-tooltip_text">カタログ請求</span></a> </li> <li class="sw-tooltip_item"> <a class="sw-tooltip_link" href="/request/trial/service/dealerselect?seriesCode=LC" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_model_history_review:-request-trial-service-dealerselect-LC\');"><span class="sw-tooltip_text">試乗予約</span></a> </li> </ul> </div> </li> </ul> </div></div><div id="top_model_recommend_review" class="conversion_item conversion_item-models"><div id="recommendModels" data-model="IS"> IS </div><div class="cCard"><h3 class="cCard_title">RECOMMEND</h3><p class="cCard_lead">おすすめモデル</p><div class="cCard_slider slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow slick-disabled" aria-label="Previous" role="button" aria-disabled="true" style="">Previous</button><div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 1023px; transform: translate3d(0px, 0px, 0px);"><div class="cCard_sliderItem slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide10" style="width: 341px;"><a class="cCard_detail" href="/models/gs/" tabindex="0" onclick="sc(\'lexus:top:conversion-top_model_recommend_review:gs\');"><img class="cCard_image" src="/common/resource/models/tcv/GS/exterior/GWL10-BEXQB(L)/1J2_90.png" alt=""><div class="cCard_model"><div class="cCard_modelInner u-clearfix"><p class="cCard_modelTitle">GS</p><p class="cCard_modelText">GS450h/GS350 /<br>GS300h/GS200t</p></div></div><p class="cCard_price"><strong class="cCard_priceValue">5,770,000</strong>円～</p></a><ul class="cCard_control"><li class="cCard_controlItem"><a class="cCard_controlLink" href="/models/gs/#st-compare" tabindex="0" onclick="sc(\'lexus:top:conversion-top_model_recommend_review:models-st-compare\');"><span class="cCard_controlText">比較</span></a></li><li class="cCard_controlItem cCard_controlItem-action"><a class="cCard_controlLink" href="/request/simulation/#gs" tabindex="0" onclick="sc(\'lexus:top:conversion-top_model_recommend_review:-request-simulation--gs\');"><span class="cCard_controlText">見積りシミュレーション</span></a><div class="cCard_action"><button class="cCard_actionButton" tabindex="0"></button><ul class="cCard_actionTooltip sw-tooltip sw-tooltip-white" style="width: 102px;"><li class="sw-tooltip_item"><a class="sw-tooltip_link" href="/request/estimate/?seriesCode=gs" tabindex="0" onclick="sc(\'lexus:top:conversion-top_model_recommend_review:-request-estimate-gs\');"><span class="sw-tooltip_text">見積依頼</span></a></li><li class="sw-tooltip_item"><a class="sw-tooltip_link" href="/request/consult/?seriesCode=gs" tabindex="0" onclick="sc(\'lexus:top:conversion-top_model_recommend_review:-request-consult-gs\');"><span class="sw-tooltip_text">ご購入相談</span></a></li><li class="sw-tooltip_item"><a class="sw-tooltip_link" href="/request/catalog/service/senderselect?seriesCode=gs" tabindex="0" onclick="sc(\'lexus:top:conversion-top_model_recommend_review:-request-catalog-service-senderselect-gs\');"><span class="sw-tooltip_text">カタログ請求</span></a></li><li class="sw-tooltip_item"><a class="sw-tooltip_link" href="/request/trial/service/dealerselect?seriesCode=gs" tabindex="0" onclick="sc(\'lexus:top:conversion-top_model_recommend_review:-request-trial-service-dealerselect-gs\');"><span class="sw-tooltip_text">試乗予約</span></a></li></ul></div></li></ul></div></div></div><button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="" aria-disabled="false">Next</button></div></div></div><div id="top_simulation_history_review" class="conversion_item conversion_item-models"><div class="cCard"> <h3 class="cCard_title">SIMULATION</h3> <p class="cCard_lead">シミュレーション履歴</p>  <a class="cCard_detail" href="/models/ls/" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_simulation_history_review:ls\');"> <img class="cCard_image" src="/common/resource/models/tcv/LS/exterior/UVF45-AEXQH(F)/1J2_90.png" alt="LS"> <div class="cCard_model"> <div class="cCard_modelInner u-clearfix"> <p class="cCard_modelTitle">LS</p> <p class="cCard_modelText cCard_modelTextSimulation">LS600h “F SPORT”</p> </div> </div> <p class="cCard_price"><strong class="cCard_priceValue">12,722,000</strong>円～</p> </a>  <ul class="cCard_control"> <li class="cCard_controlItem"> <a class="cCard_controlLink" href="/models/ls#st-compare" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_simulation_history_review:models-st-compare\');"> <span class="cCard_controlText">比較</span> </a> </li> <li class="cCard_controlItem cCard_controlItem-action"> <a class="cCard_controlLink" href="/request/simulation/step2/LS_L-Select_03.html?series=LS&amp;model=UVF45-AEXQH(F)" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_simulation_history_review:UVF45-AEXQH(F)\');"> <span class="cCard_controlText">見積りシミュレーション</span> </a> <div class="cCard_action"> <button class="cCard_actionButton" tabindex="-1"></button> <ul class="cCard_actionTooltip sw-tooltip sw-tooltip-white" style="width: 102px;"> <li class="sw-tooltip_item"> <a class="sw-tooltip_link" href="/request/estimate/?seriesCode=LS" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_simulation_history_review:-request-estimate-LS\');"> <span class="sw-tooltip_text">見積依頼</span></a> </li> <li class="sw-tooltip_item"> <a class="sw-tooltip_link" href="/request/consult/?seriesCode=LS" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_simulation_history_review:-request-consult-LS\');"> <span class="sw-tooltip_text">ご購入相談</span> </a> </li> <li class="sw-tooltip_item"> <a class="sw-tooltip_link" href="/request/catalog/service/senderselect?seriesCode=LS" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_simulation_history_review:-request-catalog-service-senderselect-LS\');"> <span class="sw-tooltip_text">カタログ請求</span> </a> </li> <li class="sw-tooltip_item"> <a class="sw-tooltip_link" href="/request/trial/service/dealerselect?seriesCode=LS" tabindex="-1" onclick="sc(\'lexus:top:conversion-top_simulation_history_review:-request-trial-service-dealerselect-LS\');"> <span class="sw-tooltip_text">試乗予約</span> </a> </li> </ul> </div> </li> </ul> </div></div></div><div class="conversion_dots"></div></div><div class="conversion_aside"><div id="top_find_dealer_review" class="conversion_item conversion_item-dealer"><div id="dealerCode" data-dealercode="21551">21551</div><div class="cCard"><h3 class="cCard_title">FIND A DEALER</h3><p class="cCard_lead">お近くの販売店を探す</p><div class="cCard_dealer selected-dealer"><a id="dealerImg" href="/lexus-dealer/dc/info/21551" onclick="sc(\'lexus:top:conversion-top_top_find_dealer_review:lexus-dealer-21551\');"><figure class="cCard_dealerIcon"><img src="/dealership/images/21551/21551-img_main_01.jpg" alt="" onerror="this.remove();"></figure><p class="cCard_dealer_Title">レクサス帯広</p><p class="cCard_dealer_Text">北海道河東郡音更町木野大通東14丁目2-1<br>0155-30-3055</p></a><span class="cCard_dealerButton"><a class="cCard_dealerButtonText" href="/dealership/" onclick="sc(\'lexus:top:conversion-top_top_find_dealer_review:dealership\');">郵便番号・住所から検索</a></span></div></div></div></div>';  localSet('.conversion',localDebugTopConversion);
      localSet('.conversion',localDebugTopConversion);

      function localSet(target, appendElem) {
        $(target).empty();
        $(target).append(appendElem);
      }
  }*/

})(window.jQuery, window.Rtoaster, _, APP, VISIONARY);
