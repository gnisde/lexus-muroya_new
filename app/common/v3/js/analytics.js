/**
 * analytics.js
 */

var startAnalytics = function () {
  // Site Catalyst
  (function() {
    if (typeof(clickCount) == "undefined") {
      var j = document.getElementsByTagName('script')[0];
      var s = document.createElement('script');
      s.src = '/common/v2/js/s_code.js';
      if (window.ignoreAdobeTagManager !== true) j.parentNode.insertBefore(s, j);
      s = document.createElement('script');
      s.src = '/common/v2/js/sc_click.js';
      j.parentNode.insertBefore(s, j);
    } else { 
      var j = document.getElementsByTagName('script')[0];
      var s = document.createElement('script');
      s.src = '/common/v2/js/s_code.js';
      if (window.ignoreAdobeTagManager !== true) j.parentNode.insertBefore(s, j);
    }
  }());

  // Site Catalyst Site Track
  var st = (function() {
      var
      siteTrack = function(pageName) {
          var s_sc_cd = window.s_sc_cd;
          if (!s_sc_cd) return 's_sc_cd is undefined';
          if (!s_sc_cd.siteID) return 's_sc_cd.siteID is undefined';
          if (!pageName) return 'pagename is undefined';

          var param = {
              "pageName": s_sc_cd.siteID + ':' + pageName
          };
          return s_sc_cd.t(param);
      };
      return siteTrack;
  }());


  try {
      //eco-tag cdn
      (function() {
          var e = document.createElement('script');
          e.type = 'text/javascript';
          e.src = (document.location.protocol=='https:' ? 'https://www.eco-tag.jp' : 'http://cf.eco-tag.jp') 
              + '/perfx/pix-toyotalexus-min.js';
          (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(e);
      })();
  } catch(err) {}


  // Rtoaster
  (function() {
      var s = document.createElement('script');
      s.type = 'text/javascript';
      s.src = '//rt.rtoaster.jp/Rtoaster.js';
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(s);

      // init
      var init = function() {
          var location = window.location;
          var Rtoaster = window.Rtoaster;
          var pathname = location.pathname;

          //debug
          //console.log(Rtoaster);
          //console.log(pathname);

          var _rtaid = '';
          if (location.hostname === 'lexus.jp') {
              _rtaid = "RTA-3c42-7e68a66f26d2";
          } else if (location.hostname === 'stg.lexus.jp') {
              _rtaid = "RTA-3c42-7e6883cc9b07";
          } else if (location.hostname === 'lexusjp.webrev.jp') {
              _rtaid = "RTA-3c42-7e686cbe55d1";
          }

          //会員IDに書き換え
          if (twpId !== '') {
              Rtoaster.init(_rtaid, twpId);
          } else {
              Rtoaster.init(_rtaid);
          }
          
          Rtoaster.item(pathname);
          
          Rtoaster.track(window.appKey);

          // append
          if (document.getElementById('recommendation')) {
              var s = document.createElement('script');
              s.type = 'text/javascript';
              s.src = '/common/v3/js/recommendation.js';
              (document.getElementsByTagName('body')[0]).appendChild(s);
          }

          if (document.getElementById('rtoaster-switch')) {
              var s = document.createElement('script');
              s.type = 'text/javascript';
              s.src = '/brand/motorsport/js/rtoaster-switch.js';
              (document.getElementsByTagName('body')[0]).appendChild(s);
          }
      };
      // check
      var checkRtoaster = function() {
          var hasRtoaster = !!window.Rtoaster,
              hasAPP = !!window.APP;
              hasVisionary = !!window.VISIONARY;
          
          if (hasRtoaster && window.APP || hasRtoaster && hasVisionary) {
              init();
              return;
          }
          window.setTimeout(checkRtoaster, 500);
      };
      // setTimeout
      window.setTimeout(checkRtoaster, 500);
  }());


  // WebAntenna
  (function() {
      var pathname = window.location.pathname;
      if (pathname.search(/^\/folw\/simulation\/presentationsimurationresult\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/catalog\/agreetotheterms\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/catalog\/selectdealerrequest\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/catalog\/setuserinforegisted\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/catalog\/requestcatalog\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/catalog\/makersetuserinfo\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/catalog\/makerselectcarseries\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/catalog\/makerrequestcatalog\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/trial\/setuserrequest\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/trial\/requesttestdrive\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/shop\/dealerinfo\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/dealercontact\/agreetotheterms\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/dealercontact\/selectcarseries\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/dealercontact\/setuserrequest\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/dealercontact\/requestcontact\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/estimate\/agreetotheterms\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/estimate\/selectcarseries\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/estimate\/setuserrequest\.do(.*.|)$/) > -1 ||
          pathname.search(/^\/folw\/estimate\/requestestimate\.do(.*.|)$/) > -1) {
          return false;
      }

      var s = document.createElement('script');
      s.type = 'text/javascript';
      s.src = '//tr.webantenna.info/js/webantenna.js';
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(s);

      // init
      var init = function() {
          if (typeof(webantenna) == 'function') {
              _wa.account = 'WA10su-1';
              if (typeof(waCv)         !== "undefined") _wa.cv = waCv;
              if (typeof(waCar)        !== "undefined") _wa.parameters[ 'car' ] = waCar;
              if (typeof(waTitle)      !== "undefined") _wa.parameters[ 'title' ] = waTitle;
              if (typeof(waId)         !== "undefined") _wa.parameters[ 'id' ] = waId;
              if (typeof(waArea)       !== "undefined") _wa.parameters[ 'area' ] = waArea;
              if (typeof(waGender)     !== "undefined") _wa.parameters[ 'gender' ] = waGender;
              if (typeof(waBirthday)   !== "undefined") _wa.parameters[ 'birthday' ] = waBirthday;
              if (typeof(waMakerOther) !== "undefined") _wa.parameters[ 'maker_other' ] = waMakerOther;
              if (typeof(waCarOther)   !== "undefined") _wa.parameters[ 'car_other' ] = waCarOther;
              webantenna();
          }
      };
      // check
      var checkWebAntenna = function() {
          var hasWebAntenna = !!window.webantenna;
          if (hasWebAntenna) {
              init();
              return;
          }
          window.setTimeout(checkWebAntenna, 1000);
      };
      // setTimeout
      window.setTimeout(checkWebAntenna, 1000);
  }());


  // Yahoo! Tag Manager
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=MLT9whn";
    s.parentNode.insertBefore(tagjs, s);
  }());

  //document.getElementsByTagName('html').classList.add('analytics-completed');

  var addClassName = function(element, classNameValue) {
      if (!element || typeof element.className === 'undefined' || typeof classNameValue !== 'string') return;
      if (element.classList) {
        element.classList.add(classNameValue);
      } else {
        var classNames = element.className.replace(/^\s+|\s+$/g, '').split(' ');
        if (classNames.toString() === '') {
            classNames = [];
        }
        if (inArray(classNameValue, classNames) > -1) return;
        classNames.push(classNameValue);
        element.className = classNames.join(' ');
      }
      return element;
  };

  var elem = document.getElementsByTagName('html')[0];
  addClassName(elem, 'analytics-completed');
};

(function () {
  var analyticsJsCheck = document.getElementsByClassName('analytics-completed').length;
  if (analyticsJsCheck < 1) { startAnalytics(); }
})();

var v3AnalyticsLoaded = true;
